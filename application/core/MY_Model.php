<?php
class MY_Model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function getNextId($idName, $tableName, $filter="")
    {
        $str = "SELECT MAX($idName) as id FROM $tableName ";
        if($filter != "")
        {
            $str = $str . " WHERE $filter";           
        }
        $query = CI::db()->query($str);
			
        if($query->num_fields() > 0)
        {
            $row = $query->row_array();
            $temp = $row["id"] + 1; //next key*/
        }else { //belum ada recordnya
            $temp = 1;
        }
			
        return $temp;
    }
    
    public function getNextCode($idName, $tableName, $filter="")
    {
        $str = "SELECT MAX(CAST($idName as SIGNED INTEGER)) as id FROM $tableName ";
        if($filter != "")
        {
            $str = $str . " WHERE $filter";           
        }
        
        $query = CI::db()->query($str);
			
        if($query->num_fields() > 0)
        {
            $row = $query->row_array();
            $temp = $row["id"] + 1; //next key*/
        }else { //belum ada recordnya
            $temp = 1;
        }
	
        return $temp;
    }
    
    public function getNextControlnum($idName, $tableName, $filter="") 
    {
        $str = "SELECT MAX(CAST(SUBSTRING_INDEX($idName, '-', -1) as SIGNED INTEGER)) as controlnum from $tableName ";
    
        if($filter != "")
        {
            $str = $str . " WHERE $filter";           
        }
        
        $query = CI::db()->query($str);
			
        if($query->num_fields() > 0)
        {
            $row = $query->row_array();
            $temp = $row["controlnum"] + 1; //next key*/
        }else { //belum ada recordnya
            $temp = 1;
        }
	
        return $temp;
    }
    
    public function getNextTimeId($idName,$tableName,$idLen=12,$filter="")
    {
    	/*Inisialisasi*/
    	$strTime = date("Ymd"); //YYYYMMDD 8 karakter
    	
    	$countLen = $idLen - 8;
    		
    	/*Cari id yang mirip*/
    	$str = "SELECT MAX($idName) AS $idName FROM $tableName WHERE $idName LIKE '%$strTime%' ";
    	if($filter != "")
    	{
    		$str = $str . " AND $filter";    		
    	}
    	
    	$query = CI::db()->query($str)->row_array();
    	
    	/*Jika ketemu, tambah 1, jika tidak generate baru*/
    	if($query[$idName])
    	{
    		$counter = substr($query[$idName],8,$countLen);
    		$counter = $counter + 1;
    		$newCountLen = strlen($counter);
    	
    		if($newCountLen < $countLen) {
    			$prefix = str_repeat("0",$countLen-$newCountLen);
    		}else {
    			$prefix = "";    			
    		}
    		
    		$nextKey = $strTime.$prefix.$counter;
    	}else {
    		//belum ada recordnya
    		$nextKey = $strTime.str_repeat("0",$countLen-1)."1";
    	}
    	return $nextKey;  		
    }
    
    public function getNextAnggotaId($idName,$tableName,$idLen=10,$filter="")
    {
    	/*Inisialisasi*/
    	$strTime = date("my"); //MMYY 4 karakter
    	 
    	$countLen = $idLen - 4;
    
    	/*Cari id yang mirip*/
    	$str = "SELECT MAX($idName) AS $idName FROM $tableName WHERE $idName LIKE '%$strTime%' ";
    	if($filter != "")
    	{
    		$str = $str . " AND $filter";
    	}
    	 
    	$query = CI::db()->query($str)->row_array();
    	 
    	/*Jika ketemu, tambah 1, jika tidak generate baru*/
    	if($query[$idName])
    	{
    		$counter = substr($query[$idName],4,$countLen);
    		$counter = $counter + 1;
    		$newCountLen = strlen($counter);
    		 
    		if($newCountLen < $countLen) {
    			$prefix = str_repeat("0",$countLen-$newCountLen);
    		}else {
    			$prefix = "";
    		}
    
    		$nextKey = $strTime.$prefix.$counter;
    	}else {
    		//belum ada recordnya
    		$nextKey = $strTime.str_repeat("0",$countLen-1)."1";
    	}
    	return $nextKey;
    }
    
    public function getNextNomorpinjam($idName,$tableName,$idLen=12,$filter="")
    {
    	/*Inisialisasi*/
    	$strTime = date("Ym"); //YYYYMM 6 karakter
    
    	$countLen = $idLen - 6;
    
    	/*Cari id yang mirip*/
    	$str = "SELECT MAX($idName) AS $idName FROM $tableName WHERE $idName LIKE '%$strTime%' ";
    	if($filter != "")
    	{
    		$str = $str . " AND $filter";
    	}
    
    	$query = CI::db()->query($str)->row_array();
    
    	/*Jika ketemu, tambah 1, jika tidak generate baru*/
    	if($query[$idName])
    	{
    		$counter = substr($query[$idName],6,$countLen);
    		$counter = $counter + 1;
    		$newCountLen = strlen($counter);
    		 
    		if($newCountLen < $countLen) {
    			$prefix = str_repeat("0",$countLen-$newCountLen);
    		}else {
    			$prefix = "";
    		}
    
    		$nextKey = $strTime.$prefix.$counter;
    	}else {
    		//belum ada recordnya
    		$nextKey = $strTime.str_repeat("0",$countLen-1)."1";
    	}
    	return $nextKey;
    }
}