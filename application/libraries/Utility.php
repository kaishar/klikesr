<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Utility
{
    /**
     * Static Method to send out javascript alert
     *
     * @param   string  $str_message
     * @return  void
     */
    public static function jsAlert($str_message)
    {
        if (!$str_message) {
            return;
        }

        // replace newline with javascripts newline
        $str_message = str_replace("\n", '\n', $str_message);
        echo '<script type="text/javascript">'."\n";
        echo 'alert("'.addslashes($str_message).'")'."\n";
        echo '</script>'."\n";
    }


    /**
     * Static Method to create random string
     *
     * @param   int     $int_num_string: number of randowm string to created
     * @return  void
     */
    public static function createRandomString($int_num_string = 32)
    {
		$_random = '';
		$_salt = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		for ($r = 0; $r < $int_num_string; $r++) {
			$_random .= $_salt[mt_srand(strlen($_salt))];
		}

		return $_random;
    }

    /**
     * Static Method to check privileges of application module form current user
     *
     * @param   string  $str_module_name
     * @param   string  $str_privilege_type
     * @return  boolean
     */
    public static function havePrivilege($str_module_name, $str_privilege_type = 'r')
    {
        // checking checksum
        //$server_addr = isset($_SERVER['SERVER_ADDR']) ? $_SERVER['SERVER_ADDR'] : (isset($_SERVER['LOCAL_ADDR']) ? $_SERVER['LOCAL_ADDR'] : gethostbyname($_SERVER['SERVER_NAME']));
        //$_checksum = defined('UCS_BASE_DIR')?md5($server_addr.UCS_BASE_DIR.'admin'):md5($server_addr.SB.'admin');
        //if ($_SESSION['checksum'] != $_checksum) {
        //    return false;
        //}
        // check privilege type
        if (!in_array($str_privilege_type, array('r', 'w'))) {
            return false;
        }
        if (isset($_SESSION['priv'][$str_module_name][$str_privilege_type]) AND $_SESSION['priv'][$str_module_name][$str_privilege_type]) {
            return true;
        }
        return false;
    }


    /**
     * Static Method to write application activities logs
     *
     * @param   object  $obj_db
     * @param   string  $str_log_type
     * @param   string  $str_value_id
     * @param   string  $str_location
     * @param   string  $str_log_msg
     * @return  void
     */
    public static function writeLogs($obj_db, $str_log_type, $str_value_id, $str_location, $str_log_msg)
    {
        if (!$obj_db->error) {
            // log table
            $_log_date = date('Y-m-d H:i:s');
            $_log_table = 'system_log';
            // filter input
            $str_log_type = $obj_db->escape_string(trim($str_log_type));
            $str_value_id = $obj_db->escape_string(trim($str_value_id));
            $str_location = $obj_db->escape_string(trim($str_location));
            $str_log_msg = $obj_db->escape_string(trim($str_log_msg));
            // insert log data to database
            @$obj_db->query('INSERT INTO '.$_log_table.'
            VALUES (NULL, \''.$str_log_type.'\', \''.$str_value_id.'\', \''.$str_location.'\', \''.$str_log_msg.'\', \''.$_log_date.'\')');
        }
    }


    /**
     * Static Method to get an ID of database table record
     *
     * @param   object  $obj_db
     * @param   string  $str_table_name
     * @param   string  $str_id_field
     * @param   string  $str_value_field
     * @param   string  $str_value
     * @param   array   $arr_cache
     * @return  mixed
     */
    public static function getID($obj_db, $str_table_name, $str_id_field, $str_value_field, $str_value, &$arr_cache = false)
    {
        $str_value = trim($str_value);
        if ($arr_cache) {
            if (isset($arr_cache[$str_value])) {
                return $arr_cache[$str_value];
            }
        }
        if (!$obj_db->error) {
            $id_q = $obj_db->query('SELECT '.$str_id_field.' FROM '.$str_table_name.' WHERE '.$str_value_field.'=\''.$obj_db->escape_string($str_value).'\'');
            if ($id_q->num_rows > 0) {
                $id_d = $id_q->fetch_row();
                unset($id_q);
                // cache
                if ($arr_cache) {
                    $arr_cache[$str_value] = $id_d[0];
                }
                return $id_d[0];
            } else {
                $_curr_date = date('Y-m-d');
                // if not found then we insert it as new value
                $obj_db->query('INSERT IGNORE INTO '.$str_table_name.' ('.$str_value_field.', input_date, last_update)
                    VALUES (\''.$obj_db->escape_string($str_value).'\', \''.$_curr_date.'\', \''.$_curr_date.'\')');
                if (!$obj_db->error) {
                    // cache
                    if ($arr_cache) {
                        $arr_cache[$str_value] = $obj_db->insert_id;
                    }
                    return $obj_db->insert_id;
                }
            }
        }
    }


    /**
     * Static method to detect mobile browser
     *
     * @return  boolean
     * this script is taken from http://detectmobilebrowsers.com/
     **/
    public static function isMobileBrowser()
    {
        $_is_mobile_browser = false;

        if(preg_match('/android.+mobile|avantgo|bada\/|blackberry|
            blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|
            iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|
            palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|symbian|
            treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|
            xda|xiino/i',
        @$_SERVER['HTTP_USER_AGENT'])
        || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|
            a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|
            amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|
            au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|
            br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|
            cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|
            do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|
            ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|
            go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|
            hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|
            i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|
            ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |
            kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|e\-|e\/|\-[a-w])|
            libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|
            me(di|rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|
            t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|
            n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|
            nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|
            pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|
            psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|
            raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|
            sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|
            sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|
            so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|
            ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|
            ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|
            vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|
            vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|
            wi(g |nc|nw)|wmlb|wonu|x700|xda(\-|2|g)|yas\-|your|zeto|zte\-/i',
        substr(@$_SERVER['HTTP_USER_AGENT'],0,4)))
            $_is_mobile_browser = true;

        return $_is_mobile_browser;
    }

    /**
     * Static method to filter data
     *
     * @param   mixed   $mix_input: input data
     * @param   string  $str_input_type: input type
     * @param   boolean $bool_trim: are input string trimmed
     *
     * @return  mixed
     **/
    public static function filterData($mix_input, $str_input_type = 'get', $bool_escape_sql = true, $bool_trim = true, $bool_strip_html = false) {
        global $dbs;

        if (extension_loaded('filter')) {
            if ($str_input_type == 'var') {
                $mix_input = filter_var($mix_input, FILTER_SANITIZE_STRING);
            } else if ($str_input_type == 'post') {
                $mix_input = filter_input(INPUT_POST, $mix_input);
            } else if ($str_input_type == 'cookie') {
                $mix_input = filter_input(INPUT_COOKIE, $mix_input);
            } else if ($str_input_type == 'session') {
                $mix_input = filter_input(INPUT_SESSION, $mix_input);
            } else {
                $mix_input = filter_input(INPUT_GET, $mix_input);
            }
        } else {
            if ($str_input_type == 'get') {
                $mix_input = $_GET[$mix_input];
            } else if ($str_input_type == 'post') {
                $mix_input = $_POST[$mix_input];
            } else if ($str_input_type == 'cookie') {
                $mix_input = $_COOKIE[$mix_input];
            } else if ($str_input_type == 'session') {
                $mix_input = $_SESSION[$mix_input];
            }
        }

        // trim whitespace on string
        if ($bool_trim) { $mix_input = trim($mix_input); }
        // strip html
        if ($bool_strip_html) { $mix_input = strip_tags($mix_input); }
        // escape SQL string
        if ($bool_escape_sql) { $mix_input = $dbs->escape_string($mix_input); }

        return $mix_input;
    }
   
}