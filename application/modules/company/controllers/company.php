<?php namespace GoCart\Controller;

class Company extends Admin {
    
    public function __construct()
    { 
        parent::__construct();
        
        \CI::load()->model(array('Search', 'M_company'));
		\CI::load()->helper(array('formatting'));
    }
        	
    public function index($sort_by='id',$sort_order='desc', $code=0, $page=0, $rows=10)
    {
        \CI::load()->helper('form');
        		
        $data['message'] = \CI::session()->flashdata('message');
        $data['page_title'] = 'Master Perusahaan';
        $data['code'] = $code;
        $term = false;
				
        $post = \CI::input()->post(null, false);
        if($post)
        {
            //if the term is in post, save it to the db and give me a reference
            $term = json_encode($post);
            $code = \CI::Search()->recordTerm($term);
            $data['code'] = $code;
            //reset the term to an object for use
            $term   = (object)$post;
        }
        elseif ($code)
        {
            $term = \CI::Search()->getTerm($code);
            $term = json_decode($term);
        }
		
        $data['term'] = $term;
        $data['company'] = \CI::M_company()->get_company($term, $sort_by, $sort_order, $rows, $page);
        $data['total'] = \CI::M_company()->get_company_count($term); 
        
        \CI::load()->library('pagination');

        $config['base_url'] = site_url('company/index/'.$sort_by.'/'.$sort_order.'/'.$code.'/');
        $config['total_rows'] = $data['total'];
        $config['per_page'] = $rows;
        $config['uri_segment'] = 6;
        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $config['full_tag_open'] = '<div class="text-center"><ul class="pagination pagination-sm">';
        $config['full_tag_close'] = '</ul></div>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        
        $config['prev_link'] = '&laquo;';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '&raquo;';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        
        \CI::pagination()->initialize($config);
        
        $data['sort_by'] = $sort_by;
        $data['sort_order'] = $sort_order;
        
        $this->view('company', $data);
    }
        
    public function form($id = false)
    {
    	\CI::load()->helper('form');
    	\CI::load()->library('form_validation');
    	 
    	$data['page_title'] = 'Master Perusahaan';
    	    
    	$data['id'] = '';
    	$data['company_name'] = '';
    	    
    	if ($id)
    	{
    		$company = \CI::M_company()->get_company_byid($id);
    
    		if (!$company)
    		{
    			\CI::session()->set_flashdata('error', lang('error_not_found'));
    			redirect('company/index');
    		}
    
    		//set values to db values
    		$data['id'] = $company->id;
    		$data['company_name'] = $company->company_name;
    	}
    
    	\CI::form_validation()->set_rules('company_name', 'Nama Perusahaan', 'trim|required');
		    
    	if (\CI::form_validation()->run() == FALSE)
    	{
    		$this->view('company_form', $data);
    	}else
    	{    			
    		$save['id'] = \CI::input()->post('id');
    		$save['company_name'] = strtoupper(\CI::input()->post('company_name'));
    			
    		$company = \CI::M_company()->save($save);
    		\CI::session()->set_flashdata('message', 'Data perusahaan sudah disimpan.');
    		
    		redirect('company/index');
    	}
    }
    
    public function delete()
    {
    	$id = \CI::input()->post('itemID');
    	if ($id)
    	{
    		$company = \CI::M_company()->get_company_byid($id);
    		if (!$company)
    		{
    			\CI::session()->set_flashdata('error', lang('error_not_found'));
    			redirect('company/index');
    		}else
    		{
    			\CI::M_company()->delete($id);
    			\CI::session()->set_flashdata('message', 'Data perusahaan already deleted');
    			redirect('company/index');
    		}
    	}else
    	{
    		\CI::session()->set_flashdata('error', lang('error_not_found'));
    		redirect('company/index');
    	}
    }
    
    public function delete_selected()
    {
    	$item = array();
    	$item = \CI::input()->post('itemID');
    
    	if (!is_array($item))
    	{
    		// make an array
    		$item = array($item);
    	}
    
    	// loop array
    	foreach ($item as $id) {
    		$company = \CI::M_company()->get_company_byid($id);
    		 
    		if ($company)
    		{
    			\CI::M_company()->delete($id);
    			\CI::session()->set_flashdata('message', 'Data perusahaan sudah dihapus');
    		}else
    		{
    			\CI::session()->set_flashdata('message', 'No data found');
    		}
    	}
    	redirect('company/index');
    }
    
}