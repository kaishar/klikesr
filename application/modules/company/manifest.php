<?php

$routes[] = ['GET|POST', '/company/index/[:orderBy]?/[:orderDir]?/[:code]?/[i:page]?', 'GoCart\Controller\Company#index'];
$routes[] = ['GET|POST', '/company/form/[i:id]?', 'GoCart\Controller\Company#form'];
$routes[] = ['GET|POST', '/company/delete/[i:itemID]?', 'GoCart\Controller\Company#delete'];
$routes[] = ['GET|POST', '/company/delete_selected/[i:itemID]?', 'GoCart\Controller\Company#delete_selected'];
