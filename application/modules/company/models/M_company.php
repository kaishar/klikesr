<?php
class M_company extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
	
	public function get_company($search=false, $sort_by='', $sort_order='desc', $limit=0, $offset=0)
    {
		if ($search)
        {
            if(!empty($search->term))
            {
                $this->get_company_searchlike($search->term);
            }
        }
        
        if($limit>0)
        {
            \CI::db()->limit($limit, $offset);
        }
        
        if(!empty($sort_by))
        {
            \CI::db()->order_by($sort_by, $sort_order);
        }

        return \CI::db()->get('company')->result();
    }

    public function get_company_count($search=false)
    {
        if ($search)
        {
            if(!empty($search->term))
            {
                $this->get_company_searchlike($search->term);
            }
        }
        		
        return CI::db()->count_all_results('company');
    }
	
    private function get_company_searchlike($str)
    {
        //support multiple words
        $term = explode(' ', $str);

        foreach($term as $t)
        {
            $not = '';
            $operator = 'OR';
            if(substr($t,0,1) == '-')
            {
                $not = 'NOT ';
                $operator = 'AND';
                //trim the - sign off
                $t = substr($t,1,strlen($t));
            }
			
            $like = '';
            $like .= "( company.id ".$not."LIKE '%".$t."%' " ;
            $like .= $operator." company.company_name ".$not."LIKE '%".$t."%' )" ;
            
            CI::db()->where($like);
        }
    }
	    
	public function get_company_byid($id)
    {
        \CI::db()->where('id', $id);
		return \CI::db()->get('company')->row();
    }
	
	public function save($data)
    {
        $admin = $this->session->userdata('admin');
        $record = $this->get_company_byid($data['id']);
		
		if(count($record) > 0)
        {
        	$data['updatedby'] = $admin['username'];
            $data['updatedat'] = date('Y-m-d H:i:s');
            
            CI::db()->where('id', $data['id']);
			CI::db()->update('company', $data);
			
			//echo $this->db->last_query(); exit;
            return $data['id'];
        }
        else
        {            
            $data['createdby'] = $admin['username'];
            $data['createdat'] = date('Y-m-d H:i:s');
                        
            CI::db()->insert('company', $data);
            return $data['id'];
        }
    }
    
    public function delete($id)
    {
    	$admin = $this->session->userdata('admin');
    	 
    	CI::db()->where('id', $id);
    	CI::db()->delete('company');
    }
	
	public function get_list()
	{
		$str = "select id, company_name from company where is_trash = 0";
		
		return CI::db()->query($str)->result();	
	}
	
}