<div class="content-wrapper">
    <div class="container">
		<div class="row">
			<div class="col-xs-12">
				<?php
					//lets have the flashdata overright "$message" if it exists
					if(CI::session()->flashdata('message'))
					{
						$message    = CI::session()->flashdata('message');
					}
		
					if(CI::session()->flashdata('error'))
					{
						$error  = CI::session()->flashdata('error');
					}
		
					if(function_exists('validation_errors') && validation_errors() != '')
					{
						$error  = validation_errors();
					}
				?>
		
				<div id="js_error_container" class="alert alert-error" style="display:none;">
					<p id="js_error"></p>
				</div>
		
				<div id="js_note_container" class="alert alert-note" style="display:none;"></div>
		
				<?php if (!empty($message)): ?>
						<div class="alert alert-success" role="alert">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<?php echo $message; ?>
						</div>
				<?php endif; ?>
		
				<?php if (!empty($error)): ?>
						<div class="alert alert-danger" role="alert">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<?php echo $error; ?>
						</div>
				<?php endif; ?>
			</div>
		</div>
        <div class="row">
            <div class="col-md-12">
                <h4 class="page-head-line">Dashboard</h4>
            </div>
        </div>
        <div class="row">        	
			<div class="col-sm-6 col-xs-12">				
				<div class="panel panel-default">
					<div class="panel-body">
						<p>Selamat menggunakan aplikasi <strong>Surat HHES</strong><br> </p>
						<p>Hanya dipergunakan untuk lingkungan <strong>KRESTON INDONESIA</strong></p>
						<hr />								
						<div class="form-group">
							<label class="col-sm-4 control-label">Username</label>
							<label class="col-sm-8 control-label"><?=$usernameinfo?></label>								
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">Fullname</label>
							<label class="col-sm-8 control-label"><?=$logininfo?></label>								
						</div>
						<?php
							echo form_open('admin/logout'); 
						?>
							<div class="form-group">
								<div class="col-md-8 col-md-offset-4">
									<input type="submit" value="LOGOUT" name="submit" class="btn btn-alpha_blue"/>
								</div>
							</div>
						</form>
					</div>
				</div>
        	</div>
        	<div class="col-sm-6 col-xs-12">
				<img src="<?=base_url('assets/img/backend/login-digital.png')?>" class="img-responsive">
			</div>			
		</div>
	</div>
</div>
<!-- CONTENT-WRAPPER SECTION END-->