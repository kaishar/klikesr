<?php namespace GoCart\Controller;

class Reports extends Admin {

    function __construct()
    {
        parent::__construct();
        \CI::load()->helper(array('formatting'));
        \CI::load()->model(array('Variable', 'M_surat'));
    }
    
    public function laporanentrisurat()
    {
    	\CI::load()->helper(array('form','date'));
    	\CI::load()->library('form_validation');    	    
    	
    	$data['message'] = \CI::session()->flashdata('message');
    	    	
    	$data['page_title'] = 'LAPORAN DATA ENTRI SURAT';
    	$data['begin_period'] = date('Y-m-d');
    	$data['end_period'] = date('Y-m-d');
    	    	
    	$list_keyword[null] = 'Semua';
    	$list_keyword['no_surat'] = 'Nomor Surat';
    	$list_keyword['nama_klien'] = 'Nama Klien';
    	$list_keyword['createdby'] = 'Pembuat Surat';
    	$data['list_keyword'] = $list_keyword;
    	
    	$data['key_variable'] = null;
				
		\CI::form_validation()->set_rules('begin_period', 'Tanggal Awal', 'trim|required');
		\CI::form_validation()->set_rules('end_period', 'Tanggal Akhir', 'trim|required');
		//\CI::form_validation()->set_rules('key_variable', 'Filter Pencarian', 'trim|required');
		//\CI::form_validation()->set_rules('key_value', 'Keyword Value', 'trim|required');
		
        if (\CI::form_validation()->run() == FALSE)
        {
            $this->view('vw_exportsurat', $data);
        }
        else
        {
			$begin = \CI::input()->post('begin_period');
			$end = \CI::input()->post('end_period');
            $filter = \CI::input()->post('key_variable');
			$value = \CI::input()->post('key_value');
			
            $export = \CI::input()->post('export');
            if($export == 'pdf')
            {
            	//create pdf filenya
                $file = $this->export_hhes_pdf($begin, $end, $filter, $value);
                
                //open modal
                echo '<script>';
                echo 'window.open("'.base_url('uploads/export/'.$file).'");';
                echo 'window.history.back();';
                echo '</script>';
                
                //cleanup
                /*
                $filename = 'uploads/export/'.$file;
                //delete the existing file if needed
                if(file_exists($filename))
                {
                	unlink($filename);
                }
                */
                
                exit;                                
            }else {
                $this->export_hhes_xls($begin, $end, $filter, $value);
                
                exit;
            }            
        } 
    }
	
	public function export_hhes_pdf($begin, $end, $filter, $value)
    {
    	$data['hhes'] = \CI::M_surat()->get_export_hhes($begin, $end, $filter, $value);
    	$file = 'report_hhes_'.time().'.pdf';
    	
    	// init HTML2PDF
    	$html2pdf = new \HTML2PDF('P', 'A4', 'en');
    	$html2pdf->setDefaultFont('arial');
    	 
    	// get the HTML
    	ob_start();
    	$this->partial('export_hhes_pdf', $data);
    	$content = ob_get_clean();
    	 
    	$html2pdf->writeHTML($content);
    	 
    	// send the PDF
    	$html2pdf->Output(FCPATH . '/uploads/export/'.$file, 'F');
    	return $file;
    }
    
    public function export_hhes_xls($begin, $end, $filter, $value)
    {
    	$hhes = \CI::M_surat()->get_export_hhes($begin, $end, $filter, $value);
    	$file = 'report_hhes_'.time().'.xls';
    	
    	//$dataentri = \CI::Reports()->laporankinerjaentri($tahun);
    
    	// Create new PHPExcel object
    	$objPHPExcel = new \PHPExcel();
    
    	// Set document properties
    	$objPHPExcel->getProperties()->setCreator("Kreston Indonesia");
    	 
    	$objPHPExcel->setActiveSheetIndex(0);
    
    	$objPHPExcel->setActiveSheetIndex(0)
    	->setCellValue('A2', 'NO.URUT')
    	->setCellValue('B2', 'NO SURAT')
    	->setCellValue('C2', 'TIPE')
    	->setCellValue('D2', 'TUJUAN')
    	->setCellValue('E2', 'TANGGAL')
    	->setCellValue('F2', 'NAMA KLIEN')
    	->setCellValue('G2', 'ALAMAT')
    	->setCellValue('H2', 'PEMBUAT');
    
    	$col = 3;
    	$i = 1;
    	foreach($hhes as $rows) 
    	{
    		$objPHPExcel->setActiveSheetIndex(0)
    		->setCellValue('A'.$col, $i)
    		->setCellValue('B'.$col, $rows->no_surat)
    		->setCellValue('C'.$col, $rows->type)
    		->setCellValue('D'.$col, $rows->tujuan)
    		->setCellValue('E'.$col, $rows->tanggal)
    		->setCellValue('F'.$col, $rows->nama_klien)
    		->setCellValue('G'.$col, $rows->alamat)
    		->setCellValue('H'.$col, $rows->createdby);
    
    		$col++;
    		$i++;
    	}
    
    	//auto width
    	$objPHPExcel->getActiveSheet()->getColumnDimension()->setAutoSize(true);
    
    	//judul
    	$objPHPExcel->getActiveSheet()->mergeCells("A1:H1");
    	$objPHPExcel->getActiveSheet()->getStyle("A1:H1")->getFont()->setBold(true);
    	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'EXPORT SURAT HHES');
    
    	//border
    	$style = array(
    			'alignment' => array(
    				'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
    			),
    			'font' => array(
    				'bold' => true
    			)
    	);
    	$first = 'A1';
    	$last = 'H2';
    	$objPHPExcel->getActiveSheet()->getStyle("$first:$last")->applyFromArray($style);
    
    	$style = array(
    			'borders' => array(
    				'allborders' => array(
    					'style' => \PHPExcel_Style_Border::BORDER_THIN
    				)
    			)
    	);
    	$first = 'A2';
    	$last = 'H'.($col-1);
    	$objPHPExcel->getActiveSheet()->getStyle("$first:$last")->applyFromArray($style);
    
    	// Rename worksheet
    	//$objPHPExcel->getActiveSheet()->setTitle('Kinerja_Entri');
    
    	// Set active sheet index to the first sheet, so Excel opens this as the first sheet
    	$objPHPExcel->setActiveSheetIndex(0);
    
    	// Redirect output to a client�s web browser (Excel5)
    	header('Content-Type: application/vnd.ms-excel');
    	header('Content-Disposition: attachment;filename="'.$file);
    	header('Cache-Control: max-age=0');
    	// If you're serving to IE 9, then the following may be needed
    	header('Cache-Control: max-age=1');
    
    	$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    	$objWriter->save('php://output');
    	exit;
    }
                
}
