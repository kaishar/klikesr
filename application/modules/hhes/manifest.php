<?php
$routes[] = ['GET|POST', '/surat-hhes/index/[:orderBy]?/[:orderDir]?/[:code]?/[i:page]?', 'GoCart\Controller\Surathhes#index'];
$routes[] = ['GET|POST', '/surat-hhes/form/[i:id]?', 'GoCart\Controller\Surathhes#form'];
$routes[] = ['GET|POST', '/surat-hhes/delete/[i:itemID]?', 'GoCart\Controller\Surathhes#delete'];
$routes[] = ['GET|POST', '/report-hhes/laporan-entrisurat', 'GoCart\Controller\Reports#laporanentrisurat'];