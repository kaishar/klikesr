<div class="content-wrapper" id="main">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php
                    //lets have the flashdata overright "$message" if it exists
                    if(CI::session()->flashdata('message'))
                    {
                        $message = CI::session()->flashdata('message');
                    }

                    if(CI::session()->flashdata('error'))
                    {
                        $error = CI::session()->flashdata('error');
                    }

                    if(function_exists('validation_errors') && validation_errors() != '')
                    {
                        $error = validation_errors();
                    }
                ?>

                <div id="js_error_container" class="alert alert-error" style="display:none;">
                    <p id="js_error"></p>
                </div>

                <div id="js_note_container" class="alert alert-note" style="display:none;"></div>

                <?php if (!empty($message)): ?>
                    <div class="alert alert-success" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <?php echo $message; ?>
                    </div>
                <?php endif; ?>

                <?php if (!empty($error)): ?>
                    <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <?php echo $error; ?>
                    </div>
                <?php endif; 

                    if ($term): 
                ?>
                    <div class="alert alert-info">
                        <?php echo sprintf(lang('search_returned'), intval($total));?>
                    </div>
                <?php endif;?>
                <?php
                    //set "code" for searches
                    if(!$code)
                    {
                        $code = '';
                    }
                    else
                    {
                        $code = '/'.$code;
                    }

                    function sort_url($lang, $by, $sort, $sorder, $code)
                    {
                        if ($sort == $by)
                        {
                            if ($sorder == 'asc')
                            {
                                $sort = 'desc';
                                $icon = ' <i class="fa fa-chevron-up"></i>';
                            }
                            else
                            {
                                $sort = 'asc';
                                $icon = ' <i class="fa fa-chevron-down"></i>';
                            }
                        }
                        else
                        {
                            $sort = 'asc';
                            $icon = '';
                        }

                        $return = site_url('surat-hhes/index/'.$by.'/'.$sort.'/'.$code.'/');

                        echo '<a href="'.$return.'" class="txt-white">'.$lang.$icon.'</a>';
                    }
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h1 class="page-head-line">SURAT KELUAR HHES</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <!--   Kitchen Sink -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <strong>Listing Surat HHES</strong>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="text-left">
                                    <a href="<?=site_url('surat-hhes/form/')?>" class="btn btn-primary"><i class="fa fa-plus"></i>&nbsp;<?php echo ('Tambah surat keluar HHES'); ?></a>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="text-right">
                                    <?php echo form_open('surat-hhes/index', 'class="form-inline"');?>
                                        <div class="form-group">
                                            <label for="search">Pencarian </label>
                                            <input type="text" size="30" class="form-control" name="term" placeholder="kata kunci" />
                                        </div>
                                        <input type="submit" id="doSearch" value="<?php echo ('Search'); ?>" class="btn btn-info" />				
                                    </form>
                                </div>
                            </div>                        
                        </div>
                        <hr>
                        <div class="row">
                            <?php
                                $page_links = CI::pagination()->create_links();

                                if($page_links != ''):
                                    echo $page_links;
                                endif;
                            ?>
                            <div class="table-responsive">
                                <table class="table table-striped datagrid" id="dataList">
                                    <thead>                                        
                                        <?php if(count($surat_hhes) < 1) { ?>
                                                <tr>
                                                	<th align="center" style="color: white; background-color: #CCCCCC;">No Data</th></tr>
                                        <?php }else { ?>
                                                <tr style="color: white; font-weight: bold; cursor: pointer; background-color: rgb(49, 53, 62);">
                                                    <th>
                                                        <?php echo sort_url('NOMOR SURAT', 'no_surat', $sort_by, $sort_order, $code); ?>
                                                    </th>
                                                    <th>
                                                        <?php echo sort_url('TANGGAL', 'tanggal', $sort_by, $sort_order, $code); ?>
                                                    </th>
                                                    <th>
                                                        <?php echo sort_url('NAMA KLIEN', 'nama_klien', $sort_by, $sort_order, $code); ?>
                                                    </th>
                                                    <th>
                                                        <?php echo sort_url('ALAMAT', 'alamat', $sort_by, $sort_order, $code); ?>
                                                    </th>
                                                    <th>
                                                        <?php echo sort_url('KETERANGAN', 'keterangan', $sort_by, $sort_order, $code); ?>
                                                    </th>
                                                    <th>
                                                        <?php echo sort_url('PEMBUAT', 'createdby', $sort_by, $sort_order, $code); ?>
                                                    </th>
                                                    <th>
                                                        <?php echo sort_url('TANGGAL DIBUAT', 'createdat', $sort_by, $sort_order, $code); ?>
                                                    </th>
                                                </tr>                                      
                                    </thead>                                    
                                    <tbody>
                                        <?php 
                                            $i = 1;
                                            foreach($surat_hhes as $rows):
                                        ?>
                                                <tr>
                                                    <td valign="top" style="width: 15%;">
                                                    	<a title="View" href="<?=site_url('surat-hhes/form/'.$rows->id)?>"><?=$rows->no_surat?></a>
                                                    </td>
                                                    <td valign="top" style="width: 10%;"><?=$rows->tanggal?></td>
                                                    <td valign="top"><?=$rows->nama_klien?></td>
                                                    <td valign="top"><?=$rows->alamat?></td>
                                                    <td valign="top"><?=$rows->keterangan?></td>
                                                    <td valign="top"><?=$rows->createdby?></td>
                                                    <td valign="top" style="width: 15%;"><?=$rows->createdat?></td>
                                                </tr>
                                        <?php   
                                            $i++;
                                            endforeach;
                                            }
                                        ?>
                                    </tbody>
                                </table>                                
                            </div>                            
                        </div>
                	</div>
                </div>
                <!-- End  Kitchen Sink -->
            </div>
        </div>
    </div>
</div>
<!-- CONTENT-WRAPPER SECTION END-->
