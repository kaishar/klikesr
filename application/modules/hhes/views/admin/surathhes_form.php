<div class="content-wrapper" id="main">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php
                    //lets have the flashdata overright "$message" if it exists
                    if(CI::session()->flashdata('message'))
                    {
                        $message = CI::session()->flashdata('message');
                    }

                    if(CI::session()->flashdata('error'))
                    {
                        $error = CI::session()->flashdata('error');
                    }

                    if(function_exists('validation_errors') && validation_errors() != '')
                    {
                        $error = validation_errors();
                    }
                ?>

                <div id="js_error_container" class="alert alert-error" style="display:none;">
                    <p id="js_error"></p>
                </div>

                <div id="js_note_container" class="alert alert-note" style="display:none;"></div>

                <?php if (!empty($message)): ?>
                    <div class="alert alert-success" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <?php echo $message; ?>
                    </div>
                <?php endif; ?>

                <?php if (!empty($error)): ?>
                    <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <?php echo $error; ?>
                    </div>
                <?php endif; ?>                 
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h1 class="page-head-line">SURAT HHES FORM</h1>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <strong>Add Surat HHES</strong>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="text-left">
                                    <a href="<?=site_url('surat-hhes/index/')?>" class="btn btn-primary"><i class="fa fa-list-alt"></i>&nbsp;<?php echo ('List surat HHES'); ?></a>                                    
                                </div>
                            </div>                                                   
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-12">
                            <?php 
                                $attributes = array('class' => 'form-horizontal');
                                echo form_open('surat-hhes/form/'.$id, $attributes); 
                            ?>
                            <div class="form-group">
                                <label for="no_surat" class="col-sm-2 control-label">Nomor Surat</label>
                                <div class="col-sm-10"><h5><strong><?=$no_surat?></strong></h5></div>
                            </div>
                            <div class="form-group">
                                <label for="no" class="col-sm-2 control-label">Penentuan Nomor</label>
                                <div class="col-sm-10">
                                	<input type="text" value="<?=$no?>" class="form-control" style="width: 30%" disabled>
                                	<input type="hidden" name="no" id="no" value="<?=$no?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="tujuan" class="col-sm-2 control-label">Tanda Tangan</label>
                                <div class="col-sm-10">
                                <?php                                
                                    $attributes = array('id' => 'tujuan',
                                        'name' => 'tujuan',
                                        'class' => 'form-control',
                                        'style' =>'width: 30%;'); 
                                    echo form_dropdown('tujuan', $list_tujuan, assign_value('tujuan', $tujuan), $attributes);   
                                ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="type" class="col-sm-2 control-label">Tipe Surat</label>
                                <div class="col-sm-10">
                                <?php                                
                                    $attributes = array('id' => 'type',
                                        'name' => 'type',
                                        'class' => 'form-control',
                                        'style' =>'width: 30%;'); 
                                    echo form_dropdown('type', $list_type, assign_value('type', $type), $attributes);   
                                ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="tanggal" class="col-sm-2 control-label">Tanggal</label>
                                <div class="col-sm-10">
                                	<div class="dateField">
		                                <input type="date" value="<?=assign_value('tanggal', $tanggal)?>" id="tanggal" name="tanggal" class="dateInput">
		                                <a title="Open Calendar" onclick="javascript: dateType = 'date'; openCalendar('tanggal');" style="cursor: pointer;" class="calendarLink notAJAX"></a>
		                            </div>                                
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="nama_klien" class="col-sm-2 control-label">Nama Klien *</label>
                                <div class="col-sm-10">
                                <?php 
                                    echo form_input(['name'=>'nama_klien', 
                                            'value'=>assign_value('nama_klien', $nama_klien), 
                                            'class'=>'form-control',
                                            'maxlength'=>'256',
                                            'style'=>'width: 60%;']); 
                                ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="alamat" class="col-sm-2 control-label">Alamat Klien *</label>
                                <div class="col-sm-10">
                                <?php 
                                    echo form_textarea(['name'=>'alamat',
                                    		'id'=>'alamat',
                                    		'value'=>assign_value('alamat', $alamat),
                                    		'class'=>'form-control',
                                    		'rows'=>4,
                                    		'style'=>'width: 60%; resize: none;']);
                                ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="keterangan" class="col-sm-2 control-label">Keterangan *</label>
                                <div class="col-sm-10">
                                <?php 
                                    echo form_input(['name'=>'keterangan', 
                                            'value'=>assign_value('keterangan', $keterangan), 
                                            'class'=>'form-control',
                                            'maxlength'=>'256',
                                            'style'=>'width: 60%;']); 
                                ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-10 col-sm-offset-2">
                                <?php
                                    if ($id) 
                                    {
                                ?>
                                    <td>
                                        <input class="button btn btn-primary" type="submit" value="Update" name="saveData">
                                        <input class="cancelButton button btn btn-primary" type="button" value="Batal" onclick="window.history.back()">
                                        <input class="button btn btn-danger btn-delete confirmSubmit" type="button" onclick="confSubmit('deleteForm', 'Apakah anda yakin akan menghapus data <?=$no_surat?>?\n')" value="Hapus data">
                                    </td>
                                <?php
                                    }else {
                                ?>
                                    <td><input type="submit" value="Simpan" name="saveData" class="button btn btn-primary"></td>
                                <?php
                                    }
                                ?>
                                </div>
                            </div>
                            </form>
                            <form action="<?=site_url('surat-hhes/delete')?>" id="deleteForm" method="post" style="display: inline;">
                                <input type="hidden" name="itemID" value="<?=$id?>" />
                            </form>  
                            <hr>
                            </div>
                        </div>
                    </div>
                    <!-- End  Kitchen Sink -->
                </div>
            </div>
        </div>
    </div>
</div>
<!-- CONTENT-WRAPPER SECTION END-->
