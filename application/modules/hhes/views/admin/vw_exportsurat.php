<div class="content-wrapper" id="main">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php
                    //lets have the flashdata overright "$message" if it exists
                    if(CI::session()->flashdata('message'))
                    {
                        $message = CI::session()->flashdata('message');
                    }

                    if(CI::session()->flashdata('error'))
                    {
                        $error = CI::session()->flashdata('error');
                    }

                    if(function_exists('validation_errors') && validation_errors() != '')
                    {
                        $error = validation_errors();
                    }
                ?>

                <div id="js_error_container" class="alert alert-error" style="display:none;">
                    <p id="js_error"></p>
                </div>

                <div id="js_note_container" class="alert alert-note" style="display:none;"></div>

                <?php if (!empty($message)): ?>
                    <div class="alert alert-success" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <?php echo $message; ?>
                    </div>
                <?php endif; ?>

                <?php if (!empty($error)): ?>
                    <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <?php echo $error; ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h1 class="page-head-line">Export Surat HHES</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <!--   Kitchen Sink -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <strong>Export Surat HHES</strong>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="text-left">
                                    <?php echo form_open('report-hhes/laporan-entrisurat', 'class="form-inline"');?>                                        
		                                 
						                <div class="row">
						                    <div class="col-sm-8">                
						                        <label class="col-md-3 col-sm-3"><h5><strong>Tanggal surat dari</strong></h5></label>
						                        <div class="col-md-4 col-sm-4">
						                            <div class="dateField">
						                                <input type="date" value="<?=assign_value('begin_period', $begin_period)?>" id="begin_period" name="begin_period" class="dateInput">
						                                <a title="Open Calendar" onclick="javascript: dateType = 'date'; openCalendar('begin_period');" style="cursor: pointer;" class="calendarLink notAJAX"></a>
						                            </div>                            
						                        </div> 
						                        <label class="col-md-2 col-sm-2"><strong>sampai dengan</strong></label>
						                        <div class="col-md-3 col-sm-3">
						                            <div class="dateField">
						                                <input type="date" value="<?=assign_value('end_period', $end_period)?>" id="end_period" name="end_period" class="dateInput">
						                                <a title="Open Calendar" onclick="javascript: dateType = 'date'; openCalendar('end_period');" style="cursor: pointer;" class="calendarLink notAJAX"></a>
						                            </div>
						                        </div>
						                    </div>            
						                </div> 
						                <div class="row">
						                    <div class="col-sm-8">                
						                        <label class="col-md-3 col-sm-3"><h5><strong>Filter Pencarian</strong></h5></label>
						                        <div class="col-md-3 col-sm-3">
						                            <?php
						                                $attributes = array('id' => 'key_variable',
						                                    'name' => 'key_variable',
						                                    'class' => 'form-control',
						                                    'style' => 'width:100%');
						                                echo form_dropdown('key_variable', $list_keyword, assign_value('key_variable', $key_variable), $attributes);
						                            ?>  						                                                                        
						                        </div>
						                        <div class="col-md-6 col-sm-6">
						                        	<input type="text" id="key_value" name="key_value" class="form-control" style="width: 100%;" placeholder="Keyword value">  
						                        </div>               
						                    </div>            
						                </div>
						                <div class="row">
						                    <div class="col-sm-8">    
						                        <label class="col-lg-3 col-md-3 txt-white">&nbsp;</label>
						                        <div class="col-lg-5 col-md-5">
													<button name="export" type="submit" class="btn btn-alpha_blue" value="pdf"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>&nbsp;EXPORT PDF</button>
													<button name="export" type="submit" class="btn btn-alpha_blue" value="xls"><i class="fa fa-file-excel-o" aria-hidden="true"></i>&nbsp;EXPORT XLS</button>
						                        </div>               
						                    </div>
						                </div>
                                        
                                    </form>
                                </div>
                            </div>                        
                        </div>
                        <hr>
                        
                	</div>
                </div>
                <!-- End  Kitchen Sink -->
            </div>
        </div>
    </div>
</div>
<!-- CONTENT-WRAPPER SECTION END-->

