<?php namespace GoCart\Controller;

class Surathrd extends Admin {
    
    public function __construct()
    { 
        parent::__construct();
        
        \CI::load()->model(array('Search','M_hrd', 'Variable'));

        \CI::load()->helper(array('formatting', 'date'));
    }
        	
    public function index($sort_by='id',$sort_order='desc', $code=0, $page=0, $rows=10)
    {
        \CI::load()->helper('form');
        \CI::load()->helper('date');
		
        $data['message'] = \CI::session()->flashdata('message');
        $data['page_title'] = 'Surat HRD HHES';
        $data['code'] = $code;
        $term = false;
		
        $post = \CI::input()->post(null, false);
        if($post)
        {
            //if the term is in post, save it to the db and give me a reference
            $term = json_encode($post);
            $code = \CI::Search()->recordTerm($term);
            $data['code'] = $code;
            //reset the term to an object for use
            $term   = (object)$post;
        }
        elseif ($code)
        {
            $term = \CI::Search()->getTerm($code);
            $term = json_decode($term);
        }

        $data['term'] = $term;
        $data['surat_hrd'] = \CI::M_hrd()->get_surat($term, $sort_by, $sort_order, $rows, $page);
        $data['total'] = \CI::M_hrd()->get_surat_count($term); 

        \CI::load()->library('pagination');

        $config['base_url'] = site_url('surat-hrd/index/'.$sort_by.'/'.$sort_order.'/'.$code.'/');
        $config['total_rows'] = $data['total'];
        $config['per_page'] = $rows;
        $config['uri_segment'] = 6;
        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $config['full_tag_open'] = '<div class="text-center"><ul class="pagination pagination-sm">';
        $config['full_tag_close'] = '</ul></div>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        
        $config['prev_link'] = '&laquo;';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '&raquo;';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        
        \CI::pagination()->initialize($config);
        
        $data['sort_by'] = $sort_by;
        $data['sort_order'] = $sort_order;
        
        $this->view('surat_hrd', $data);
    } 
    
    public function form($id = false)
    {
        \CI::load()->helper('form');
        \CI::load()->library(array('form_validation', 'Utility'));
        
        $data['page_title'] = 'Tambah Surat HRD';
        
        $data['id'] = '';
        $data['no_surat'] = '';
        $data['no'] = \CI::M_hrd()->getNextCode('no', 'hrdsurats');
        $data['type'] = '';
        $data['tujuan'] = '';
        $data['kap'] = '';
        $data['tanggal'] = date('Y-m-d');
        $data['nama'] = '';
        $data['alamat'] = '';
        $data['pembuat'] = '';
        $data['keterangan'] = '';
        
        $list = \CI::Variable()->get_variable('TUJUAN_HRD');
        $list_tujuan[NULL] = 'Pilih tanda tangan';
        foreach($list as $rows)
        {
        	$list_tujuan[$rows->VALUE] = $rows->DESCRIPTION;
        }
        $data['list_tujuan'] = $list_tujuan;
        
        $list = \CI::Variable()->get_variable('TYPE_HRD');
        $list_type[NULL] = 'Pilih tipe surat';
        foreach($list as $rows)
        {
        	$list_type[$rows->VALUE] = $rows->DESCRIPTION;
        }
        $data['list_type'] = $list_type;
        
        // privileges checking
        $can_read = \CI::Utility()->havePrivilege('hrd', 'r');
        $can_write = \CI::Utility()->havePrivilege('hrd', 'w');
        
        //echo $can_read.NL;
        //echo $can_write; exit;
        
        if (!$can_write) {
        	\CI::session()->set_flashdata('error', 'You don\'t have enough privileges to view this section');
        	redirect('surat-hrd/index');
        }
                            
        if ($id)
        { 
            $surat = \CI::M_hrd()->get_surat_byid($id);
            
            if (!$surat)
            {
                \CI::session()->set_flashdata('error', lang('error_not_found'));
                redirect('surat-hrd/index');
            }
           			
            //set values to db values
            $data['id'] = $surat->id;
            $data['no_surat'] = $surat->no_surat;
	        $data['no'] = $surat->no;
	        $data['type'] = $surat->type;
	        $data['tujuan'] = $surat->tujuan;
	        $data['kap'] = $surat->kap;
	        $data['tanggal'] = $surat->tanggal;
	        $data['nama'] = $surat->nama;
	        $data['alamat'] = $surat->alamat;
	        $data['pembuat'] = $surat->pembuat;
	        $data['keterangan'] = $surat->keterangan;
        }
        
		\CI::form_validation()->set_rules('type', 'Tipe Surat', 'trim|required');
        \CI::form_validation()->set_rules('tujuan', 'Tujuan (Tanda Tangan)', 'trim|required');
		\CI::form_validation()->set_rules('tanggal', 'Tanggal', 'trim|required');
        \CI::form_validation()->set_rules('nama', 'Nama', 'trim|required');
        \CI::form_validation()->set_rules('alamat', 'Alamat', 'trim|required');
        \CI::form_validation()->set_rules('keterangan', 'Keterangan', 'trim|required');
                        
        if (\CI::form_validation()->run() == FALSE)
        {
            $this->view('surathrd_form', $data);
        }else
        {
            $save['id'] = $id;            
	        $save['no'] = \CI::input()->post('no');
	        $save['type'] = \CI::input()->post('type');
	        $save['tujuan'] = \CI::input()->post('tujuan');
	        $save['kap'] = 'HHES';
	        $save['tanggal'] = \CI::input()->post('tanggal');
	        $save['nama'] = \CI::input()->post('nama');
	        $save['alamat'] = \CI::input()->post('alamat');
	        //$save['pembuat'] = \CI::input()->post('descr');
	        $save['keterangan'] = \CI::input()->post('keterangan');
	        
	        $bulantahun = \CI::M_hrd()->get_year_month($save['tanggal']);
	        $bulansurat = getBilRomawi($bulantahun['month']);
	        $tahunsurat = $bulantahun['year'];
	        
	        $save['no_surat'] = $this->get_nosurat($save['no'], $save['type'], $save['tujuan'], $save['kap'], $bulansurat, $tahunsurat);
                        
            \CI::M_hrd()->save($save);
            
            \CI::session()->set_flashdata('message', 'Surat HRD sudah disimpan.');
                  
            redirect('surat-hrd/index');  
        }
    }
    
    public function delete()
    {
        $id = \CI::input()->post('itemID'); 
        
        if ($id)
        { 
            $surat = \CI::M_hrd()->get_surat_byid($id);
            if (!$surat)
            {
                \CI::session()->set_flashdata('error', lang('error_not_found'));
                redirect('surat-hrd/index');
            }
            else
            {
                \CI::M_hrd()->delete($id);
                \CI::session()->set_flashdata('message', 'Data surat sudah dihapus');
                redirect('surat-hrd/index');
            }
        }
        else
        {
            \CI::session()->set_flashdata('error', lang('error_not_found'));
            redirect('surat-hrd/index');
        }
    }
        
    public function get_nosurat($no, $type, $tujuan, $kap, $bln, $thn)
    {
    	$str = '';
    	$str = $str.$no.'/'.$type.'/'.$tujuan.'/'.$kap.'/'.$bln.'/'.$thn;
    
    	return $str;
    }
    
}