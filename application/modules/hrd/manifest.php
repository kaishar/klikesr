<?php
$routes[] = ['GET|POST', '/surat-hrd/index/[:orderBy]?/[:orderDir]?/[:code]?/[i:page]?', 'GoCart\Controller\Surathrd#index'];
$routes[] = ['GET|POST', '/surat-hrd/form/[i:id]?', 'GoCart\Controller\Surathrd#form'];
$routes[] = ['GET|POST', '/surat-hrd/delete/[i:itemID]?', 'GoCart\Controller\Surathrd#delete'];
$routes[] = ['GET|POST', '/report-hrd/laporan-entrisurat', 'GoCart\Controller\Reportshrd#laporanentrisurat'];