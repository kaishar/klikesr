<?php
Class M_hrd extends MY_Model
{
    public function get_surat($search=false, $sort_by='', $sort_order='desc', $limit=0, $offset=0)
    {
        if ($search)
        {
            if(!empty($search->term))
            {
                $this->get_surat_searchlike($search->term);
            }
        }
        
        CI::db()->where('is_trash','0');
        
        if($limit>0)
        {
            \CI::db()->limit($limit, $offset);
        }
        
        if(!empty($sort_by))
        {
            \CI::db()->order_by($sort_by, $sort_order);
        }

        return \CI::db()->get('hrdsurats')->result();
    }

    public function get_surat_count($search=false)
    {
        if ($search)
        {
            if(!empty($search->term))
            {
                $this->get_surat_searchlike($search->term);
            }
        }
        
        CI::db()->where('is_trash','0');
        return CI::db()->count_all_results('hrdsurats');
    }
	
    private function get_surat_searchlike($str)
    {
        //support multiple words
        $term = explode(' ', $str);

        foreach($term as $t)
        {
            $not = '';
            $operator = 'OR';
            if(substr($t,0,1) == '-')
            {
                $not = 'NOT ';
                $operator = 'AND';
                //trim the - sign off
                $t = substr($t,1,strlen($t));
            }

            $like = '';
            $like .= "( hrdsurats.no_surat ".$not."LIKE '%".CI::db()->escape_like_str($t)."%' ";
            $like .= $operator." hrdsurats.tanggal ".$not."LIKE '%".CI::db()->escape_like_str($t)."%' ";
            $like .= $operator." hrdsurats.nama_klien ".$not."LIKE '%".CI::db()->escape_like_str($t)."%' ";
            $like .= $operator." hrdsurats.keterangan ".$not."LIKE '%".CI::db()->escape_like_str($t)."%' ";
            $like .= $operator." hrdsurats.createdby ".$not."LIKE '%".CI::db()->escape_like_str($t)."%' ";
            $like .= $operator." hrdsurats.createdat ".$not."LIKE '%".CI::db()->escape_like_str($t)."%' )";

            CI::db()->where($like);
        }
    }
	
    public function save($surats)
    {
        $admin = $this->session->userdata('admin');
        $surats['is_trash'] = 0;
        
        if($surats['id'])
        {
            $surats['updatedby'] = $admin['username'];
            $surats['updatedat'] = date('Y-m-d H:i:s');
            
            CI::db()->where('id', $surats['id']);
            CI::db()->update('hrdsurats', $surats);
            return $surats['id'];
        }
        else
        {
            $surats['createdby'] = $admin['username'];
            $surats['createdat'] = date('Y-m-d H:i:s');
            
            $this->db->insert('hrdsurats', $surats);
            return $this->db->insert_id();;
        }
    }

    public function delete($id)
    {
        $admin = $this->session->userdata('admin');
        
        $save['is_trash'] = 1;
        $save['updatedby'] = $admin['username'];
        $save['updatedat'] = date('Y-m-d H:i:s');
            
        CI::db()->where('id', $id);
        CI::db()->update('hrdsurats', $save);
    }
    	
    public function get_surat_byid($id)
    {
        $result = CI::db()->get_where('hrdsurats', array('id'=>$id));
        return $result->row();
    }
        
    public function get_year_month($tanggal)
    {
    	$_sql = "SELECT 
    			DATE_FORMAT(?, '%y') as year,
    			MONTH(?) AS 'month'";
    	$result = CI::db()->query($_sql, array($tanggal, $tanggal));
    	
    	return $result->row_array();
    }
    
    public function get_surat_intrash($search=false, $sort_by='', $sort_order='desc', $limit=0, $offset=0)
    {
    	if ($search)
    	{
    		if(!empty($search->term))
    		{
    			$this->get_surat_searchlike($search->term);
    		}
    	}
    
    	CI::db()->where('is_trash','1');
    
    	if($limit>0)
    	{
    		\CI::db()->limit($limit, $offset);
    	}
    
    	if(!empty($sort_by))
    	{
    		\CI::db()->order_by($sort_by, $sort_order);
    	}
    
    	return \CI::db()->get('hrdsurats')->result();
    }
    
    public function get_surat_intrash_count($search=false)
    {
    	if ($search)
    	{
    		if(!empty($search->term))
    		{
    			$this->get_surat_searchlike($search->term);
    		}
    	}
    
    	CI::db()->where('is_trash','1');
    	return CI::db()->count_all_results('hrdsurats');
    }
    
    public function get_export_hrd($begin, $end, $filter, $value)
    {
    	$sql = "select * from hrdsurats
    			where 
    				is_trash = 0 
    				and (tanggal between ? and ?)";
    	
    	if(isset($filter) && isset($value)) {
    		$sql .= " and ".$filter." like ?";
    		
    		$query = $this->db->query($sql, array($begin, $end, '%'.$value.'%'));
    	}else {
    		$query = $this->db->query($sql, array($begin, $end));
    	}
    	
    	//echo $this->db->last_query();
    	return $query->result();
    }
   
}
