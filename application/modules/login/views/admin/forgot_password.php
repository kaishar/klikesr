<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" /><meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="MobileOptimized" content="360">
    <meta name="description" content="Sistem Informasi Perpustakaan Sekolah">
    <meta name="programmer" content="kwibisan">
    <meta name="author" content="kwibisan">
    <meta name="copyright" content="materawali inc.">
    <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <title>KRESTON INDONESIA</title>
    <!-- BOOTSTRAP CORE STYLE  -->
    <link href="<?=base_url('assets/css/bootstrap.min.css')?>" rel="stylesheet" />
    <link href="<?=base_url('assets/css/animate.min.css')?>" rel="stylesheet" />
    <!-- FONT AWESOME ICONS  -->
    <link href="<?=base_url('assets/css/font-awesome.css')?>" rel="stylesheet" />
    <!-- CUSTOM STYLE  -->
    <link href="<?=base_url('assets/css/style.css')?>" rel="stylesheet" />
    
    <link href="<?=base_url('assets/css/background.css')?>" rel="stylesheet" />
     <!-- HTML5 Shiv and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <link rel="shortcut icon" href="<?=base_url('assets/img/logoicon/favicon.ico'); ?>">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?=base_url('assets/img/logoicon/apple-touch-icon-144-precomposed.png'); ?>">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?=base_url('assets/img/logoicon/apple-touch-icon-114-precomposed.png'); ?>">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?=base_url('assets/img/logoicon/apple-touch-icon-72-precomposed.png'); ?>">
    <link rel="apple-touch-icon-precomposed" href="<?=base_url('assets/img/logoicon/apple-touch-icon-57-precomposed.png'); ?>">
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>   
    <!-- MENU SECTION END-->
    <div class="content-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <?php
                        //lets have the flashdata overright "$message" if it exists
                        if(CI::session()->flashdata('message'))
                        {
                            $message    = CI::session()->flashdata('message');
                        }

                        if(CI::session()->flashdata('error'))
                        {
                            $error  = CI::session()->flashdata('error');
                        }

                        if(function_exists('validation_errors') && validation_errors() != '')
                        {
                            $error  = validation_errors();
                        }
                    ?>

                    <div id="js_error_container" class="alert alert-error" style="display:none;">
                        <p id="js_error"></p>
                    </div>

                    <div id="js_note_container" class="alert alert-note" style="display:none;"></div>

                    <?php if (!empty($message)): ?>
                            <div class="alert alert-success" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <?php echo $message; ?>
                            </div>
                    <?php endif; ?>

                    <?php if (!empty($error)): ?>
                            <div class="alert alert-danger" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <?php echo $error; ?>
                            </div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="row">
            	<div class="col-md-4 col-md-offset-4 well-kw well-sm">
	            <?php echo form_open('admin/forgot-password') ?>
	
			        <div class="form-group">
			            <label for="username"><span class="txt-white"><?php echo lang('username');?></span></label>
			            <?php echo form_input(array('name'=>'username', 'class'=>'form-control')); ?>
			        </div>
			
			        <input class="btn btn-primary" type="submit" value="<?php echo lang('reset_password');?>"/>
			        
			    <?php echo  form_close(); ?>
			
			        <div class="text-center">
			            <a href="<?php echo site_url('admin/login');?>"><span class="txt-white"><?php echo lang('return_to_login');?></span></a>
			        </div>
			    </div>
		    </div>
		</div>
    </div>
    <!-- CONTENT-WRAPPER SECTION END-->
    <!-- BACKGROUND-->
    <div class="s-background animated fadeIn"></div>
    <!-- JAVASCRIPT AT THE BOTTOM TO REDUCE THE LOADING TIME  -->
    <!-- CORE JQUERY SCRIPTS -->
    <script src="<?=base_url('assets/js/jquery.min.js')?>"></script>
    <!-- BOOTSTRAP SCRIPTS  -->
    <script src="<?=base_url('assets/js/bootstrap.min.js')?>"></script>
    <script src="<?=base_url('assets/js/wow.min.js')?>"></script>
    <script type="text/javascript">jQuery('#username').focus();</script>
</body>
</html>
