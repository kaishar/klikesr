<?php namespace GoCart\Controller;

class Opinions extends Admin {
    
    public function __construct()
    { 
        parent::__construct();
        
        \CI::load()->model(array('Search','M_opini', 'Variable', 'M_company'));

        \CI::load()->helper(array('formatting', 'date'));
    }
        	
    public function index($sort_by='id',$sort_order='desc', $code=0, $page=0, $rows=10)
    {
        \CI::load()->helper('form');
        \CI::load()->helper('date');
		
        $data['message'] = \CI::session()->flashdata('message');
        $data['page_title'] = 'Surat Opini Audit';
        $data['code'] = $code;
        $term = false;
		
        $post = \CI::input()->post(null, false);
        if($post)
        {
            //if the term is in post, save it to the db and give me a reference
            $term = json_encode($post);
            $code = \CI::Search()->recordTerm($term);
            $data['code'] = $code;
            //reset the term to an object for use
            $term   = (object)$post;
        }
        elseif ($code)
        {
            $term = \CI::Search()->getTerm($code);
            $term = json_decode($term);
        }

        $data['term'] = $term;
        $data['opini'] = \CI::M_opini()->get_surat($term, $sort_by, $sort_order, $rows, $page);
		//echo \CI::db()->last_query(); exit;
        $data['total'] = \CI::M_opini()->get_surat_count($term); 
		//echo \CI::db()->last_query(); exit;

        \CI::load()->library('pagination');

        $config['base_url'] = site_url('opini/index/'.$sort_by.'/'.$sort_order.'/'.$code.'/');
        $config['total_rows'] = $data['total'];
        $config['per_page'] = $rows;
        $config['uri_segment'] = 6;
        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $config['full_tag_open'] = '<div class="text-center"><ul class="pagination pagination-sm">';
        $config['full_tag_close'] = '</ul></div>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        
        $config['prev_link'] = '&laquo;';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '&raquo;';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        
        \CI::pagination()->initialize($config);
        
        $data['sort_by'] = $sort_by;
        $data['sort_order'] = $sort_order;
        
        $this->view('opini', $data);
    } 
    
    public function form($id = false)
    {
		// privileges checking
        $can_read = \CI::Utility()->havePrivilege('opini', 'r');
        $can_write = \CI::Utility()->havePrivilege('opini', 'w');
        
        //echo $can_read.NL;
        //echo $can_write; exit;
        
        if (!$can_write) {
        	\CI::session()->set_flashdata('error', 'You don\'t have enough privileges to view this section');
        	redirect('opini/index');
        }
		
        \CI::load()->helper(array('form','date'));
        \CI::load()->library(array('form_validation', 'Utility'));
        
        $data['page_title'] = 'Tambah Surat Opini Audit';
		
		$data['disable'] = false;
        
        $data['id'] = '';
        $data['no_op'] = '';
        $data['no'] = \CI::M_opini()->get_next_nopini();
        //$data['type'] = '';
        $data['tujuan'] = '';
        $data['kap'] = '';
        $data['tanggal'] = date('Y-m-d');
		$data['penandatanganan'] = '';
		//$data['init_klien'] = '';
		$data['company_name'] = '';
        $data['nama_klien'] = '';
        $data['alamat'] = '';
        $data['pembuat'] = '';
        $data['keterangan'] = '';
        $data['change_no'] = '';
        $data['partner'] = '';
        $data['manager'] = '';
        $data['nama_peminta_no'] = '';
        $data['proses_flag'] = '';
        $data['tanggal_laporan'] = '';
        
        /*
        $list = \CI::Variable()->get_variable('TUJUAN');
        $list_tujuan[NULL] = 'Pilih tanda tangan';
        foreach($list as $rows)
        {
        	$list_tujuan[$rows->VALUE] = $rows->DESCRIPTION;
        }
        $data['list_tujuan'] = $list_tujuan;
                
        $list = \CI::Variable()->get_variable('TYPE_OPINIONS');
        $list_type[NULL] = 'Pilih tipe surat';
        foreach($list as $rows)
        {
        	$list_type[$rows->VALUE] = $rows->DESCRIPTION;
        }
        $data['list_type'] = $list_type;
        */
		
		$list = \CI::Variable()->get_variable_orderby('PARTNER_OPINI', 'ORDERBY', 'ASC');
        $list_partner[NULL] = 'Pilih Partner';
        foreach($list as $rows)
        {
        	$list_partner[$rows->VALUE] = $rows->DESCRIPTION;
        }
        $data['list_partner'] = $list_partner;
        
        $list = \CI::Variable()->get_variable_orderby('KAP', 'ORDERBY', 'ASC');
        $list_kap[NULL] = 'Pilih Cabang';
        foreach($list as $rows)
        {
        	$list_kap[$rows->VALUE] = $rows->DESCRIPTION;
        }
        $data['list_kap'] = $list_kap;
		
		$list_penandatanganan[NULL] = 'Penandatanganan Ke-';
        for($i=1; $i<6; $i++)
		{
				$list_penandatanganan[getBilRomawi($i)] = getBilRomawi($i);
		}
        $data['list_penandatanganan'] = $list_penandatanganan;
        
        $list = \CI::Variable()->get_variable('PROSES_FLAG');
        $list_proses_flag[NULL] = 'Pilih Proses Flag';
        foreach($list as $rows)
        {
        	$list_proses_flag[$rows->VALUE] = $rows->DESCRIPTION;
        }
        $data['list_proses_flag'] = $list_proses_flag;
		
		//Data Company
		$data['company'] = \CI::M_company()->get_list();
                                    
        if ($id)
        { 
			$data['disable'] = true;
			
            $surat = \CI::M_opini()->find_opinicompany_byid($id);
            
            if (!$surat)
            {
                \CI::session()->set_flashdata('error', lang('error_not_found'));
                redirect('opini/index');
            }
           			
            //set values to db values
            $data['id'] = $surat->id;
            $data['no_op'] = $surat->no_op;
	        $data['no'] = $surat->no;
	        //$data['type'] = $surat->type;
	        $data['tujuan'] = $surat->tujuan;
	        $data['kap'] = $surat->kap;
	        $data['tanggal'] = $surat->tanggal;
			$data['penandatanganan'] = $surat->penandatanganan;
			$data['company_name'] = $surat->company_name;
	        $data['nama_klien'] = $surat->nama_klien;
	        //$data['init_klien'] = $surat->init_klien;
	        $data['alamat'] = $surat->alamat;
	        $data['pembuat'] = $surat->pembuat;
	        $data['keterangan'] = $surat->keterangan;
	        $data['change_no'] = $surat->change_no;
	        $data['partner'] = $surat->partner;
	        $data['manager'] = $surat->manager;
	        $data['nama_peminta_no'] = $surat->nama_peminta_no;
	        $data['proses_flag'] = $surat->proses_flag;
	        $data['tanggal_laporan'] = $surat->tanggal_laporan;
        }
        
        //\CI::form_validation()->set_rules('type', 'Tipe', 'trim|required');
        //\CI::form_validation()->set_rules('tujuan', 'Tujuan', 'trim|required');              
        //\CI::form_validation()->set_rules('init_klien', 'Inisial klien', 'trim|required');
        //\CI::form_validation()->set_rules('alamat', 'Alamat', 'trim|required');
        \CI::form_validation()->set_rules('keterangan', 'Keterangan', 'trim|required');
        \CI::form_validation()->set_rules('partner', 'Partner', 'trim|required');
        \CI::form_validation()->set_rules('manager', 'Manager', 'trim|required');
		\CI::form_validation()->set_rules('nama_klien', 'Nama klien', 'trim|required');
		\CI::form_validation()->set_rules('tanggal', 'Tanggal', 'trim|required');
		\CI::form_validation()->set_rules('penandatanganan', 'Penandatanganan Ke-', 'trim|required');
        \CI::form_validation()->set_rules('nama_peminta_no', 'Nama Peminta No', 'trim|required');
        \CI::form_validation()->set_rules('proses_flag', 'Proses', 'trim|required');
        \CI::form_validation()->set_rules('tanggal_laporan', 'Tanggal Laporan', 'trim|required');
		\CI::form_validation()->set_rules('kap', 'Nama cabang', 'trim|required');
		\CI::form_validation()->set_rules('alamat', 'Alamat Klien', 'trim|required');
		\CI::form_validation()->set_rules('keterangan', 'Keterangan', 'trim|required');
                        
        if (\CI::form_validation()->run() == FALSE)
        {
            $this->view('opini_form', $data);
        }else
        {
            $save['id'] = $id;            
	        $save['no'] = \CI::input()->post('no');
	        //$save['type'] = \CI::input()->post('type');
	        $save['tujuan'] = \CI::input()->post('tujuan');
	        $save['kap'] = \CI::input()->post('kap');
	        $save['tanggal'] = \CI::input()->post('tanggal');
			$save['penandatanganan'] = \CI::input()->post('penandatanganan');
	        $save['nama_klien'] = \CI::input()->post('nama_klien');
	        //$save['init_klien'] = \CI::input()->post('init_klien');
	        $save['alamat'] = \CI::input()->post('alamat');
	        //$save['pembuat'] = \CI::input()->post('descr');
	        $save['keterangan'] = \CI::input()->post('keterangan');
	        $save['partner'] = \CI::input()->post('partner');
	        $save['manager'] = \CI::input()->post('manager');
	        $save['nama_peminta_no'] = \CI::input()->post('nama_peminta_no');
	        $save['proses_flag'] = \CI::input()->post('proses_flag');
	        $save['tanggal_laporan'] = \CI::input()->post('tanggal_laporan');
			
			if(count(\CI::M_opini()->find_suratopini_byklienttdyear($save['nama_klien'], $save['penandatanganan'], $save['tanggal'])) > 0 &&  !$save['id'])
			{
				$data['error'] = 'Penandatanganan dan Nama Klien yang sama ditahun yang sama tidak diperbolehkan';
				$this->view('opini_form', $data);
				exit;
			}
	        
	        $bulantahun = \CI::M_opini()->get_year_month($save['tanggal']);
	        //$bulansurat = getBilRomawi($bulantahun['month']);
	        $tahunsurat = $bulantahun['year'];
	        
	        //revisi number
			/*
	        if(!$id)
	        {
	        	$save['change_no'] = 1;
	        }else {
	        	$save['change_no'] = \CI::input()->post('change_no') + 1;
	        }
			*/
	        
	        //$changes_romawi = getBilRomawi($save['penandatanganan']);
	        
	        $save['no_op'] = $this->get_nosurat($save['no'], $save['kap'],  $save['partner'], $save['penandatanganan'], $tahunsurat);
                        
            \CI::M_opini()->save($save);
            
            \CI::session()->set_flashdata('message', 'Surat opini audit sudah disimpan.');
                  
            redirect('opini/index');  
        }
    }
    
    public function delete()
    {
        $id = \CI::input()->post('itemID'); 
        
        if ($id)
        { 
            $surat = \CI::M_opini()->get_surat_byid($id);
            if (!$surat)
            {
                \CI::session()->set_flashdata('error', lang('error_not_found'));
                redirect('opini/index');
            }
            else
            {
                \CI::M_opini()->delete($id);
                \CI::session()->set_flashdata('message', 'Data surat sudah dihapus');
                redirect('opini/index');
            }
        }
        else
        {
            \CI::session()->set_flashdata('error', lang('error_not_found'));
            redirect('opini/index');
        }
    }
        
    public function get_nosurat($no, $kap, $partner, $change_no, $thn)
    {
    	$str = '';
    	//sprintf('%03d', $no);
    	$str .= sprintf('%03d', $no).'/'.$kap.'/'.$partner.'/'.$change_no.'/'.$thn;
    
    	return $str;
    }
	
	public function namapt() {
		$nama_klien = \CI::input()->post('query');
		//$result = $this->Servicehistory_model->get_chassisno($vin);
		$result = \CI::M_opini()->get_nama_klien($nama_klien);
		$nama_pt[] = array();
		foreach($result as $rows):
			$nama_pt[] = $rows->nama_klien;
		endforeach;
		
		echo json_encode($nama_pt);
	}
	
	public function find_company() {
		$nama_klien = \CI::input()->post('kode_pt');
		$company = 0;
		$result = \CI::M_opini()->get_fixed_namapt($nama_klien);
		//echo \CI::db()->last_query(); exit;
		if($result)
			$company = $result->no_op;
		
		echo json_encode($company);
	}
	    
}