<?php namespace GoCart\Controller;

//use Ghunti\HighchartsPHP\Highchart;
//use Ghunti\HighchartsPHP\HighchartJsExpr;

class Reports_opinions extends Admin {

    function __construct()
    {
        parent::__construct();
        \CI::load()->helper(array('formatting'));
        \CI::load()->model(array('Variable', 'M_opini'));
    }
    
    public function laporanentrisurat()
    {
    	\CI::load()->helper(array('form','date'));
    	\CI::load()->library('form_validation');    	    
    	
    	$data['message'] = \CI::session()->flashdata('message');
    	    	
    	$data['page_title'] = 'LAPORAN SURAT OPINI';
    	$data['begin_period'] = date('Y-m-d');
    	$data['end_period'] = date('Y-m-d');
    	    	
    	$list_keyword[null] = 'Semua';
    	$list_keyword['no_op'] = 'Nomor Opini';
    	$list_keyword['partner'] = 'Partner';
    	$list_keyword['nama_klien'] = 'Nama Klien';
    	$list_keyword['createdby'] = 'Pembuat Surat';
    	$data['list_keyword'] = $list_keyword;
    	
    	$data['key_variable'] = null;
				
		\CI::form_validation()->set_rules('begin_period', 'Tanggal Awal', 'trim|required');
		\CI::form_validation()->set_rules('end_period', 'Tanggal Akhir', 'trim|required');
		//\CI::form_validation()->set_rules('key_variable', 'Filter Pencarian', 'trim|required');
		//\CI::form_validation()->set_rules('key_value', 'Keyword Value', 'trim|required');
		
        if (\CI::form_validation()->run() == FALSE)
        {
            $this->view('vw_exportopini', $data);
        }
        else
        {
			$begin = \CI::input()->post('begin_period');
			$end = \CI::input()->post('end_period');
            $filter = \CI::input()->post('key_variable');
			$value = \CI::input()->post('key_value');
			
            $export = \CI::input()->post('export');
            if($export == 'pdf')
            {
            	//create pdf filenya
                $file = $this->export_opini_pdf($begin, $end, $filter, $value);
                
                //open modal
                echo '<script>';
                echo 'window.open("'.base_url('uploads/export/'.$file).'");';
                echo 'window.history.back();';
                echo '</script>';
                
                //cleanup
                /*
                $filename = 'uploads/export/'.$file;
                //delete the existing file if needed
                if(file_exists($filename))
                {
                	unlink($filename);
                }
                */
                
                exit;                                
            }else {
                $this->export_opini_xls($begin, $end, $filter, $value);
                
                exit;
            }            
        } 
    }
	
	public function export_opini_pdf($begin, $end, $filter, $value)
    {
    	$data['opini'] = \CI::M_opini()->get_export_surat($begin, $end, $filter, $value);
    	$file = 'report_opini_'.time().'.pdf';
    	
    	// init HTML2PDF
    	$html2pdf = new \HTML2PDF('L', 'A4', 'en');
    	$html2pdf->setDefaultFont('arial');
    	 
    	// get the HTML
    	ob_start();
    	$this->partial('export_opini_pdf', $data);
    	$content = ob_get_clean();
    	 
    	$html2pdf->writeHTML($content);
    	 
    	// send the PDF
    	$html2pdf->Output(FCPATH . '/uploads/export/'.$file, 'F');
    	return $file;
    }
    
    public function export_opini_xls($begin, $end, $filter, $value)
    {
    	$opini = \CI::M_opini()->get_export_surat($begin, $end, $filter, $value);
    	$file = 'report_opini_'.time().'.xls';
    	
    	//$dataentri = \CI::Reports()->laporankinerjaentri($tahun);
    
    	// Create new PHPExcel object
    	$objPHPExcel = new \PHPExcel();
    
    	// Set document properties
    	$objPHPExcel->getProperties()->setCreator("Kreston Indonesia");
    	 
    	$objPHPExcel->setActiveSheetIndex(0);
    
    	$objPHPExcel->setActiveSheetIndex(0)
    	->setCellValue('A2', 'NO.ALL')
    	->setCellValue('B2', 'NO.URUT')
    	->setCellValue('C2', 'NO OPINI/LAPORAN')
    	->setCellValue('D2', 'NAMA PERUSAHAAN')
    	->setCellValue('E2', 'MANAGER')
    	->setCellValue('F2', 'TTD CHANGE')
    	->setCellValue('G2', 'TANGGAL PERMINTAAN NO')
    	->setCellValue('H2', 'NAMA PEMINTA NO')
    	->setCellValue('I2', 'PROSES/FINAL')
    	->setCellValue('J2', 'TANGGAL LAPORAN')
    	->setCellValue('K2', 'KELENGKAPAN FORM');
    	
    	$col = 3;
    	$i = 1;
    	foreach($opini as $rows)
    	{
    		$objPHPExcel->setActiveSheetIndex(0)
    		->setCellValue('A'.$col, $i)
    		->setCellValue('B'.$col, $rows->no)
    		->setCellValue('C'.$col, $rows->no_op)
    		->setCellValue('D'.$col, $rows->partner)
    		->setCellValue('E'.$col, $rows->manager)
    		->setCellValue('F'.$col, $rows->change_no)
    		->setCellValue('G'.$col, $rows->tanggal)
    		->setCellValue('H'.$col, $rows->nama_peminta_no)
    		->setCellValue('I'.$col, $rows->proses_flag)
    		->setCellValue('J'.$col, $rows->tanggal_laporan)
    		->setCellValue('K'.$col, '');
    	
    		$col++;
    		$i++;
    	}
    	    
    	//auto width
    	$objPHPExcel->getActiveSheet()->getColumnDimension()->setAutoSize(true);
    
    	//judul
    	$objPHPExcel->getActiveSheet()->mergeCells("A1:K1");
    	$objPHPExcel->getActiveSheet()->getStyle("A1:K1")->getFont()->setBold(true);
    	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'EXPORT SURAT OPINI AUDIT');
    
    	//border
    	$style = array(
    			'alignment' => array(
    				'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
    			),
    			'font' => array(
    				'bold' => true
    			)
    	);
    	$first = 'A1';
    	$last = 'K2';
    	$objPHPExcel->getActiveSheet()->getStyle("$first:$last")->applyFromArray($style);
    
    	$style = array(
    			'borders' => array(
    				'allborders' => array(
    					'style' => \PHPExcel_Style_Border::BORDER_THIN
    				)
    			)
    	);
    	$first = 'A2';
    	$last = 'K'.($col-1);
    	$objPHPExcel->getActiveSheet()->getStyle("$first:$last")->applyFromArray($style);
    	
    	//Just before saving de Excel document, you do this:
    	//\PHPExcel_Shared_Font::setAutoSizeMethod(\PHPExcel_Shared_Font::AUTOSIZE_METHOD_EXACT);
    	
    	//We get the util used space on worksheet. Change getActiveSheet to setActiveSheetIndex(0) to choose the sheet you want to autosize. Iterate thorugh'em if needed.
    	//We remove all digits from this string, which cames in a form of "A1:G24".
    	//Exploding via ":" to get a 2 position array being 0 fisrt used column and 1, the last used column.
    	$cols = explode(":", trim(preg_replace('/\d+/u', '', $objPHPExcel->getActiveSheet()->calculateWorksheetDimension())));
    	
    	$col = $cols[0]; //first util column with data
    	$end = ++$cols[1]; //last util column with data +1, to use it inside the WHILE loop. Else, is not going to use last util range column.
    	while($col != $end){
    		$objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
    	
    		$col++;
    	}
        
    	// Set active sheet index to the first sheet, so Excel opens this as the first sheet
    	$objPHPExcel->setActiveSheetIndex(0);
    
    	// Redirect output to a client�s web browser (Excel5)
    	header('Content-Type: application/vnd.ms-excel');
    	header('Content-Disposition: attachment;filename="'.$file);
    	header('Cache-Control: max-age=0');
    	// If you're serving to IE 9, then the following may be needed
    	header('Cache-Control: max-age=1');
    
    	$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    	$objWriter->save('php://output');
    	exit;
    }
                
}
