<?php
$routes[] = ['GET|POST', '/opini/index/[:orderBy]?/[:orderDir]?/[:code]?/[i:page]?', 'GoCart\Controller\Opinions#index'];
$routes[] = ['GET|POST', '/opini/form/[i:id]?', 'GoCart\Controller\Opinions#form'];
$routes[] = ['GET|POST', '/opini/delete/[i:itemID]?', 'GoCart\Controller\Opinions#delete'];
$routes[] = ['GET|POST', '/report-opini/laporan-entrisurat', 'GoCart\Controller\Reports_opinions#laporanentrisurat'];
#$routes[] = ['GET|POST', '/opini/namapt', 'GoCart\Controller\Opinions#namapt'];
$routes[] = ['GET|POST', '/opini/find_company', 'GoCart\Controller\Opinions#find_company'];