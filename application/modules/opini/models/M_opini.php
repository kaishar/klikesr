<?php
Class M_opini extends MY_Model
{
    public function get_surat($search=false, $sort_by='', $sort_order='desc', $limit=0, $offset=0)
    {
		\CI::db()->select('opinions.id,
				  opinions.no_op,
				  opinions.no,
				  opinions.type,
				  opinions.tujuan,
				  opinions.kap,
				  opinions.tanggal,
				  opinions.penandatanganan,
				  opinions.init_klien,
				  company.company_name nama_klien,
				  opinions.alamat,
				  opinions.pembuat,
				  opinions.keterangan,
				  opinions.is_trash,
				  opinions.partner,
				  opinions.manager,
				  opinions.nama_peminta_no,
				  opinions.proses_flag,
				  opinions.tanggal_laporan,
				  opinions.change_no,
				  opinions.createdby,
				  opinions.createdat');
		
        if ($search)
        {
            if(!empty($search->term))
            {
                $this->get_surat_searchlike($search->term);
            }
        }
        
        CI::db()->where('opinions.is_trash','0');
		CI::db()->join('company', 'company.id = opinions.nama_klien', 'left');
        
        if($limit>0)
        {
            \CI::db()->limit($limit, $offset);
        }
        
        if(!empty($sort_by))
        {
            \CI::db()->order_by($sort_by, $sort_order);
        }

        return \CI::db()->get('opinions')->result();
    }

    public function get_surat_count($search=false)
    {
        if ($search)
        {
            if(!empty($search->term))
            {
                $this->get_surat_searchlike($search->term);
            }
        }
        
        CI::db()->where('opinions.is_trash','0');
		CI::db()->join('company', 'company.id = opinions.nama_klien', 'left');
		
        return CI::db()->count_all_results('opinions');
    }
	
    private function get_surat_searchlike($str)
    {
        //support multiple words
        $term = explode(' ', $str);

        foreach($term as $t)
        {
            $not = '';
            $operator = 'OR';
            if(substr($t,0,1) == '-')
            {
                $not = 'NOT ';
                $operator = 'AND';
                //trim the - sign off
                $t = substr($t,1,strlen($t));
            }

            $like = '';
            $like .= "( opinions.no_op ".$not."LIKE '%".CI::db()->escape_like_str($t)."%' ";
            $like .= $operator." opinions.partner ".$not."LIKE '%".CI::db()->escape_like_str($t)."%' ";
            $like .= $operator." opinions.tanggal ".$not."LIKE '%".CI::db()->escape_like_str($t)."%' ";
            $like .= $operator." company.company_name ".$not."LIKE '%".CI::db()->escape_like_str($t)."%' ";
            $like .= $operator." opinions.keterangan ".$not."LIKE '%".CI::db()->escape_like_str($t)."%' ";
            $like .= $operator." opinions.createdby ".$not."LIKE '%".CI::db()->escape_like_str($t)."%' ";
            $like .= $operator." opinions.createdat ".$not."LIKE '%".CI::db()->escape_like_str($t)."%' )";

            CI::db()->where($like);
        }
    }
	
    public function save($surats)
    {
        $admin = $this->session->userdata('admin');
        $surats['is_trash'] = 0;
        
        if($surats['id'])
        {
            $surats['updatedby'] = $admin['username'];
            $surats['updatedat'] = date('Y-m-d H:i:s');
            
            CI::db()->where('id', $surats['id']);
            CI::db()->update('opinions', $surats);
            return $surats['id'];
        }
        else
        {
            $surats['createdby'] = $admin['username'];
            $surats['createdat'] = date('Y-m-d H:i:s');
            
            $this->db->insert('opinions', $surats);
            return $this->db->insert_id();;
        }
    }

    public function delete($id)
    {
        $admin = $this->session->userdata('admin');
        
        $save['is_trash'] = 1;
        $save['updatedby'] = $admin['username'];
        $save['updatedat'] = date('Y-m-d H:i:s');
            
        CI::db()->where('id', $id);
        CI::db()->update('opinions', $save);
    }
    	
    public function get_surat_byid($id)
    {
        $result = CI::db()->get_where('opinions', array('id'=>$id));
        return $result->row();
    }
        
    public function get_year_month($tanggal)
    {
    	$_sql = "SELECT 
    			DATE_FORMAT(?, '%y') as year,
    			MONTH(?) AS 'month'";
    	$result = CI::db()->query($_sql, array($tanggal, $tanggal));
    	
    	return $result->row_array();
    }
    
    public function get_surat_intrash($search=false, $sort_by='', $sort_order='desc', $limit=0, $offset=0)
    {
    	if ($search)
    	{
    		if(!empty($search->term))
    		{
    			$this->get_surat_searchlike($search->term);
    		}
    	}
    
    	CI::db()->where('is_trash','1');
    
    	if($limit>0)
    	{
    		\CI::db()->limit($limit, $offset);
    	}
    
    	if(!empty($sort_by))
    	{
    		\CI::db()->order_by($sort_by, $sort_order);
    	}
    
    	return \CI::db()->get('opinions')->result();
    }
    
    public function get_surat_intrash_count($search=false)
    {
    	if ($search)
    	{
    		if(!empty($search->term))
    		{
    			$this->get_surat_searchlike($search->term);
    		}
    	}
    
    	CI::db()->where('is_trash','1');
    	return CI::db()->count_all_results('surats');
    }
    
    public function get_export_surat($begin, $end, $filter, $value)
    {
    	$sql = "select 
    				opinions.no_op,
    				opinions.no,
    				opinions.kap,
    				opinions.tanggal,
					opinions.penandatanganan,
    				company.company_name nama_klien,
    				opinions.alamat,
    				opinions.pembuat,
    				opinions.keterangan,
    				opinions.partner,
    				opinions.manager,
    				opinions.nama_peminta_no,
    				variable.DESCRIPTION as proses_flag,
    				opinions.tanggal_laporan,
    				opinions.change_no
    			from opinions
				left join company on company.id = opinions.nama_klien
    			left join variable on variable.VALUE = opinions.proses_flag and variable.VARIABLE = 'PROSES_FLAG'
    			where 
    				opinions.is_trash = 0 
    				and (opinions.tanggal between ? and ?)";
    				    	 
    	if(isset($filter) && isset($value)) {
    		$sql .= " and opinions.".$filter." like ?";
    	
    		$query = $this->db->query($sql, array($begin, $end, '%'.$value.'%'));
    	}else {
    		$query = $this->db->query($sql, array($begin, $end));
    	}
    	
    	//echo $this->db->last_query(); exit;
    	return $query->result();
    }
    
    public function get_next_nopini()
    {
    	$sql = "select 
    				ifnull(MAX(CAST(no as SIGNED INTEGER)),0) as no 
    			from opinions
				where RIGHT(no_op, 2) = (DATE_FORMAT(CURDATE(), '%y'))";
    	
    	$query = $this->db->query($sql);
    	
    	if($query->num_fields() > 0)
    	{
    		$row = $query->row_array();
    		$temp = $row["no"] + 1; //next key*/
    	}else { //belum ada recordnya
    		$temp = 1;
    	}
    	
    	return $temp;
    }
	
	public function get_nama_klien($nama_klien)
	{	
		$sql = "select 
					nama_klien
				from opinions
				where nama_klien like ?";
		
		$query = $this->db->query($sql, array($nama_klien.'%'));
		return $query->result();
	}
	
	public function get_fixed_namapt($nama_klien)
	{
		$sql = "select 
					no_op
				from opinions
				where nama_klien = ?
				limit 1";
		
		$query = $this->db->query($sql, array($nama_klien));
		return $query->row();
	}
	
	public function find_opinicompany_byid($id)
	{
		$sql = "select 
				  o.id,
				  o.no_op,
				  o.no,
				  o.type,
				  o.tujuan,
				  o.kap,
				  o.tanggal,
				  o.penandatanganan,
				  o.init_klien,
				  o.nama_klien,
				  c.company_name,
				  o.alamat,
				  o.pembuat,
				  o.keterangan,
				  o.is_trash,
				  o.partner,
				  o.manager,
				  o.nama_peminta_no,
				  o.proses_flag,
				  o.tanggal_laporan,
				  o.change_no
				from opinions o
				left join company c on o.nama_klien = c.id
				where o.id = ?";
		$result = CI::db()->query($sql, array($id));
		return $result->row();
	}
	
	public function find_suratopini_byklienttdyear($klien, $ttd, $tanggal)
	{
		$sql = "select id from opinions where nama_klien = ? and penandatanganan = ? and YEAR(tanggal) = ?";
		
		$result = CI::db()->query($sql, array($klien, $ttd, $tanggal));
		return $result->result();
	}
   
}
