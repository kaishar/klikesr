<?php
Class M_review extends MY_Model
{
    public function get_surat($search=false, $sort_by='', $sort_order='desc', $limit=0, $offset=0)
    {
        if ($search)
        {
            if(!empty($search->term))
            {
                $this->get_surat_searchlike($search->term);
            }
        }
        
        CI::db()->where('is_trash','0');
        
        if($limit>0)
        {
            \CI::db()->limit($limit, $offset);
        }
        
        if(!empty($sort_by))
        {
            \CI::db()->order_by($sort_by, $sort_order);
        }

        return \CI::db()->get('rev_opinions')->result();
    }

    public function get_surat_count($search=false)
    {
        if ($search)
        {
            if(!empty($search->term))
            {
                $this->get_surat_searchlike($search->term);
            }
        }
        
        CI::db()->where('is_trash','0');
        return CI::db()->count_all_results('rev_opinions');
    }
	
    private function get_surat_searchlike($str)
    {
        //support multiple words
        $term = explode(' ', $str);

        foreach($term as $t)
        {
            $not = '';
            $operator = 'OR';
            if(substr($t,0,1) == '-')
            {
                $not = 'NOT ';
                $operator = 'AND';
                //trim the - sign off
                $t = substr($t,1,strlen($t));
            }

            $like = '';
            $like .= "( rev_opinions.no_re ".$not."LIKE '%".CI::db()->escape_like_str($t)."%' ";
            $like .= $operator." rev_opinions.tanggal ".$not."LIKE '%".CI::db()->escape_like_str($t)."%' ";
            $like .= $operator." rev_opinions.nama_klien ".$not."LIKE '%".CI::db()->escape_like_str($t)."%' ";
            $like .= $operator." rev_opinions.keterangan ".$not."LIKE '%".CI::db()->escape_like_str($t)."%' ";
            $like .= $operator." rev_opinions.createdby ".$not."LIKE '%".CI::db()->escape_like_str($t)."%' ";
            $like .= $operator." rev_opinions.createdat ".$not."LIKE '%".CI::db()->escape_like_str($t)."%' )";

            CI::db()->where($like);
        }
    }
	
    public function save($surats)
    {
        $admin = $this->session->userdata('admin');
        $surats['is_trash'] = 0;
        
        if($surats['id'])
        {
            $surats['updatedby'] = $admin['username'];
            $surats['updatedat'] = date('Y-m-d H:i:s');
            
            CI::db()->where('id', $surats['id']);
            CI::db()->update('rev_opinions', $surats);
            return $surats['id'];
        }
        else
        {
            $surats['createdby'] = $admin['username'];
            $surats['createdat'] = date('Y-m-d H:i:s');
            
            $this->db->insert('rev_opinions', $surats);
            return $this->db->insert_id();;
        }
    }

    public function delete($id)
    {
        $admin = $this->session->userdata('admin');
        
        $save['is_trash'] = 1;
        $save['updatedby'] = $admin['username'];
        $save['updatedat'] = date('Y-m-d H:i:s');
            
        CI::db()->where('id', $id);
        CI::db()->update('rev_opinions', $save);
    }
    	
    public function get_surat_byid($id)
    {
        $result = CI::db()->get_where('rev_opinions', array('id'=>$id));
        return $result->row();
    }
        
    public function get_year_month($tanggal)
    {
    	$_sql = "SELECT 
    			DATE_FORMAT(?, '%y') as year,
    			MONTH(?) AS 'month'";
    	$result = CI::db()->query($_sql, array($tanggal, $tanggal));
    	
    	return $result->row_array();
    }
    
    public function get_surat_intrash($search=false, $sort_by='', $sort_order='desc', $limit=0, $offset=0)
    {
    	if ($search)
    	{
    		if(!empty($search->term))
    		{
    			$this->get_surat_searchlike($search->term);
    		}
    	}
    
    	CI::db()->where('is_trash','1');
    
    	if($limit>0)
    	{
    		\CI::db()->limit($limit, $offset);
    	}
    
    	if(!empty($sort_by))
    	{
    		\CI::db()->order_by($sort_by, $sort_order);
    	}
    
    	return \CI::db()->get('rev_opinions')->result();
    }
    
    public function get_surat_intrash_count($search=false)
    {
    	if ($search)
    	{
    		if(!empty($search->term))
    		{
    			$this->get_surat_searchlike($search->term);
    		}
    	}
    
    	CI::db()->where('is_trash','1');
    	return CI::db()->count_all_results('rev_opinions');
    }
    
    public function get_export_surat($begin, $end, $filter, $value)
    {
    	$sql = "select * from rev_opinions
    			where 
    				is_trash = 0 
    				and (tanggal between ? and ?)";
    	
    	if(isset($filter) && isset($value)) {
    		$sql .= " and ".$filter." like ?";
    		
    		$query = $this->db->query($sql, array($begin, $end, '%'.$value.'%'));
    	}else {
    		$query = $this->db->query($sql, array($begin, $end));
    	}
    	
    	//echo $this->db->last_query();
    	return $query->result();
    }
    
    public function get_next_nreview()
    {
    	$sql = "select 
    				ifnull(MAX(CAST(no as SIGNED INTEGER)),0) as no 
    			from rev_opinions
				where RIGHT(no_re, 2) = (DATE_FORMAT(CURDATE(), '%y'))";
    	
    	$query = $this->db->query($sql);
    	
    	if($query->num_fields() > 0)
    	{
    		$row = $query->row_array();
    		$temp = $row["no"] + 1; //next key*/
    	}else { //belum ada recordnya
    		$temp = 1;
    	}
    	
    	return $temp;
    }
   
}
