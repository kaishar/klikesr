<style type="text/css">
<!--
#surats {
    font-family: Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

#surats td, #surats th {
	font-size: 10px;
    border: 1px solid #333;
    padding: 8px;
}

#surats tr:nth-child(even){background-color: #f2f2f2;}

#surats tr:hover {background-color: #ddd;}

#surats th {
    padding-top: 6px;
    padding-bottom: 6px;
    text-align: center;
    background-color: #ccc;
    color: #000;
}
-->
</style>
<page style="font-size: 10pt">
<h4 style="text-align: center;">EXPORT SURAT OPINI AUDIT</h4>
<table id="surats">
	<thead>
		<tr>
			<th style="text-align: center; width: 5%;">NO.ALL</th>
			<th style="text-align: center; width: 7%;">NO.URUT</th>
			<th style="width: 10%;">NO.OPINI/LAPORAN</th>
			<th style="width: 10%;">NAMA PERUSAHAAN</th>
			<th style="width: 10%;">MANAGER</th>
			<th style="width: 5%;">PENANDATANGANAN KE-</th>
			<th style="width: 8%;">TANGGAL PERMINTAAN NO</th>
			<th style="width: 10%;">NAMA PEMINTA NO</th>
			<th style="width: 10%;">PROSES/FINAL</th>
			<th style="width: 5%;">TANGGAL LAPORAN</th>
			<th style="width: 18%;">KELENGKAPAN FORM</th>
		</tr>
	</thead>
	<tbody>
		<?php
			$i=1;
			foreach($opini as $rows): 
		?>
		<tr>
			<td style="text-align: center;"><?=$i?></td>
			<td style="text-align: center;"><?=$rows->no?></td>
			<td><?=$rows->no_op?></td>
			<td><?=$rows->nama_klien?></td>
			<td><?=$rows->manager?></td>
			<td><?=$rows->penandatanganan?></td>
			<td><?=$rows->tanggal?></td>
			<td><?=$rows->nama_peminta_no?></td>
			<td><?=$rows->proses_flag?></td>
			<td><?=$rows->tanggal_laporan?></td>
			<td><?=$rows->keterangan?></td>
		</tr>
		<?php
			$i++;
			endforeach; 
		?>                                
	</tbody>
</table>

</page>