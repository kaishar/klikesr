<style type="text/css">
<!--
#surats {
    font-family: "Raleway", Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

#surats td, #surats th {
	font-size: 10px;
    border: 1px solid #333;
    padding: 8px;
}

#surats tr:nth-child(even){background-color: #f2f2f2;}

#surats tr:hover {background-color: #ddd;}

#surats th {
    padding-top: 6px;
    padding-bottom: 6px;
    text-align: center;
    background-color: #ccc;
    color: #000;
}
-->
</style>
<page style="font-size: 10pt">
<h4 style="text-align: center;">EXPORT SURAT OPINI AUDIT</h4>
<table id="surats">
	<thead>
		<tr>
			<th>NO.URUT</th>
			<th>NO.OPINI</th>
			<th>TIPE</th>
			<th>TUJUAN</th>
			<th>TANGGAL</th>
			<th>NAMA KLIEN</th>
			<th>ALAMAT</th>
			<th>PEMBUAT</th>
		</tr>
	</thead>
	<tbody>
		<?php
			$i=1;
			foreach($opini as $rows): 
		?>
		<tr>
			<td style="text-align: center;"><?=$i?></td>
			<td><?=$rows->no_op?></td>
			<td><?=$rows->type?></td>
			<td><?=$rows->tujuan?></td>
			<td><?=$rows->tanggal?></td>
			<td><?=$rows->nama_klien?></td>
			<td><?=$rows->alamat?></td>
			<td><?=$rows->createdby?></td>
		</tr>
		<?php
			$i++;
			endforeach; 
		?>                                
	</tbody>
</table>

</page>