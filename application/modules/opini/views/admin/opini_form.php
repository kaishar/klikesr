<div class="content-wrapper" id="main">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php
                    //lets have the flashdata overright "$message" if it exists
                    if(CI::session()->flashdata('message'))
                    {
                        $message = CI::session()->flashdata('message');
                    }

                    if(CI::session()->flashdata('error'))
                    {
                        $error = CI::session()->flashdata('error');
                    }

                    if(function_exists('validation_errors') && validation_errors() != '')
                    {
                        $error = validation_errors();
                    }
                ?>

                <div id="js_error_container" class="alert alert-error" style="display:none;">
                    <p id="js_error"></p>
                </div>

                <div id="js_note_container" class="alert alert-note" style="display:none;"></div>

                <?php if (!empty($message)): ?>
                    <div class="alert alert-success" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <?php echo $message; ?>
                    </div>
                <?php endif; ?>

                <?php if (!empty($error)): ?>
                    <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <?php echo $error; ?>
                    </div>
                <?php endif; ?>                 
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h1 class="page-head-line">SURAT OPINI AUDIT FORM</h1>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <strong>Add Surat Opini Audit</strong>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="text-left">
                                    <a href="<?=site_url('opini/index/')?>" class="btn btn-primary"><i class="fa fa-list-alt"></i>&nbsp;<?php echo ('List Surat Opini Audit'); ?></a>                                    
                                </div>
                            </div>                                                   
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-12">
                            <?php 
                                $attributes = array('class' => 'form-horizontal');
                                echo form_open('opini/form/'.$id, $attributes); 
                            ?>
                            <div class="form-group">
                                <label for="no_surat" class="col-sm-3 control-label">Nomor Opini</label>
                                <div class="col-sm-9"><h5><strong><?=$no_op?></strong></h5></div>
                            </div>
                            <div class="form-group">
                                <label for="no" class="col-sm-3 control-label">Penentuan Nomor</label>
                                <div class="col-sm-9">
                                	<input type="text" value="<?=$no?>" class="form-control" style="width: 30%" disabled>
                                	<input type="hidden" name="no" id="no" value="<?=$no?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="nama_klien" class="col-sm-3 control-label">Partner *</label>
                                <div class="col-sm-9">
                                <?php
									if($disable) {
										$attributes = array('id' => 'partner',
											'class' => 'form-control',
											'style' =>'width: 30%;',
											'disabled' => '');
										echo form_dropdown('', $list_partner, assign_value('partner', $partner), $attributes); 
										echo '<input type="hidden" name="partner" id="partner" value="'.assign_value('partner', $partner).'">';
									}else {
										$attributes = array('id' => 'partner',
											'class' => 'form-control',
											'style' =>'width: 30%;');
											
										echo form_dropdown('partner', $list_partner, assign_value('partner', $partner), $attributes); 
									}                                    
                                ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="manager" class="col-sm-3 control-label">Manager *</label>
                                <div class="col-sm-9">		
									<input type="text" name="manager" value="<?=assign_value('manager', $manager)?>" class="form-control" style="width:40%;" <?=$disable?'readonly':''?>>
                                </div>
                            </div>
                            <div class="form-group">								
								<label for="nama_klien" class="col-sm-3 control-label">Nama Klien (Tanpa Prefik PT.) *</label>
								<div class="col-sm-3">
									<input type="text" id="nama_pt" name="nama_pt" value="<?=assign_value('nama_pt', $company_name)?>" class="form-control" readonly="">
									<input type="hidden" id="nama_klien" name="nama_klien" value="<?=assign_value('nama_klien', $nama_klien)?>" class="form-control">
								</div>
								<div class="col-sm-2">
									<button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal" <?=$disable?'disabled':''?>><b>Cari</b> <i class="fa fa-search"></i></button>
								</div>
								<div class="col-sm-4">
									<div id="log" class="alert alert-danger"></div>
								</div>							
                            </div>
							<div class="form-group">
                                <label for="tanggal" class="col-sm-3 control-label">Tanggal Permintaan *</label>
                                <div class="col-sm-9">
                                	<div class="dateField">
		                                <input type="date" value="<?=assign_value('tanggal', $tanggal)?>" id="tanggal" name="tanggal" class="dateInput" readonly="">
		                            <?php    
										//if(!$disable) {
									?>
											<!--<a title="Open Calendar" onclick="javascript: dateType = 'date'; openCalendar('tanggal');" style="cursor: pointer;" class="calendarLink notAJAX"></a>-->
									<?php
										//}
									?>
									</div>                                
                                </div>
                            </div>
							<div class="form-group">
                                <label for="penandatanganan" class="col-sm-3 control-label">Penandatanganan Ke-  *</label>
                                <div class="col-sm-9">
                                <?php 
									if($disable) {
										$attributes = array('id' => 'penandatanganan',
											'class' => 'form-control',
											'style' =>'width: 20%;',
											'disabled' => '');
										echo form_dropdown('', $list_penandatanganan, assign_value('penandatanganan', $penandatanganan), $attributes);
										echo '<input type="hidden" name="penandatanganan" id="penandatanganan" value="'.assign_value('penandatanganan', $penandatanganan).'">';
									}else {
										$attributes = array('id' => 'penandatanganan',
											'class' => 'form-control',
											'style' =>'width: 20%;');		
										echo form_dropdown('penandatanganan', $list_penandatanganan, assign_value('penandatanganan', $penandatanganan), $attributes);
									}
																		
                                       
                                ?>
                                </div>
                            </div>
							<div class="form-group">
                                <label for="nama_peminta_no" class="col-sm-3 control-label">Nama Peminta Nomor *</label>
                                <div class="col-sm-9">
									<input type="text" name="nama_peminta_no" value="<?=assign_value('nama_peminta_no', $nama_peminta_no)?>" class="form-control" style="width:40%;">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="tujuan" class="col-sm-3 control-label">Proses/Final</label>
                                <div class="col-sm-9">
                                <?php    
									$attributes = array('id' => 'proses_flag',
										'class' => 'form-control',
										'style' =>'width: 30%;');
									
                                    echo form_dropdown('proses_flag', $list_proses_flag, assign_value('proses_flag', $proses_flag), $attributes);   
                                ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="tanggal" class="col-sm-3 control-label">Tanggal Laporan *</label>
                                <div class="col-sm-9">
                                	<div class="dateField">
		                                <input type="date" value="<?=assign_value('tanggal_laporan', $tanggal_laporan)?>" id="tanggal_laporan" name="tanggal_laporan" class="dateInput">
										<a title="Open Calendar" onclick="javascript: dateType = 'date'; openCalendar('tanggal_laporan');" style="cursor: pointer;" class="calendarLink notAJAX"></a>
									</div>                                
                                </div>
                            </div>                            
                            <div class="form-group">
                                <label for="type" class="col-sm-3 control-label">Nama Cabang *</label>
                                <div class="col-sm-9">
                                <?php   
									if($disable) {
										$attributes = array('id' => 'kap',
                                        'class' => 'form-control',
                                        'style' =>'width: 30%;',
										'disabled' => ''); 
										echo form_dropdown('', $list_kap, assign_value('kap', $kap), $attributes);   
										echo '<input type="hidden" name="kap" id="kap" value="'.assign_value('kap', $kap).'">';
									}else {
										$attributes = array('id' => 'kap',
                                        'class' => 'form-control',
                                        'style' =>'width: 30%;'); 		
										echo form_dropdown('kap', $list_kap, assign_value('kap', $kap), $attributes);   
									}
                                    
                                    
                                ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="alamat" class="col-sm-3 control-label">Alamat Klien *</label>
                                <div class="col-sm-9">
									<textarea id="alamat" name="alamat" class="form-control" rows="4" style="width:40%; resize:none;" <?=$disable?'readonly':''?>><?=assign_value('alamat', $alamat)?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="keterangan" class="col-sm-3 control-label">Keterangan *</label>
                                <div class="col-sm-9">
                                <?php 
                                    echo form_input(['name'=>'keterangan', 
                                            'value'=>assign_value('keterangan', $keterangan), 
                                            'class'=>'form-control',
                                            'maxlength'=>'256',
                                            'style'=>'width: 60%;']); 
                                ?>
                                </div>
                            </div>                             
                            <div class="form-group">
                                <div class="col-sm-9 col-sm-offset-3">
                                <?php
                                    if ($id) 
                                    {
                                ?>
                                    <td>
                                        <input class="button btn btn-primary" type="submit" value="Update" name="saveData">
                                        <input class="cancelButton button btn btn-primary" type="button" value="Batal" onclick="window.history.back()">
                                        <input class="button btn btn-danger btn-delete confirmSubmit" type="button" onclick="confSubmit('deleteForm', 'Apakah anda yakin akan menghapus data <?=$no_op?>?\n')" value="Hapus data">
                                    </td>
                                <?php
                                    }else {
                                ?>
                                    <td><input type="submit" value="Simpan" name="saveData" class="button btn btn-primary"></td>
                                <?php
                                    }
                                ?>
                                </div>
                            </div>
                            <input type="hidden" name="change_no" value="<?=assign_value('change_no', $change_no)?>">
                            </form>
                            <form action="<?=site_url('opini/delete')?>" id="deleteForm" method="post" style="display: inline;">
                                <input type="hidden" name="itemID" value="<?=$id?>" />
                            </form>  
                            <hr>
                            </div>
                        </div>
                    </div>
                    <!-- End  Kitchen Sink -->
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog" style="width:800px">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Lookup Nama Klien (Nama Perusahaan)</h4>
			</div>
			<div class="modal-body">
				<table id="lookup" class="table table-bordered table-hover table-striped">
					<thead>
						<tr>
							<th>Kode Perusahaan</th>
							<th>Nama Perusahaan</th>
						</tr>
					</thead>
					<tbody>
						<?php
							foreach($company as $rows):
						?>
							<tr class="pilih" data-kodeperusahaan="<?=$rows->id?>" data-namaperusahaan="<?=$rows->company_name?>">
								<td><?=$rows->id?></td>
								<td><?=$rows->company_name?></td>
							</tr>
						<?php
							endforeach;
						?>
					</tbody>
				</table>  
			</div>
		</div>
	</div>
</div>
<!-- CONTENT-WRAPPER SECTION END-->
