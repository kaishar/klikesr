<?php namespace GoCart\Controller;

class Home extends Front{

	public function __construct()
	{
		parent::__construct();
	
		\CI::load()->model(array('M_katalog', 'Variable', 'M_katalog_cover', 'M_katalog_file'));
		\CI::load()->model('Search');
	
		\CI::load()->helper(array('formatting'));
	}
	
    public function homepage()
    {
        //do we have a homepage view?
        if(file_exists(FCPATH.'themes/'.config_item('theme').'/views/homepage.php'))
        {
        	$data['koleksi_digital'] =  \CI::M_Katalog()->get_koleksi_ebooks();
        	//$data['list_koleksi'] = \CI::M_Katalog()->get_katalog(false, $sort_by='', $sort_order='DESC', $limit=0, $offset=0);
        	
        	$this->view('homepage', $data);
            return;
        }
        else
        {
            //if we don't have a homepage view, check for a registered homepage
            if(config_item('homepage'))
            {
                if(isset($this->pages['all'][config_item('homepage')]))
                {
                    //we have a registered homepage and it's active
                    $this->index($this->pages['all'][config_item('homepage')]->slug, false);
                    return;
                }
            }
        }

        // wow, we do not have a registered homepage and we do not have a homepage.php
        // let's give them something default to look at.
        $this->view('homepage_fallback');
    }

    public function show404()
    {
    	$this->partial('header');
        $this->partial('404');
        $this->partial('footer');
    }

    public function index($slug=false, $show_title=true)
    {

        $page = false;

        //this means there's a slug, lets see what's going on.
        foreach($this->pages['all'] as $p)
        {
            if($p->slug == $slug)
            {
                $page = $p;
                continue;
            }
        }

        if(!$page)
        {
            throw_404();
        }
        else
        {
            //create view variable
            $data['page_title'] = false;
            if($show_title)
            {
                $data['page_title'] = $page->title;
            }
            $data['meta'] = $page->meta;
            $data['seo_title'] = (!empty($page->seo_title))?$page->seo_title:$page->title;
            $data['page'] = $page;

            //load the view
            $this->view('page', $data);
        }
    }

    public function api($slug)
    {
        \CI::load()->language('page');

        $page = $this->Page_model->slug($slug);

        if(!$page)
        {
            $json = json_encode(['error'=>lang('error_page_not_found')]);
        }
        else
        {
            $json = json_encode($page);
        }

        $this->view('json', ['json'=>json_encode($json)]);
    }
    
    public function service()
    {
    	$SortBy = \CI::input()->post('strSortBy');
    	$SortDirection = \CI::input()->post('strSortDirection');
    	$Keyword = \CI::input()->post('strKeyword');
    	$PageNumber = \CI::input()->post('intPageNumber');
    
    	$length = 8;
    	if($PageNumber == 1) {
    		$start = 0;
    	}else {
    		$start = ($PageNumber-1) * $length;
    	}
    	 
    	//$data['response'] = 'false'; //Set default response
    	 
    	$query['data'] = \CI::M_katalog()->list_koleksi($Keyword, $SortBy, $SortDirection, $length, $start); //Search DB
    	$total = \CI::M_katalog()->list_koleksi_count($Keyword);
    	 
    	$query['TotalDataPage'] = ceil($total/$length);
    	//print_r($query); exit;
    	 
    	echo json_encode($query);
    }
}

/* End of file Home.php */
/* Location: ./GoCart/controllers/Home.php */