<?php namespace GoCart\Controller;

class Result extends Front{
	
	public function __construct()
	{
		parent::__construct();
	
		\CI::load()->model(array('M_katalog','M_katalog_file'));
		\CI::load()->model('Search');
	
		\CI::load()->helper(array('formatting'));
	}

    public function index($sort_by='controlnum',$sort_order='desc', $code=0, $page=0, $rows=3)
    {
    	\CI::load()->helper('form');
    	\CI::load()->helper('date');
    	
    	$data['message'] = \CI::session()->flashdata('message');
    	$data['code'] = $code;
    	$term = false;
    	    	
    	$post = \CI::input()->post(null, false);
     	if($post)
    	{
    		//if the term is in post, save it to the db and give me a reference
    		$term = json_encode($post);
    		$code = \CI::Search()->recordTerm($term);
    		$data['code'] = $code;
    		//reset the term to an object for use
    		$term   = (object)$post;
    	}
    	elseif ($code)
    	{
    		$term = \CI::Search()->getTerm($code);
    		$term = json_decode($term);
    	}
    	$data['term'] = $term;
    	$data['koleksi'] = \CI::M_katalog()->list_koleksi_filter($term, $sort_by, $sort_order, $rows, $page);
    	$data['total'] = \CI::M_katalog()->list_koleksi_filter_count($term);
    	
    	\CI::load()->library('pagination');
    	
    	$config['base_url'] = site_url('result/'.$sort_by.'/'.$sort_order.'/'.$code.'/');
    	$config['total_rows'] = $data['total'];
    	$config['per_page'] = $rows;
    	$config['uri_segment'] = 5;
    	$config['first_link'] = 'First';
    	$config['first_tag_open'] = '<li>';
    	$config['first_tag_close'] = '</li>';
    	$config['last_link'] = 'Last';
    	$config['last_tag_open'] = '<li>';
    	$config['last_tag_close'] = '</li>';
    	
    	$config['full_tag_open'] = '<div class="text-center"><ul class="pagination pagination-sm">';
    	$config['full_tag_close'] = '</ul></div>';
    	$config['cur_tag_open'] = '<li class="active"><a href="#">';
    	$config['cur_tag_close'] = '</a></li>';
    	
    	$config['num_tag_open'] = '<li>';
    	$config['num_tag_close'] = '</li>';
    	
    	$config['prev_link'] = '&laquo;';
    	$config['prev_tag_open'] = '<li>';
    	$config['prev_tag_close'] = '</li>';
    	
    	$config['next_link'] = '&raquo;';
    	$config['next_tag_open'] = '<li>';
    	$config['next_tag_close'] = '</li>';
    	
    	\CI::pagination()->initialize($config);
    	
    	$data['sort_by'] = $sort_by;
    	$data['sort_order'] = $sort_order;
    	
    	$this->partial('koleksi_result', $data);
    }   
    
    public function detail($controlnum)
    {
    	//echo $controlnum; exit;
    	$data['koleksi'] = \CI::M_katalog()->find_detail_katalog($controlnum);
    	$data['files'] = \CI::M_katalog_file()->find_files_bycontrolnum($controlnum);
    	$this->partial('koleksi_detail', $data);
    }
}

