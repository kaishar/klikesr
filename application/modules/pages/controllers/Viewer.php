<?php namespace GoCart\Controller;

class Viewer extends Front{

	public function __construct()
    {		
        parent::__construct();

        \CI::load()->model(array('M_katalog','M_katalog_file'));
        
        \CI::load()->helper(array('date', 'url'));
    }
	
	public function index()
    {
    	$controlnum = \CI::uri()->segment(2);
		$id = \CI::uri()->segment(3);
				
		$koleksi = \CI::M_katalog()->get_katalog_byid($controlnum);
		$data['title'] = $koleksi->title;
		
		$data['files'] = \CI::M_katalog_file()->find_dokumen_byid($controlnum, $id);
		
		if($data['files']['tipe_dokumen'] == 'PDF') {
			$this->partial('modal_pdf', $data);
		}else if($data['files']['tipe_dokumen'] == 'VIDEO') {
			$this->partial('modal_video', $data);
		}else if($data['files']['tipe_dokumen'] == 'HTML') {
			$this->partial('modal_flipone', $data);
		}else if($data['files']['tipe_dokumen'] == 'IMAGE') {
			$this->partial('modal_photo', $data);
		}
    }
}