<?php

$routes[] = ['GET', '/admin/pages', 'GoCart\Controller\AdminPages#index'];
$routes[] = ['GET|POST', '/admin/pages/form/[i:id]?', 'GoCart\Controller\AdminPages#form'];
$routes[] = ['GET|POST', '/admin/pages/link_form/[i:id]?', 'GoCart\Controller\AdminPages#link_form'];
$routes[] = ['GET|POST', '/admin/pages/delete/[i:id]?', 'GoCart\Controller\AdminPages#delete'];
$routes[] = ['GET|POST', '/page/[:slug]', 'GoCart\Controller\Page#index'];
$routes[] = ['GET|POST', '/download/filedigital/[*:filename]?', 'GoCart\Controller\Download#filedigital'];
$routes[] = ['GET|POST', '/home/service?', 'GoCart\Controller\Home#service'];
$routes[] = ['GET|POST', '/viewer/[:controlnum]?/[i:id]?', 'GoCart\Controller\Viewer#index'];
$routes[] = ['GET|POST', '/result/[:orderBy]?/[:orderDir]?/[:code]?/[i:page]?', 'GoCart\Controller\Result#index'];
$routes[] = ['GET|POST', '/detail/[:id]?', 'GoCart\Controller\Result#detail'];