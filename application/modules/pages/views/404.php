<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <title>Kreston Indonesia</title>
    <!-- BOOTSTRAP CORE STYLE  -->
    <!-- FONT AWESOME ICONS  -->
    <link href="<?=theme_css('font-awesome.css')?>" rel="stylesheet" />
    <!-- CUSTOM STYLE  -->
    <link href="<?=theme_css('styles.css')?>" rel="stylesheet" />
    <!-- HTML5 Shiv and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<section class="sectionalpha framesection">
    <div class="container">
    	<div class="row">
            <div class="openingpage text-center">
                <h1 class="" style="margin:70px auto;">404 The page you are looking for could not be found!</h1>
                <div class="btn-group btn-group-sm" role="group">
                    <a href="<?=base_url()?>" class="btn btn-primary">BACK TO MAIN PAGE</a>
                </div>
            </div>
        </div>
    </div>
</section>

</body>
</html>