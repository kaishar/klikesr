<?php

$routes[] = ['GET|POST', '/admin/parameter/index/[:orderBy]?/[:orderDir]?/[:code]?/[i:page]?', 'GoCart\Controller\Parameter#index'];
$routes[] = ['GET|POST', '/admin/parameter/form/[i:id]?', 'GoCart\Controller\Parameter#form'];
$routes[] = ['GET|POST', '/admin/parameter/delete/[i:itemID]?', 'GoCart\Controller\Parameter#delete'];
$routes[] = ['GET|POST', '/admin/parameter/delete_selected/[i:itemID]?', 'GoCart\Controller\Parameter#delete_selected'];
