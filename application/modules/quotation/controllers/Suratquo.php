<?php namespace GoCart\Controller;

class Suratquo extends Admin {
    
    public function __construct()
    { 
        parent::__construct();
        
        \CI::load()->model(array('Search','M_surat', 'Variable'));

        \CI::load()->helper(array('formatting', 'date'));
    }
        	
    public function index($sort_by='id',$sort_order='desc', $code=0, $page=0, $rows=10)
    {
        \CI::load()->helper('form');
        \CI::load()->helper('date');
		
        $data['message'] = \CI::session()->flashdata('message');
        $data['page_title'] = 'Surat Quotation';
        $data['code'] = $code;
        $term = false;
		
        $post = \CI::input()->post(null, false);
        if($post)
        {
            //if the term is in post, save it to the db and give me a reference
            $term = json_encode($post);
            $code = \CI::Search()->recordTerm($term);
            $data['code'] = $code;
            //reset the term to an object for use
            $term   = (object)$post;
        }
        elseif ($code)
        {
            $term = \CI::Search()->getTerm($code);
            $term = json_decode($term);
        }

        $data['term'] = $term;
        $data['surat_quo'] = \CI::M_quotation()->get_surat($term, $sort_by, $sort_order, $rows, $page);
        $data['total'] = \CI::M_quotation()->get_surat_count($term); 

        \CI::load()->library('pagination');

        $config['base_url'] = site_url('surat-quo/index/'.$sort_by.'/'.$sort_order.'/'.$code.'/');
        $config['total_rows'] = $data['total'];
        $config['per_page'] = $rows;
        $config['uri_segment'] = 6;
        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $config['full_tag_open'] = '<div class="text-center"><ul class="pagination pagination-sm">';
        $config['full_tag_close'] = '</ul></div>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        
        $config['prev_link'] = '&laquo;';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '&raquo;';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        
        \CI::pagination()->initialize($config);
        
        $data['sort_by'] = $sort_by;
        $data['sort_order'] = $sort_order;
        
        $this->view('suratquo', $data);
    } 
    
    public function form($id = false)
    {
        \CI::load()->helper('form');
        \CI::load()->library(array('form_validation', 'Utility'));
        
        $data['page_title'] = 'Tambah surat keluar Quotation';
        
        $data['id'] = '';
        $data['no_quo'] = '';
        $data['no'] = \CI::M_quotation()->getNextCode('no', 'quotations');
        $data['type'] = '';
        $data['tujuan'] = '';
        $data['kap'] = '';
        $data['tanggal'] = date('Y-m-d');
        $data['nama_klien'] = '';
        $data['alamat'] = '';
        $data['pembuat'] = '';
        $data['keterangan'] = '';
        $data['change_no'] = '';
        
        $list = \CI::Variable()->get_variable('TUJUAN_QUO');
        $list_tujuan[NULL] = 'Pilih tanda tangan';
        foreach($list as $rows)
        {
        	$list_tujuan[$rows->VALUE] = $rows->DESCRIPTION;
        }
        $data['list_tujuan'] = $list_tujuan;
        
        $list = \CI::Variable()->get_variable('KAP');
        $list_kap[NULL] = 'Pilih Cabang';
        foreach($list as $rows)
        {
        	$list_kap[$rows->VALUE] = $rows->DESCRIPTION;
        }
        $data['list_kap'] = $list_kap;
                
        // privileges checking
        $can_read = \CI::Utility()->havePrivilege('quotation', 'r');
        $can_write = \CI::Utility()->havePrivilege('quotation', 'w');
        
        //echo $can_read.NL;
        //echo $can_write; exit;
        
        if (!$can_write) {
        	\CI::session()->set_flashdata('error', 'You don\'t have enough privileges to view this section');
        	redirect('surat-quo/index');
        }
                            
        if ($id)
        { 
            $surat = \CI::M_quotation()->get_surat_byid($id);
            
            if (!$surat)
            {
                \CI::session()->set_flashdata('error', lang('error_not_found'));
                redirect('surat-quo/index');
            }
           			
            //set values to db values
            $data['id'] = $surat->id;
            $data['no_quo'] = $surat->no_quo;
	        $data['no'] = $surat->no;
	        $data['type'] = $surat->type;
	        $data['tujuan'] = $surat->tujuan;
	        $data['kap'] = $surat->kap;
	        $data['tanggal'] = $surat->tanggal;
	        $data['nama_klien'] = $surat->nama_klien;
	        $data['alamat'] = $surat->alamat;
	        $data['pembuat'] = $surat->pembuat;
	        $data['keterangan'] = $surat->keterangan;
	        $data['change_no'] = $surat->change_no;
        }
        
        \CI::form_validation()->set_rules('kap', 'Nama cabang', 'trim|required');
        \CI::form_validation()->set_rules('tanggal', 'Tanggal', 'trim|required');
        \CI::form_validation()->set_rules('nama_klien', 'Nama klien', 'trim|required');
        \CI::form_validation()->set_rules('alamat', 'Alamat', 'trim|required');
        \CI::form_validation()->set_rules('keterangan', 'Keterangan', 'trim|required');
                        
        if (\CI::form_validation()->run() == FALSE)
        {
            $this->view('suratquo_form', $data);
        }else
        {
            $save['id'] = $id;            
	        $save['no'] = \CI::input()->post('no');
	        $save['type'] = 'QUO';
	        $save['tujuan'] = \CI::input()->post('tujuan');
	        $save['kap'] = \CI::input()->post('kap');
	        $save['tanggal'] = \CI::input()->post('tanggal');
	        $save['nama_klien'] = \CI::input()->post('nama_klien');
	        $save['alamat'] = \CI::input()->post('alamat');
	        //$save['pembuat'] = \CI::input()->post('descr');
	        $save['keterangan'] = \CI::input()->post('keterangan');
	        
	        $bulantahun = \CI::M_quotation()->get_year_month($save['tanggal']);
	        $bulansurat = getBilRomawi($bulantahun['month']);
	        $tahunsurat = $bulantahun['year'];
	        
	        //revisi number
	        if(!$id)
	        {
	        	$save['change_no'] = 1;
	        }else {
	        	$save['change_no'] = \CI::input()->post('change_no') + 1;
	        }
	        
	        $save['no_quo'] = $this->get_nosurat($save['no'], $save['type'], $save['tujuan'], $save['change_no'], $bulansurat, $tahunsurat);
                        
            \CI::M_quotation()->save($save);
            
            \CI::session()->set_flashdata('message', 'Surat quotation sudah disimpan.');
                  
            redirect('surat-quo/index');  
        }
    }
    
    public function delete()
    {
        $id = \CI::input()->post('itemID'); 
        
        if ($id)
        { 
            $surat = \CI::M_quotation()->get_surat_byid($id);
            if (!$surat)
            {
                \CI::session()->set_flashdata('error', lang('error_not_found'));
                redirect('surat-quo/index');
            }
            else
            {
                \CI::M_quotation()->delete($id);
                \CI::session()->set_flashdata('message', 'Data surat sudah dihapus');
                redirect('surat-quo/index');
            }
        }
        else
        {
            \CI::session()->set_flashdata('error', lang('error_not_found'));
            redirect('surat-quo/index');
        }
    }
        
    public function get_nosurat($no, $type, $tujuan, $change_no, $bln, $thn)
    {
    	$str = '';
    	$str .= sprintf('%03d', $no).'/'.$type.'/'.$tujuan.'/HEST-'.sprintf('%02d', $change_no).'/'.$bln.'/'.$thn;
    
    	return $str;
    }
    
}