<?php
$routes[] = ['GET|POST', '/surat-quo/index/[:orderBy]?/[:orderDir]?/[:code]?/[i:page]?', 'GoCart\Controller\Suratquo#index'];
$routes[] = ['GET|POST', '/surat-quo/form/[i:id]?', 'GoCart\Controller\Suratquo#form'];
$routes[] = ['GET|POST', '/surat-quo/delete/[i:itemID]?', 'GoCart\Controller\Suratquo#delete'];
$routes[] = ['GET|POST', '/report-quo/laporan-entrisurat', 'GoCart\Controller\Reportsquo#laporanentrisurat'];