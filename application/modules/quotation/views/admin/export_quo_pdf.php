<style type="text/css">
<!--
#surats {
    font-family: "Raleway", Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

#surats td, #surats th {
	font-size: 10px;
    border: 1px solid #333;
    padding: 8px;
}

#surats tr:nth-child(even){background-color: #f2f2f2;}

#surats tr:hover {background-color: #ddd;}

#surats th {
    padding-top: 6px;
    padding-bottom: 6px;
    text-align: center;
    background-color: #ccc;
    color: #000;
}
-->
</style>
<page style="font-size: 10pt">
<h4 style="text-align: center;">EXPORT SURAT QUOTATION</h4>
<table id="surats">
	<thead>
		<tr>
			<th>NO. URUT</th>
			<th>NO. QUOTATIONS</th>
			<th>TANGGAL</th>
			<th>NAMA KLIEN</th>
			<th>ALAMAT</th>
			<th>KETERANGAN</th>
			<th>PEMBUAT</th>
		</tr>
	</thead>
	<tbody>
		<?php
			$i=1;
			foreach($quo as $rows): 
		?>
		<tr>
			<td style="text-align: center;"><?=$i?></td>
			<td><?=$rows->no_quo?></td>			
			<td><?=$rows->tanggal?></td>
			<td><?=$rows->nama_klien?></td>
			<td><?=$rows->alamat?></td>
			<td><?=$rows->keterangan?></td>
			<td><?=$rows->createdby?></td>
		</tr>
		<?php
			$i++;
			endforeach; 
		?>                                
	</tbody>
</table>

</page>