<?php namespace GoCart\Controller;

class Recoveryopini extends Admin {
    
    public function __construct()
    { 
        parent::__construct();
        
        \CI::load()->model(array('Search','M_surat', 'Variable'));
        \CI::load()->helper(array('formatting', 'date'));
        \CI::load()->library(array('Utility'));
    }
        	
    public function index($sort_by='id',$sort_order='desc', $code=0, $page=0, $rows=10)
    {
        \CI::load()->helper(array('form', 'date'));
                
        // privileges checking
        $can_read = \CI::Utility()->havePrivilege('restore', 'r');
        $can_write = \CI::Utility()->havePrivilege('restore', 'w');
        
        //echo $can_read.NL;
        //echo $can_write; exit;
        
        if (!$can_read && !$can_write) {
        	\CI::session()->set_flashdata('error', 'You don\'t have enough privileges to view this section');
        	redirect('recover-opini/index');
        }
		
        $data['message'] = \CI::session()->flashdata('message');
        $data['page_title'] = 'Restore Surat Opini Audit';
        $data['code'] = $code;
        $term = false;
		
        $post = \CI::input()->post(null, false);
        if($post)
        {
            //if the term is in post, save it to the db and give me a reference
            $term = json_encode($post);
            $code = \CI::Search()->recordTerm($term);
            $data['code'] = $code;
            //reset the term to an object for use
            $term   = (object)$post;
        }
        elseif ($code)
        {
            $term = \CI::Search()->getTerm($code);
            $term = json_decode($term);
        }

        $data['term'] = $term;
        $data['surat_opini'] = \CI::M_opini()->get_surat_intrash($term, $sort_by, $sort_order, $rows, $page);
        $data['total'] = \CI::M_opini()->get_surat_intrash_count($term); 

        \CI::load()->library('pagination');

        $config['base_url'] = site_url('recover-opini/index/'.$sort_by.'/'.$sort_order.'/'.$code.'/');
        $config['total_rows'] = $data['total'];
        $config['per_page'] = $rows;
        $config['uri_segment'] = 6;
        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $config['full_tag_open'] = '<div class="text-center"><ul class="pagination pagination-sm">';
        $config['full_tag_close'] = '</ul></div>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        
        $config['prev_link'] = '&laquo;';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '&raquo;';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        
        \CI::pagination()->initialize($config);
        
        $data['sort_by'] = $sort_by;
        $data['sort_order'] = $sort_order;
        
        $this->view('recover_opini', $data);
    } 
    
    public function form($id = false)
    {
    	\CI::load()->library(array('form_validation', 'Utility'));    
    	    
    	// privileges checking
    	$can_read = \CI::Utility()->havePrivilege('restore', 'r');
    	$can_write = \CI::Utility()->havePrivilege('restore', 'w');
    
    	//echo $can_read.NL;
    	//echo $can_write; exit;
    
    	if (!$can_write) {
    		\CI::session()->set_flashdata('error', 'You don\'t have enough privileges to view this section');
    		redirect('opini/index');
    	}
    	
    	$save['id'] = $id;
    	$save['is_trash'] = 0;   	 
    	
    	\CI::M_opini()->save($save);
    	
    	\CI::session()->set_flashdata('message', 'Surat Opini Audit sudah direstore.');
    	
    	redirect('recover-opini/index');    	
    }
            
}