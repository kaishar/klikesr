<?php
$routes[] = ['GET|POST', '/recover-hhes/index/[:orderBy]?/[:orderDir]?/[:code]?/[i:page]?', 'GoCart\Controller\Recoveryhhes#index'];
$routes[] = ['GET|POST', '/recover-hhes/form/[i:id]?', 'GoCart\Controller\Recoveryhhes#form'];
$routes[] = ['GET|POST', '/recover-opini/index/[:orderBy]?/[:orderDir]?/[:code]?/[i:page]?', 'GoCart\Controller\Recoveryopini#index'];
$routes[] = ['GET|POST', '/recover-opini/form/[i:id]?', 'GoCart\Controller\Recoveryopini#form'];
$routes[] = ['GET|POST', '/recover-hrd/index/[:orderBy]?/[:orderDir]?/[:code]?/[i:page]?', 'GoCart\Controller\Recoveryhrd#index'];
$routes[] = ['GET|POST', '/recover-hrd/form/[i:id]?', 'GoCart\Controller\Recoveryhrd#form'];
$routes[] = ['GET|POST', '/recover-quotation/index/[:orderBy]?/[:orderDir]?/[:code]?/[i:page]?', 'GoCart\Controller\Recoveryquotation#index'];
$routes[] = ['GET|POST', '/recover-quotation/form/[i:id]?', 'GoCart\Controller\Recoveryquotation#form'];