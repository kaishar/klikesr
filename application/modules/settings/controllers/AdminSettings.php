<?php namespace GoCart\Controller;
/**
 * AdminSettings Class
 *
 * @package     GoCart
 * @subpackage  Controllers
 * @category    AdminSettings
 * @author      Clear Sky Designs
 * @link        http://gocartdv.com
 */

class AdminSettings extends Admin {

    public function __construct()
    {
        parent::__construct();

        \CI::auth()->check_access(1, true);
        \CI::load()->model(['Messages', 'Pages']);
        \CI::lang()->load('settings');
        \CI::load()->helper('inflector');
    }

    public function index()
    {
        \CI::load()->helper('form');
        \CI::load()->library('form_validation');

        //set defaults
        $data = [
            'company_name' => '',
            'theme' => 'default',
            'homepage' => '',
            'default_meta_keywords' => '',
            'default_meta_description' => '',

            'email_from' => '',
            'email_to' => '',
            'email_method' => 'Mail',
            'smtp_server' => '',
            'smtp_username' => '',
            'smtp_password' => '',
            'smtp_port' => '25',

            'city' => '',
            'address1' => '',
            'address2' => '',
            'zip' => '',

            'ssl_support' => '',
            'stage_username' => '',
            'stage_password' => '',
            'require_login' => '',
            'new_customer_status' => '1'            
        ];

        \CI::form_validation()->set_rules('company_name', 'lang:company_name', 'required');
        \CI::form_validation()->set_rules('default_meta_keywords', 'lang:default_meta_keywords', 'trim|strip_tags');
        \CI::form_validation()->set_rules('default_meta_description', 'lang:default_meta_description', 'trim|strip_tags');

        \CI::form_validation()->set_rules('email_from', 'lang:email_from', 'required|valid_email');
        \CI::form_validation()->set_rules('email_to', 'lang:email_to', 'required|valid_email');
        \CI::form_validation()->set_rules('email_method', 'lang:email_method', 'required');

        if(\CI::input()->post('email_method') == 'smtp')
        {
            \CI::form_validation()->set_rules('smtp_server', 'lang:smtp_server', 'required');
            \CI::form_validation()->set_rules('smtp_username', 'lang:smtp_username', 'required');
            \CI::form_validation()->set_rules('smtp_password', 'lang:smtp_password', 'required');
            \CI::form_validation()->set_rules('smtp_port', 'lang:smtp_port', 'required');
        }
    
        \CI::form_validation()->set_rules('address1', 'lang:address');
        \CI::form_validation()->set_rules('address2', 'lang:address');
        \CI::form_validation()->set_rules('zip', 'lang:zip');

       
        // get the values from the DB
        $data = array_merge( $data, \CI::Settings()->get_settings('cms'));

        $data['config'] = $data;
        //break out order statuses to an array

        //get installed themes
        $data['themes'] = [];
        $themePath = FCPATH.'themes/';
        if ($handle = opendir($themePath)) {
            while (false !== ($entry = readdir($handle))) {
                if ($entry != "." && $entry != ".." && is_dir($themePath.$entry)) {
                    $data['themes'][$entry] = $entry;
                }
            }
            closedir($handle);
        }
        asort($data['themes']);

        //get ISO 4217 codes
        $data['iso_4217'] = [];
        $iso_4217 =json_decode(json_encode(simplexml_load_file(FCPATH.'ISO_4217.xml')));
        $iso_4217 = $iso_4217->CcyTbl->CcyNtry;
        foreach($iso_4217 as $iso_code)
        {
            if(isset($iso_code->Ccy))
            {
                $data['iso_4217'][$iso_code->Ccy] = $iso_code->Ccy;
            }
        }
        asort($data['iso_4217']);

        $data['page_title'] = 'Sistem Konfigurasi';

        $pages = \CI::Pages()->get_pages_tiered();
        $data['pages'] = [];
        foreach($pages['all'] as $page)
        {
            if(empty($page->url))
            {
                $data['pages'][$page->id] = $page->title;
            }
        }


        if (\CI::form_validation()->run() == FALSE)
        {
            $data['error'] = validation_errors();
            $this->view('settings', $data);
        }
        else
        {
            \CI::session()->set_flashdata('message', 'Konfigurasi sistem sudah diupdate. ');

            $save = \CI::input()->post();
            //fix boolean values
            $save['ssl_support'] = (bool)\CI::input()->post('ssl_support');
            $save['require_login'] = (bool)\CI::input()->post('require_login');
            $save['new_customer_status'] = \CI::input()->post('new_customer_status');
            $save['homepage'] = \CI::input()->post('homepage');

            \CI::Settings()->save_settings('cms', $save);

            redirect('admin/settings');
        }

    }

    public function canned_messages()
    {
        $data['canned_messages'] = \CI::Messages()->get_list();
        $data['page_title'] = lang('common_canned_messages');
        $this->view('canned_messages', $data);
    }


    public function canned_message_form($id=false)
    {
        $data['page_title'] = lang('canned_message_form');

        $data['id'] = $id;
        $data['name'] = '';
        $data['subject'] = '';
        $data['content'] = '';
        $data['deletable'] = 1;

        if($id)
        {
            $message = \CI::Messages()->get_message($id);
            $data = array_merge($data, $message);
        }

        \CI::load()->helper('form');
        \CI::load()->library('form_validation');

        \CI::form_validation()->set_rules('name', 'lang:message_name', 'trim|required|max_length[50]');
        \CI::form_validation()->set_rules('subject', 'lang:subject', 'trim|required|max_length[100]');
        \CI::form_validation()->set_rules('content', 'lang:message_content', 'trim|required');

        if (\CI::form_validation()->run() == FALSE)
        {
            $data['errors'] = validation_errors();

            $this->view('canned_message_form', $data);
        }
        else
        {

            $save['id'] = $id;
            $save['name'] = \CI::input()->post('name');
            $save['subject'] = \CI::input()->post('subject');
            $save['content'] = \CI::input()->post('content');

            //all created messages are typed to order so admins can send them from the view order page.
            if($data['deletable'])
            {
                $save['type'] = 'order';
            }
            \CI::Messages()->save_message($save);

            \CI::session()->set_flashdata('message', lang('message_saved_message'));
            redirect('admin/settings/canned_messages');
        }
    }

    public function delete_message($id)
    {
        \CI::Messages()->delete_message($id);

        \CI::session()->set_flashdata('message', lang('message_deleted_message'));
        redirect('admin/settings/canned_messages');
    }
}
