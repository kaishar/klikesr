<?php

// only administrator have privileges for below menus
if (CI::auth()->check_access(1, false, false)) {
    $menu[] = array(('Settings'), site_url('admin/settings'), ('Configure Application Settings'));
}
