<div class="content-wrapper" id="main">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php
                    //lets have the flashdata overright "$message" if it exists
                    if(CI::session()->flashdata('message'))
                    {
                        $message = CI::session()->flashdata('message');
                    }

                    if(CI::session()->flashdata('error'))
                    {
                        $error = CI::session()->flashdata('error');
                    }

                    if(function_exists('validation_errors') && validation_errors() != '')
                    {
                        $error = validation_errors();
                    }
                ?>

                <div id="js_error_container" class="alert alert-error" style="display:none;">
                    <p id="js_error"></p>
                </div>

                <div id="js_note_container" class="alert alert-note" style="display:none;"></div>

                <?php if (!empty($message)): ?>
                    <div class="alert alert-success" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <?php echo $message; ?>
                    </div>
                <?php endif; ?>

                <?php if (!empty($error)): ?>
                    <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <?php echo $error; ?>
                    </div>
                <?php endif; ?>                 
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h1 class="page-head-line">SISTEM KONFIGURASI</h1>
            </div>
        </div>        
        <div class="row">            
            <div class="col-md-12 col-sm-12">
				<?php echo form_open_multipart('admin/settings');?>
				    <fieldset>
				        <legend><?php echo 'Detail Lembaga';?></legend>
				        <div class="row">
				            <div class="col-md-6">
				                <div class="form-group">
				                    <label><?php echo 'Nama Lembaga';?></label>
				                    <?php echo form_input(array('class'=>'form-control', 'name'=>'company_name', 'value'=>assign_value('company_name', $company_name)));?>
				                </div>
				            </div>
				            
				            <div class="col-md-6">
				                <div class="form-group">
				                    <label><?php echo 'Kode Lembaga';?></label>
				                    <?php echo form_input(array('class'=>'form-control', 'name'=>'school_code', 'value'=>assign_value('school_code', $school_code)));?>
				                </div>
				            </div>		
				        </div>
				        <div class="row">
				            <div class="col-md-6">
				                <div class="form-group">
				                    <label><?php echo lang('default_meta_keywords');?></label>
				                    <?php echo form_input(array('class'=>'form-control', 'name'=>'default_meta_keywords', 'value'=>assign_value('default_meta_keywords', $default_meta_keywords), 'class' => 'form-control'));?>
				                </div>
				            </div>
				
				            <div class="col-md-6">
				                <div class="form-group">
				                    <label><?php echo lang('default_meta_description');?></label>
				                    <?php echo form_input(array('class'=>'form-control', 'name'=>'default_meta_description', 'value'=>assign_value('default_meta_description', $default_meta_description), 'class' => 'form-control'));?>
				                </div>
				            </div>
				        </div>
				    </fieldset>
				
				    <fieldset>
				        <legend><?php echo 'Setting Email';?></legend>
				        <div class="row form-group">
				            <div class="col-md-4">
				                <div class="form-group">
				                    <label><?php echo 'Email Pengelola'; ?></label>
				                    <?php echo form_input(array('class'=>'form-control', 'name'=>'email_to', 'value'=>assign_value('email_to', $email_to), 'class' => 'form-control'));?>
				                </div>
				            </div>
				
				            <div class="col-md-4">
				                <div class="form-group">
				                    <label><?php echo 'Email Pengirim';?></label>
				                    <?php echo form_input(array('class'=>'form-control', 'name'=>'email_from', 'value'=>assign_value('email_from', $email_from), 'class'=>'form-control'));?>
				                </div>
				            </div>
				
				            <div class="col-md-4">
				                <div class="form-group">
				                    <label><?php echo lang('email_method');?></label>
				                    <?php echo form_dropdown('email_method', ['mail'=>'Mail', 'smtp'=>'SMTP'], assign_value('email_method', $email_method), 'class="form-control" id="emailMethod"');?>
				                </div>
				            </div>
				        </div>
				
				        <div class="row emailMethods form-group" id="email_smtp">
				            <div class="col-md-3">
				                <div class="form-group">
				                <label><?php echo lang('smtp_server');?></label>
				                <?php echo form_input(array('class'=>'form-control', 'name'=>'smtp_server', 'value'=>assign_value('smtp_server', $smtp_server)));?>
				                </div>
				            </div>
				
				            <div class="col-md-3">
				                <div class="form-group">
				                    <label><?php echo lang('smtp_port');?></label>
				                    <?php echo form_input(array('class'=>'form-control', 'name'=>'smtp_port', 'value'=>assign_value('smtp_port', $smtp_port)));?>
				                </div>
				            </div>
				
				            <div class="col-md-3">
				                <div class="form-group">
				                    <label><?php echo lang('smtp_username');?></label>
				                    <?php echo form_input(array('class'=>'form-control', 'name'=>'smtp_username', 'value'=>assign_value('smtp_username', $smtp_username)));?>
				                </div>
				            </div>
				
				            <div class="col-md-3">
				                <div class="form-group">
				                    <label><?php echo lang('smtp_password');?></label>
				                    <?php echo form_input(array('class'=>'form-control', 'name'=>'smtp_password', 'value'=>assign_value('smtp_password', $smtp_password)));?>
				                </div>
				            </div>
				        </div>
				
				    </fieldset>
				
				    <fieldset>
				        <legend><?php echo 'Alamat Lembaga';?></legend>
				        				
				        <div class="form-group">
				            <label><?php echo 'Alamat 1';?></label>
				            <?php echo form_input(array('name'=>'address1', 'class'=>'form-control','value'=>assign_value('address1',$address1)));?>
				        </div>
				
				        <div class="form-group">
				            <?php echo form_input(array('name'=>'address2', 'class'=>'form-control','value'=> assign_value('address2',$address2)));?>
				        </div>
				
				        <div class="row">
				            <div class="col-md-4">
				                <div class="form-group">
				                    <label><?php echo 'Kota';?></label>
				                    <?php echo form_input(array('name'=>'city','class'=>'form-control', 'value'=>assign_value('city',$city)));?>
				                </div>
				            </div>
				            
				            <div class="col-md-2">
				                <div class="form-group">
				                    <label><?php echo 'Kode Pos';?></label>
				                    <?php echo form_input(array('maxlength'=>'10', 'class'=>'form-control', 'name'=>'zip', 'value'=> assign_value('zip',$zip)));?>
				                </div>
				            </div>
				        </div>
				    </fieldset>			
				
				    <input type="submit" class="btn btn-primary" value="<?php echo lang('save');?>" />
				
				</form>
			</div>
		</div>
	</div>
</div>
            
<!-- CONTENT-WRAPPER SECTION END-->

<style type="text/css">
.emailMethods {
   display:none;
}
#email_<?php echo $email_method;?> {
    display:block;
}
</style>
