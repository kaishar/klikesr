<?php namespace GoCart\Controller;

class AdminModules extends Admin {
	//this is used when editing or adding a customer
    var $module_id = false; 
   
    public function __construct()
    { 
        parent::__construct();
        
        \CI::load()->model(array('Mst_module','Group_access'));
        \CI::load()->model('Search');

        \CI::load()->helper(array('formatting'));
        /*
        if (!\CI::auth()->check_access(1, false, false)) {
            \CI::session()->set_flashdata('error', 'You don\'t have enough privileges to view this section');
            redirect(site_url('admin'));
        }
         * 
         */
    }
        	
    public function index($sort_by='module_id',$sort_order='desc', $code=0, $page=0, $rows=10)
    {
        \CI::load()->helper('form');
        \CI::load()->helper('date');
		
        $data['message'] = \CI::session()->flashdata('message');
        $data['page_title'] = 'Modules';
        $data['code'] = $code;
        $term = false;
		
        $post = \CI::input()->post(null, false);
        if($post)
        {
            //if the term is in post, save it to the db and give me a reference
            $term = json_encode($post);
            $code = \CI::Search()->recordTerm($term);
            $data['code'] = $code;
            //reset the term to an object for use
            $term   = (object)$post;
        }
        elseif ($code)
        {
            $term = \CI::Search()->getTerm($code);
            $term = json_decode($term);
        }

        $data['term'] = $term;
        $data['modul'] = \CI::Mst_module()->getModule($term, $sort_by, $sort_order, $rows, $page);
        $data['total'] = \CI::Mst_module()->getModuleCount($term); 

        \CI::load()->library('pagination');

        $config['base_url'] = site_url('admin/modules/index/'.$sort_by.'/'.$sort_order.'/'.$code.'/');
        $config['total_rows'] = $data['total'];
        $config['per_page'] = $rows;
        $config['uri_segment'] = 7;
        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $config['full_tag_open'] = '<nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        
        $config['prev_link'] = '&laquo;';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '&raquo;';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        
        \CI::pagination()->initialize($config);
        
        $data['sort_by'] = $sort_by;
        $data['sort_order'] = $sort_order;
        
        $this->view('modules', $data);
    } 
    
    public function form($module_id = false)
    {
        \CI::load()->helper('form');
        \CI::load()->library('form_validation');
        
        $data['page_title'] = 'Add Modules';
        
        //default values are empty if the module is new
        $data['module_id'] = '';
        $data['module_name'] = '';
        $data['module_path'] = '';
        $data['module_desc'] = '';
                            
        if ($module_id)
        { 
            $this->module_id = $module_id;
            $module = \CI::Mst_module()->get_module($module_id);
            //if the module does not exist, redirect them to the module list with an error
            if (!$module)
            {
                \CI::session()->set_flashdata('error', lang('error_not_found'));
                redirect('admin/modules/index');
            }
        			
            //set values to db values
            $data['module_id'] = $module->module_id;
            $data['module_name'] = $module->module_name;
            $data['module_path'] = $module->module_path;
            $data['module_desc'] = $module->module_desc;
        }
        
        \CI::form_validation()->set_rules('module_name', 'Module Name', 'trim|required');
                        
        if (\CI::form_validation()->run() == FALSE)
        {
            $this->view('modules_form', $data);
        }
        else
        {
            $save['module_id'] = $module_id;
            $save['module_name'] = \CI::input()->post('module_name');
            $save['module_path'] = \CI::input()->post('module_path');
            $save['module_desc'] = \CI::input()->post('module_desc');
                                    
            \CI::Mst_module()->save($save);
            
            \CI::session()->set_flashdata('message', 'The modules has been saved.');
            
            //go back to the modules list
            redirect('admin/modules/index');  
        }
    }
    
    public function delete()
    {
        $module_id = \CI::input()->post('itemID'); 
        if ($module_id)
        { 
            $modules = \CI::Mst_module()->get_module($module_id);
            if (!$modules)
            {
                \CI::session()->set_flashdata('error', lang('error_not_found'));
                redirect('admin/modules/index');
            }
            else
            {
                \CI::Mst_module()->delete($module_id);
                
                \CI::session()->set_flashdata('message', lang('message_customer_deleted'));
                redirect('admin/modules/index');
            }
        }
        else
        {
            \CI::session()->set_flashdata('error', lang('error_not_found'));
            redirect('admin/modules/index');
        }
    }
    
    public function delete_selected()
    {
        $item = array();
        $item = \CI::input()->post('itemID');
        if (!is_array($item)) 
        {
            // make an array
            $item = array((integer)$item);
        }
        
        // loop array
        foreach ($item as $module_id) {
            $module_id = (integer)$module_id;
            \CI::Mst_module()->delete($module_id);
            // also delete all records related to this data
            // delete group privileges
            \CI::Group_access()->delete_by_module($module_id);
        }
        
        \CI::session()->set_flashdata('message', lang('message_customer_deleted'));
        redirect('admin/modules/index');
    }
}