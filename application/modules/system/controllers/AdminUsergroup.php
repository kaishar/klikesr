<?php namespace GoCart\Controller;

class AdminUsergroup extends Admin {
    //this is used when editing or adding a customer
    var $group_id = false; 
   
    public function __construct()
    { 
        parent::__construct();
        
        \CI::load()->model(array('User_group','Mst_module','Group_access'));
        \CI::load()->model('Search');

        \CI::load()->helper(array('formatting'));
        /*
        if (!\CI::auth()->check_access(1, false, false)) {
            \CI::session()->set_flashdata('error', 'You don\'t have enough privileges to view this section');
            redirect(site_url('admin'));
        }
        */
    }
        	
    public function index($sort_by='group_id',$sort_order='desc', $code=0, $page=0, $rows=10)
    {
        \CI::load()->helper('form');
        \CI::load()->helper('date');
		
        $data['message'] = \CI::session()->flashdata('message');
        $data['page_title'] = 'Group Access';
        $data['code'] = $code;
        $term = false;
		
        $post = \CI::input()->post(null, false);
        if($post)
        {
            //if the term is in post, save it to the db and give me a reference
            $term = json_encode($post);
            $code = \CI::Search()->recordTerm($term);
            $data['code'] = $code;
            //reset the term to an object for use
            $term   = (object)$post;
        }
        elseif ($code)
        {
            $term = \CI::Search()->getTerm($code);
            $term = json_decode($term);
        }

        $data['term'] = $term;
        $data['usergroup'] = \CI::User_group()->getUsergroup($term, $sort_by, $sort_order, $rows, $page);
        $data['total'] = \CI::User_group()->getUsergroupCount($term); 

        \CI::load()->library('pagination');

        $config['base_url'] = site_url('admin/usergroup/index/'.$sort_by.'/'.$sort_order.'/'.$code.'/');
        $config['total_rows'] = $data['total'];
        $config['per_page'] = $rows;
        $config['uri_segment'] = 7;
        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $config['full_tag_open'] = '<nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        
        $config['prev_link'] = '&laquo;';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '&raquo;';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        
        \CI::pagination()->initialize($config);
        
        $data['sort_by'] = $sort_by;
        $data['sort_order'] = $sort_order;
        
        $this->view('usergroup', $data);
    } 
    
    public function form($group_id = false)
    {
        \CI::load()->helper('form');
        \CI::load()->library('form_validation');
        
        $data['page_title'] = 'Add Group Access';
        
        //default values are empty if the module is new
        $data['group_id'] = '';
        $data['group_name'] = '';
        $data['input_date'] = '';
        $data['last_update'] = '';
        $data['module'] = array();
        
        // get group list
        $data['modules_list'] = \CI::Mst_module()->getModule();
                            
        if ($group_id)
        { 
            $this->group_id = $group_id;
            $group = \CI::User_group()->get_usergroup($group_id);
            
            if (!$group)
            {
                \CI::session()->set_flashdata('error', lang('error_not_found'));
                redirect('admin/usergroup/index');
            }
            
            $privileges = \CI::Group_access()->get_module_by_group($group_id);
            
            // privileges
            // get group access data
            $priv_data = array();
            foreach($privileges as $rows):
            	$priv_data[$rows->module_id]['r'] = $rows->r;
            	$priv_data[$rows->module_id]['w'] = $rows->w;            	
            endforeach;
            /*
            while ($access_data = $privileges) 
            {
            	print_r($access_data[0]); exit;
            	$priv_data[$access_data['module_id']]['r'] = $access_data['r'];
            	$priv_data[$access_data['module_id']]['w'] = $access_data['w'];
            }
            */
            //print_r($priv_data); exit;
            $data['priv_data'] = $priv_data;
            
            //set values to db values
            $data['group_id'] = $group->group_id;
            $data['group_name'] = $group->group_name;
            $data['last_update'] = $group->last_update;
            $data['input_date'] = $group->input_date;
        }
        
        \CI::form_validation()->set_rules('group_name', 'Group Name', 'trim|required');
                        
        if (\CI::form_validation()->run() == FALSE)
        {
            $this->view('usergroup_form', $data);
        }
        else
        {
            $save['group_id'] = $group_id;
            $save['group_name'] = \CI::input()->post('group_name');
                        
            $group_id = \CI::User_group()->save($save);
                                    
            \CI::Group_access()->delete_by_group($group_id);
            // set group privileges
            if (\CI::input()->post('access')) 
            {
                foreach (\CI::input()->post('access') as $module) {
                    // check write privileges
                    $is_write = 0;
                    if (\CI::input()->post('full')) {
                    	foreach (\CI::input()->post('full') as $module_write) {
                    		if ($module_write == $module) {
                    			$is_write = 1;
                    		}
                    	}
                    }
                    $save1['group_id'] = $group_id;
                    $save1['module_id'] = $module;
                    $save1['r'] = 1;
                    $save1['w'] = $is_write;
                    \CI::Group_access()->save_by_group($save1);
                }
            }
            \CI::session()->set_flashdata('message', 'The User Group has been saved.');
                  
            redirect('admin/usergroup/index');  
        }
    }
    
    public function delete()
    {
        $group_id = \CI::input()->post('itemID'); 
        if ($group_id)
        { 
            $groups = \CI::User_group()->get_group($group_id);
            if (!$groups)
            {
                \CI::session()->set_flashdata('error', lang('error_not_found'));
                redirect('admin/usergroup/index');
            }
            else
            {
                \CI::User_group()->delete($group_id);
                // delete group privileges
                \CI::Group_access()->delete_by_group($group_id);
                \CI::session()->set_flashdata('message', lang('message_customer_deleted'));
                redirect('admin/usergroup/index');
            }
        }
        else
        {
            \CI::session()->set_flashdata('error', lang('error_not_found'));
            redirect('admin/usergroup/index');
        }
    }
    
    public function delete_selected()
    {
        $item = array();
        $item = \CI::input()->post('itemID');
        if (!is_array($item)) 
        {
            // make an array
            $item = array((integer)$item);
        }
        
        // loop array
        foreach ($item as $group_id) {
            $group_id = (integer)$group_id;
            \CI::User_group()->delete($group_id);
            // also delete all records related to this data
            // delete group privileges
            \CI::Group_access()->delete_by_group($group_id);
        }
        
        \CI::session()->set_flashdata('message', lang('message_customer_deleted'));
        redirect('admin/usergroup/index');
    }
}