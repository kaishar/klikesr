<?php

$routes[] = ['GET|POST', '/admin/modules/index/[:orderBy]?/[:orderDir]?/[:code]?/[i:page]?', 'GoCart\Controller\AdminModules#index'];
$routes[] = ['GET|POST', '/admin/modules/form/[i:id]?', 'GoCart\Controller\AdminModules#form'];
$routes[] = ['GET|POST', '/admin/modules/delete/[i:itemID]?', 'GoCart\Controller\AdminModules#delete'];
$routes[] = ['GET|POST', '/admin/modules/delete_selected/[i:itemID]?', 'GoCart\Controller\AdminModules#delete_selected'];
$routes[] = ['GET|POST', '/admin/appuser/index/[:orderBy]?/[:orderDir]?/[:code]?/[i:page]?', 'GoCart\Controller\AdminAppuser#index'];
$routes[] = ['GET|POST', '/admin/appuser/form/[i:id]?', 'GoCart\Controller\AdminAppuser#form'];
$routes[] = ['GET|POST', '/admin/appuser/delete/[i:itemID]?', 'GoCart\Controller\AdminAppuser#delete'];
$routes[] = ['GET|POST', '/admin/appuser/delete_selected/[i:itemID]?', 'GoCart\Controller\AdminAppuser#delete_selected'];
//$routes[] = ['GET|POST', '/admin/appuser/delete_image/[i:itemID]?', 'GoCart\Controller\AdminAppuser#delete_image'];
$routes[] = ['GET|POST', '/admin/usergroup/index/[:orderBy]?/[:orderDir]?/[:code]?/[i:page]?', 'GoCart\Controller\AdminUsergroup#index'];
$routes[] = ['GET|POST', '/admin/usergroup/form/[i:id]?', 'GoCart\Controller\AdminUsergroup#form'];
$routes[] = ['GET|POST', '/admin/usergroup/delete/[i:itemID]?', 'GoCart\Controller\AdminUsergroup#delete'];
$routes[] = ['GET|POST', '/admin/usergroup/delete_selected/[i:itemID]?', 'GoCart\Controller\AdminUsergroup#delete_selected'];
$routes[] = ['GET|POST', '/admin/appuser/change-profile/[userName]?', 'GoCart\Controller\AdminAppuser#change_profile'];