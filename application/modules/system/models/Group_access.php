<?php
Class Group_access extends CI_Model
{
    public function delete_by_module($id)
    {
        CI::db()->where('module_id', $id);
        CI::db()->delete('group_access');
    }
    	
    public function delete_by_group($id)
    {
        CI::db()->where('group_id', $id);
        CI::db()->delete('group_access');
    }
    
    public function save_by_group($data)
    {
        CI::db()->insert('group_access', $data);
        return CI::db()->insert_id();        
    }
    
    public function get_module_by_group($group_id)
    {
        $module = array();
        
        CI::db()->select('module_id, r, w');
        $result = CI::db()->get_where('group_access', array('group_id'=>$group_id));
        
        /*
        foreach($result->result() as $row):
            $module[] = $row->module_id; 
        endforeach;
        
        return $module;
        */
        return $result->result();
    }
    
}
