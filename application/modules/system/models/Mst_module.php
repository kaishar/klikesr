<?php
Class Mst_module extends CI_Model
{
    public function getModule($search=false, $sort_by='', $sort_order='DESC', $limit=0, $offset=0)
    {
        \CI::db()->select('module_id, module_name, module_path, module_desc');
        
        if ($search)
        {
            if(!empty($search->term))
            {
                $this->getModuleSearchLike($search->term);
            }
        }

        if($limit>0)
        {
            \CI::db()->limit($limit, $offset);
        }
        if(!empty($sort_by))
        {
            \CI::db()->order_by($sort_by, $sort_order);
        }

        return \CI::db()->get('mst_module')->result();
    }

    public function getModuleCount($search=false)
    {
		if ($search)
        {
            if(!empty($search->term))
            {
                $this->getModuleSearchLike($search->term);
            }
        }

        return CI::db()->count_all_results('mst_module');
    }
	
    private function getModuleSearchLike($str)
    {
        //support multiple words
        $term = explode(' ', $str);

        foreach($term as $t)
        {
            $not = '';
            $operator = 'OR';
            if(substr($t,0,1) == '-')
            {
                $not = 'NOT ';
                $operator = 'AND';
                //trim the - sign off
                $t = substr($t,1,strlen($t));
            }

            $like = '';
            $like .= "( `mst_module`.`module_name` ".$not."LIKE '%".CI::db()->escape_like_str($t)."%' " ;
            $like .= $operator." `mst_module`.`module_path` ".$not."LIKE '%".CI::db()->escape_like_str($t)."%'  ";
            $like .= $operator." `mst_module`.`module_desc` ".$not."LIKE '%".CI::db()->escape_like_str($t)."%' )";

            CI::db()->where($like);
        }
    }
	
    public function save($modules)
    {
        if($modules['module_id'])
        {
            CI::db()->where('module_id', $modules['module_id']);
            CI::db()->update('mst_module', $modules);
            return $modules['module_id'];
        }
        else
        {
            CI::db()->insert('mst_module', $modules);
            return CI::db()->insert_id();
        }
    }

    public function delete($id)
    {
        CI::db()->where('module_id', $id);
        CI::db()->delete('mst_module');
    }
    	
    public function get_module($id)
    {
        $result = CI::db()->get_where('mst_module', array('module_id'=>$id));
        return $result->row();
    }
}
