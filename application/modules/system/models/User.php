<?php
Class User extends CI_Model
{
    public function getUser($search=false, $sort_by='', $sort_order='DESC', $limit=0, $offset=0)
    {
        \CI::db()->select('id, '
                . 'username, realname, password, email,'
                . 'nip, user_image, social_media,'
                . 'last_login, groups, last_update');
        
        //\CI::db()->join('variable', 'variable.VALUE = user.user_type', 'left');
        
        if ($search)
        {
            if(!empty($search->term))
            {
                $this->getUserSearchLike($search->term);
            }
        }

        //CI::db()->where('user_type', 1);
        
        if($limit>0)
        {
            \CI::db()->limit($limit, $offset);
        }
        if(!empty($sort_by))
        {
            \CI::db()->order_by($sort_by, $sort_order);
        }

        return \CI::db()->get('user')->result();
    }

    public function getUserCount($search=false)
    {
        \CI::db()->select('id, '
                . 'username, realname, password, email,'
                . 'nip, user_image, social_media,'
                . 'last_login, groups, last_update');
        
        if ($search)
        {
            if(!empty($search->term))
            {
                $this->getUserSearchLike($search->term);
            }
        }

        //CI::db()->where('user_type', 1);
        
        return CI::db()->count_all_results('user');
    }
	
    private function getUserSearchLike($str)
    {
        //support multiple words
        $term = explode(' ', $str);

        foreach($term as $t)
        {
            $not = '';
            $operator = 'OR';
            if(substr($t,0,1) == '-')
            {
                $not = 'NOT ';
                $operator = 'AND';
                //trim the - sign off
                $t = substr($t,1,strlen($t));
            }

            $like = '';
            $like .= "( `user`.`username` ".$not."LIKE '%".CI::db()->escape_like_str($t)."%' " ;
            $like .= $operator." `user`.`realname` ".$not."LIKE '%".CI::db()->escape_like_str($t)."%'  ";
            $like .= $operator." `user`.`nip` ".$not."LIKE '%".CI::db()->escape_like_str($t)."%' )";

            CI::db()->where($like);
        }
    }
	
    public function save($user)
    {
        if($user['id'])
        {
            $user['last_update'] = date('Y-m-d');
            CI::db()->where('id', $user['id']);
            CI::db()->update('user', $user);
            return $user['id'];
        }
        else
        {
            $user['input_date'] = date('Y-m-d');
            $user['user_type'] = 1;
            CI::db()->insert('user', $user);
            return CI::db()->insert_id();
        }
    }

    public function delete($id)
    {
        $image = $this->get_image($id);
        if($image->user_image)
        {
            $this->delete_image($image->user_image);
        }
        CI::db()->where('id', $id);
        CI::db()->delete('user');
    }
    	
    public function get_user($id)
    {
        $result = CI::db()->get_where('user', array('id'=>$id));
        return $result->row();
    }
    
    public function check_username($str)
    {        
        CI::db()->select('username');
        CI::db()->from('user');
        CI::db()->where('username', $str);
        
        $count = CI::db()->count_all_results();
        
        if ($count > 0)
        {
            return true;
        }
        else
        {
            return false;
        }     
    }
    
    public function get_image($id)
    {
        CI::db()->select('user_image');
        $result = CI::db()->get_where('user', array('id'=>$id));
                
        return $result->row();
    }
    
    public function delete_image($image)
    { 
        $file = [];
        $file[] = 'uploads/images/full/'.$image;
        $file[] = 'uploads/images/medium/'.$image;
        $file[] = 'uploads/images/small/'.$image;
        $file[] = 'uploads/images/thumbnails/'.$image;

        foreach($file as $f)
        {
            //delete the existing file if needed
            if(file_exists($f))
            {
                unlink($f);
            }
        }
    }
    
    public function get_id_by_username($username)
    {
        CI::db()->select('id');
        $result = CI::db()->get_where('user', array('username'=>$username));
                
        return $result->row_array();
    }
    
    public function get_info_byusername($username)
    {
        CI::db()->select('*');
        $result = CI::db()->get_where('user', array('username'=>$username));
                
        return $result->row_array();
    }
}
