<?php
Class User_group extends CI_Model
{  	
    public function get_group()
    {
        CI::db()->not_like('group_name', 'Web');
        return CI::db()->get('user_group')->result();
    }
    
    public function getUsergroup($search=false, $sort_by='', $sort_order='DESC', $limit=0, $offset=0)
    {
        \CI::db()->select('group_id, group_name, input_date, last_update');
        
        if ($search)
        {
            if(!empty($search->term))
            {
                $this->getUsergroupSearchLike($search->term);
            }
        }

        if($limit>0)
        {
            \CI::db()->limit($limit, $offset);
        }
        if(!empty($sort_by))
        {
            \CI::db()->order_by($sort_by, $sort_order);
        }

        return \CI::db()->get('user_group')->result();
    }

    public function getUsergroupCount($search=false)
    {
		if ($search)
        {
            if(!empty($search->term))
            {
                $this->getUsergroupSearchLike($search->term);
            }
        }

        return CI::db()->count_all_results('user_group');
    }
	
    private function getUsergroupSearchLike($str)
    {
        //support multiple words
        $term = explode(' ', $str);

        foreach($term as $t)
        {
            $not = '';
            $operator = 'OR';
            if(substr($t,0,1) == '-')
            {
                $not = 'NOT ';
                $operator = 'AND';
                //trim the - sign off
                $t = substr($t,1,strlen($t));
            }

            $like = '';
            $like .= "( `user_group`.`group_name` ".$not."LIKE '%".CI::db()->escape_like_str($t)."%' )";

            CI::db()->where($like);
        }
    }
	
    public function save($group)
    {
        if($group['group_id'])
        {
            $group['last_update'] = date('Y-m-d');
            CI::db()->where('group_id', $group['group_id']);
            CI::db()->update('user_group', $group);
            return $group['group_id'];
        }
        else
        {
            $group['input_date'] = date('Y-m-d');
            CI::db()->insert('user_group', $group);
            return CI::db()->insert_id();
        }
    }

    public function delete($id)
    {
        CI::db()->where('group_id', $id);
        CI::db()->delete('user_group');
    }
    	
    public function get_usergroup($id)
    {
        $result = CI::db()->get_where('user_group', array('group_id'=>$id));
        return $result->row();
    }
}
