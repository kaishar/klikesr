<?php

// only administrator have privileges for below menus
if (CI::auth()->check_access(1, false, false)) {
    //$menu[] = array(('Modules'), site_url('admin/modules/index'), ('Configure Application Modules'));
    $menu[] = array(('Users Management'), site_url('admin/appuser/index'), ('Manage Application User or Library Staff'));
    $menu[] = array(('User Group'), site_url('admin/usergroup/index'), ('Manage Group of Application User'));
	$menu[] = array(('General Parameter'), site_url('admin/parameter/index/'), ('General Parameter'));
	$menu[] = array(('Master Perusahaan'), site_url('company/index/'), ('Master Perusahaan'));
}
