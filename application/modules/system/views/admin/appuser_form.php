<div class="content-wrapper" id="main">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php
                    //lets have the flashdata overright "$message" if it exists
                    if(CI::session()->flashdata('message'))
                    {
                        $message = CI::session()->flashdata('message');
                    }

                    if(CI::session()->flashdata('error'))
                    {
                        $error = CI::session()->flashdata('error');
                    }

                    if(function_exists('validation_errors') && validation_errors() != '')
                    {
                        $error = validation_errors();
                    }
                ?>

                <div id="js_error_container" class="alert alert-error" style="display:none;">
                    <p id="js_error"></p>
                </div>

                <div id="js_note_container" class="alert alert-note" style="display:none;"></div>

                <?php if (!empty($message)): ?>
                    <div class="alert alert-success" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <?php echo $message; ?>
                    </div>
                <?php endif; ?>

                <?php if (!empty($error)): ?>
                    <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <?php echo $error; ?>
                    </div>
                <?php endif; ?>                 
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h1 class="page-head-line">User Management</h1>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <strong>Form add user</strong>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="text-left">
                                    <a href="<?=site_url('admin/appuser/index/')?>" class="btn btn-primary"><i class="fa fa-list-alt"></i>&nbsp;<?php echo ('List User'); ?></a>
                                    
                                </div>
                            </div>                                                   
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-12">
                            <?php 
                                $attributes = array('class' => 'form-horizontal');
                                echo form_open_multipart('admin/appuser/form/'.$id, $attributes); 								
                            ?>                            
                            <div class="form-group">
                                <label for="username" class="col-sm-2 control-label">Username *</label>
                                <div class="col-sm-10">
                                <?php 
                                    echo form_input(['name'=>'username', 
                                    		'id'=>'username',
                                            'value'=>assign_value('username', $username), 
                                            'class'=>'form-control',
                                            'maxlength'=>'256',
                                            'style'=>'width: 50%;']); 
                                ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="realname" class="col-sm-2 control-label">Real Name</label>
                                <div class="col-sm-10">
                                <?php
	                                echo form_input(['name'=>'realname',
	                                		'id'=>'realname',
	                                		'value'=>assign_value('realname', $realname),
	                                		'class'=>'form-control',
	                                		'maxlength'=>'256',
	                                		'style'=>'width: 50%;']);
                                ?>
                                </div>
                            </div>                            	
                            <div class="form-group">
                                <label for="nip" class="col-sm-2 control-label">NIK</label>
                                <div class="col-sm-10">
                                <?php
	                                echo form_input(['name'=>'nip', 
	                                			'id'=>'nip',
                                                'value'=>assign_value('nip', $nip), 
                                                'class'=>'form-control',
                                                'maxlength'=>'9',
                                                'style'=>'width: 50%;']);
                                ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="email" class="col-sm-2 control-label">Email</label>
                                <div class="col-sm-10">
                                <?php
	                                echo form_input(['name'=>'email',
	                                		'id'=>'email',
	                                		'value'=>assign_value('email', $email),
	                                		'class'=>'form-control',
	                                		'maxlength'=>'256',
	                                		'style'=>'width: 50%;']);
                                ?>
                                </div>
                            </div>	
                            <div class="form-group">
                                <label for="image" class="col-sm-2 control-label">User Picture</label>
                                <div class="col-sm-10">
                                <?php
                                	if ($user_image) {
                                ?>
                                	<div id="imageFilename">
                                		<a href="<?=base_url('uploads/img/user/full/'.$user_image)?>" class="openPopUp notAJAX">
                                        	<strong><?=$user_image?></strong>
                                        </a> 
                                    </div>
                               	<?php
                                	}
                                    echo NL;
                               	?>
                                	<input type="file" value="" id="image" name="image">
                                	Maximum 500 KB
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Group(s)</label>
                                <div class="col-sm-10">  
	                            <?php
		                            //print_r($groups);
		                            foreach($group_list as $group):
		                            echo '<div>';
		                            $data = array(
		                            		'name'          => 'groups_checked[]',
		                            		'value'         => $group->group_id,
		                            		'checked'       => in_array($group->group_id, $groups)?TRUE:FALSE,
		                            		'style'         => 'border: 0;'
		                            );
		                            
		                            echo form_checkbox($data).'&nbsp;'.$group->group_name;
		                            echo '</div>';
		                            endforeach;
	                            ?>
	                            </div>
                            </div> 
                            <div class="form-group">
                                <label class="col-sm-2 control-label">New Password *</label>
                                <div class="col-sm-10">
                                    <input type="password" style="width: 50%;" value="" name="password" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Confirm New Password *</label>
                                <div class="col-sm-10">
                                    <input type="password" style="width: 50%;" value="" name="passconf" class="form-control">
                                </div>
                            </div>                                                           														
                            <div class="form-group">
                                <div class="col-sm-10 col-sm-offset-2">
                                <?php
                                    if ($id) 
                                    {
                                ?>
                                    <td>
                                        <input class="button btn btn-primary" type="submit" value="Update" name="saveData">
                                        <input class="cancelButton button btn btn-primary" type="button" value="Batal" onclick="window.history.back()">
                                        <input class="button btn btn-danger btn-delete confirmSubmit" type="button" onclick="confSubmit('deleteForm', 'Apakah anda yakin akan menghapus data <?=$username?>?\n')" value="Hapus data">
                                    </td>
                                <?php
                                    }else {
                                ?>
                                    <td><input type="submit" value="Simpan" name="saveData" class="button btn btn-primary"></td>
                                <?php
                                    }
                                ?>
                                </div>
                            </div>
							<input type="hidden" name="id" value="<?=$id?>" />
                            </form>
                            <form action="<?=site_url('/admin/appuser/delete/')?>" id="deleteForm" method="post" style="display: inline;">
                                <input type="hidden" name="itemID" value="<?=$id?>" />
                            </form>  
                            <hr>
                            </div>
                        </div>
                    </div>
                    <!-- End  Kitchen Sink -->
                </div>
            </div>
        </div>
    </div>
</div>
<!-- CONTENT-WRAPPER SECTION END-->
