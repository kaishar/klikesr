<div class="content-wrapper" id="main">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php
                    //lets have the flashdata overright "$message" if it exists
                    if(CI::session()->flashdata('message'))
                    {
                        $message = CI::session()->flashdata('message');
                    }

                    if(CI::session()->flashdata('error'))
                    {
                        $error = CI::session()->flashdata('error');
                    }

                    if(function_exists('validation_errors') && validation_errors() != '')
                    {
                        $error = validation_errors();
                    }
                ?>

                <div id="js_error_container" class="alert alert-error" style="display:none;">
                    <p id="js_error"></p>
                </div>

                <div id="js_note_container" class="alert alert-note" style="display:none;"></div>

                <?php if (!empty($message)): ?>
                    <div class="alert alert-success" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <?php echo $message; ?>
                    </div>
                <?php endif; ?>

                <?php if (!empty($error)): ?>
                    <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <?php echo $error; ?>
                    </div>
                <?php endif; ?>                 
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h1 class="page-head-line">Group Access</h1>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <strong>Mapping Group Access</strong>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="text-left">
                                    <a href="<?=site_url('admin/usergroup/index/')?>" class="btn btn-primary"><i class="fa fa-list-alt"></i>&nbsp;<?php echo ('List user group'); ?></a>
                                </div>
                            </div>                                                   
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-12">
                            <?php 
                                $attributes = array('class' => 'form-horizontal');
                                echo form_open_multipart('admin/usergroup/form/'.$group_id, $attributes); 								
                            ?>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Group Name *</label>
                                <div class="col-sm-10">
                                <?php 
	                                echo form_input(['name'=>'group_name',
	                                		'value'=>assign_value('group_name', $group_name),
	                                		'class'=>'form-control',
	                                		'maxlength'=>'256',
	                                		'style'=>'width: 60%;']);
                                ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Privilege</label>
                                <div class="col-sm-10">
                                	<table cellspacing="0" cellpadding="2">
	                                    <tbody>
	                                        <tr style="color: white; font-weight: bold; cursor: pointer; background-color: rgb(49, 53, 62);">
	                                            <td>&nbsp;Module Name</td>
	                                            <td align="center">Read</td>
	                                            <td align="center">Write</td>
	                                        </tr>
	                                        <?php
	                                            foreach($modules_list as $modul):
	                                            	$read_checked = FALSE;
	                                           		$write_checked = FALSE;
	                                           		
	                                           		if (isset($priv_data[$modul->module_id]['r']) AND $priv_data[$modul->module_id]['r'] == 1) {
	                                           			$read_checked = TRUE;
	                                           		}
	                                           		
	                                           		if (isset($priv_data[$modul->module_id]['w']) AND $priv_data[$modul->module_id]['w'] == 1) {
	                                           			$read_checked = TRUE;
	                                           			$write_checked = TRUE;
	                                           		}
	                                        ?>
	                                                <tr>
	                                                    <td valign="top" style="width: 200px; font-weight: bold;">&nbsp;<?=$modul->module_name?></td>
	                                                    <td valign="top" style="width: 80px;" align="center">
	                                        <?php
	                                                        $data = array(
	                                                        'name'          => 'access[]',
	                                                        'value'         => $modul->module_id,
	                                                        'checked'       => $read_checked,
	                                                        'class'         => 'read',
	                                                        'style'         => 'border: 0;');
	
	                                                        echo form_checkbox($data);
	                                        ?>
	                                                    </td>
	                                                    <td valign="top" style="width: 80px;" align="center">
	                                        <?php
	                                                        $data = array(
	                                                        'name'          => 'full[]',
	                                                        'value'         => $modul->module_id,
	                                                        'checked'       => $write_checked,
	                                                        'class'         => 'read',
	                                                        'style'         => 'border: 0;');
	
	                                                        echo form_checkbox($data);
	                                        ?>
	                                                    </td>
	                                                </tr>
	                                        <?php
	                                            endforeach;
	                                        ?>
	                                    </tbody>
	                                </table>
                                </div>
                            </div>                            														
                            <div class="form-group">
                                <div class="col-sm-10 col-sm-offset-2">
                                <?php
                                    if ($group_id) 
                                    {
                                ?>
                                    <td>
                                        <input class="button btn btn-success" type="submit" value="Update" name="saveData">
                                        <input class="cancelButton button btn btn-warning" type="button" value="Batal" onclick="window.history.back()">                                        
                                    </td>
                                <?php
                                    }else {
                                ?>
                                    <td><input type="submit" value="Simpan" name="saveData" class="button btn btn-success"></td>
                                <?php
                                    }
                                ?>
                                </div>
                            </div>
							<input type="hidden" name="id" value="<?=$group_id?>" />
                            </form>
                            <form action="<?=site_url('admin/dealer/delete')?>" id="deleteForm" method="post" style="display: inline;">
                                <input type="hidden" name="itemID" value="<?=$group_id?>" />
                            </form>  
                            <hr>
                            </div>
                        </div>
                    </div>
                    <!-- End  Kitchen Sink -->
                </div>
            </div>
        </div>
    </div>
</div>
<!-- CONTENT-WRAPPER SECTION END-->
