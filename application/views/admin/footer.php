    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    &copy; <?=date('Y')?> KRESTON - INDONESIA
                </div>
            </div>
        </div>
    </footer>
    <!-- FOOTER SECTION END-->
    <!-- JAVASCRIPT AT THE BOTTOM TO REDUCE THE LOADING TIME  -->
    <!-- CORE JQUERY SCRIPTS -->
    <script src="<?=base_url('assets/js/jquery-2.1.4.min.js')?>"></script>
    <script src="<?=base_url('assets/js/updater.js'); ?>"></script> 
    <script src="<?=base_url('assets/js/gui.js'); ?>"></script> 
    <script src="<?=base_url('assets/js/form.js'); ?>"></script>
    <!-- BOOTSTRAP SCRIPTS  -->
    <script src="<?=base_url('assets/js/bootstrap.js')?>"></script>
	<script src="<?=base_url('assets/js/calendar.js')?>"></script> 
	<script src="<?=base_url('assets/plugins/datatables/js/jquery.dataTables.min.js')?>"></script>
	<script src="<?=base_url('assets/plugins/datatables/js/dataTables.bootstrap.js')?>"></script>
	<script type="text/javascript">

		//jika dipilih, kode obat akan masuk ke input dan modal di tutup
		$(document).on('click', '.pilih', function (e) {
			var str = '',
				pt = $(this).attr('data-kodeperusahaan'),
				nama_pt = $(this).attr('data-namaperusahaan');
				
			document.getElementById("nama_klien").value = pt;
			document.getElementById("nama_pt").value = nama_pt;
			$('#myModal').modal('hide');
						
			str = str + '<small><strong>PT. ' + nama_pt + '</strong>, Nomor Surat Opini Terakhir : <br /></small>';
			
			$.ajax({
				url:"<?=site_url('opini/find_company')?>",
				type:"post",
				data:"kode_pt="+pt,
				dataType: "JSON",
				success:function(resp) {
					if(resp == 0)
					{
						str = str + '<small>Penandatanganan dimulai dari <strong>I</strong></small>';
						
					}else {
						str = str + '<strong>' + resp + '</strong>';
					}
					
					$('#log').html(str);
				}
			})
		});

		//tabel lookup obat
		$(function () {
			$("#lookup").dataTable();
		});
	</script>
	<script type="text/javascript">
		var keyvalue = $('#key_value').val();
		var keyvariable = $('#key_variable').val();

		$("#key_value").prop("disabled", true);

		$('#key_variable').change(function() {
			keyvariable = $(this).find(":selected").val();

		    if(!keyvariable)
			{
				$("#key_value").prop("disabled", true);
			}else {
				$("#key_value").prop("disabled", false);
			}
		});
		
		if (keyvalue && !keyvariable) {
	        alert('Filter Pencarian belum dipilih!');
	    }
	</script>	
</body>
</html>