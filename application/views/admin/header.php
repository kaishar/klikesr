<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

    <meta charset="utf-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

    <meta name="description" content="Kreston Indonesia">

    <meta name="designer" content="alphasquad inc.">

    <meta name="author" content="" />

    <!--[if IE]>

        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <![endif]-->

    <title>Kreston Indonesia</title>

    <!-- BOOTSTRAP CORE STYLE  -->

    <link href="<?=base_url('assets/css/bootstrap.css')?>" rel="stylesheet" />

    <link href="<?=base_url('assets/css/animate.min.css')?>" rel="stylesheet" />

    <!-- FONT AWESOME ICONS  -->

    <link href="<?=base_url('assets/css/font-awesome.css')?>" rel="stylesheet" />

    <!-- CUSTOM STYLE  -->

    <link href="<?=base_url('assets/css/style.css')?>" rel="stylesheet" />

    <link href="<?=base_url('assets/plugins/fineuploader/fineuploader.css')?>" rel="stylesheet" />

    <link href="<?=base_url('assets/plugins/jcrop/jquery.Jcrop.min.css'); ?>" rel="stylesheet" type="text/css" />

    <link href="<?=base_url('assets/plugins/colorbox/colorbox.css');?>" rel="stylesheet" type="text/css" />

    <link href="<?=base_url('assets/plugins/jQuery-Impromptu/jquery-impromptu.css');?>" rel="stylesheet" type="text/css" />
	<link href="<?=base_url('assets/plugins/datatables/css/dataTables.bootstrap.css');?>" rel="stylesheet" type="text/css" />
	<link rel="shortcut icon" href="<?=base_url('assets/img/backend/favicon.ico'); ?>">

    <!-- HTML5 Shiv and Respond.js for IE8 support of HTML5 elements and media queries -->

    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

    <!--[if lt IE 9]>

        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>

        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

    <![endif]-->         

</head>

<body>

    <!-- HEADER END-->

    <div class="navbar set-radius-zero bg-admins">

        <div class="container">

            <div class="navbar-header">

                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">

                    <span class="icon-bar"></span>

                    <span class="icon-bar"></span>

                    <span class="icon-bar"></span>

                </button>               

            	<div class="col-md-1 col-sm-12">

                	<a class="navbar-brand" href="<?php echo site_url('admin'); ?>">

                		<img src="<?=base_url('assets/img/mainlogo-solid.png')?>" />                	

                	</a>

                </div>

            </div>

            <div class="left-div hidden-xs">            	

                <div class="user-settings-wrapper pull-right">

                    <ul class="nav">

                        <li class="dropdown">

                            <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">

                                <span class="glyphicon glyphicon-user" style="font-size: 25px;"></span>

                            </a>

                            <div class="dropdown-menu dropdown-settings">

                                <div class="media">

                                    <a class="media-left" href="#">

                                        <img src="<?=theme_upload('user/thumbnails/'.$profilepict)?>" alt="" class="img-rounded" />

                                    </a>

                                    <div class="media-body">

                                        <h4 class="media-heading"><?php echo $logininfo; ?></h4>

                                    </div>

                                </div>

                                <hr />

                                &nbsp; <a href="<?=site_url('admin/logout')?>" class="btn btn-danger">Logout</a>

                            </div>

                        </li>

                    </ul>

                </div>               

            </div>

        </div>

    </div>

    <!-- LOGO HEADER END-->

    <section class="menu-section">

        <div class="container">

            <div class="row">

                <div class="col-md-12">

                    <div class="navbar-collapse collapse">

                        <?php echo $main_menu; ?>

                    </div>

                </div>

            </div>            

        </div>

    </section>

    <!-- MENU SECTION END-->