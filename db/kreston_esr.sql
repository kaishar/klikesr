SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;


DROP TABLE IF EXISTS canned_messages;
CREATE TABLE IF NOT EXISTS canned_messages (
id int(9) unsigned NOT NULL,
  deletable tinyint(1) NOT NULL DEFAULT '1',
  `type` enum('internal','order') NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `subject` varchar(100) DEFAULT NULL,
  content text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS company;
CREATE TABLE IF NOT EXISTS company (
id int(11) NOT NULL,
  company_name varchar(200) NOT NULL,
  is_trash tinyint(4) NOT NULL DEFAULT '0',
  createdby varchar(20) NOT NULL,
  createdat datetime NOT NULL,
  updatedby varchar(20) NOT NULL,
  updatedat datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS customers;
CREATE TABLE IF NOT EXISTS customers (
id int(9) unsigned NOT NULL,
  firstname varchar(32) NOT NULL,
  lastname varchar(32) NOT NULL,
  email varchar(128) NOT NULL,
  email_subscribe tinyint(1) NOT NULL DEFAULT '0',
  phone varchar(32) NOT NULL,
  company varchar(128) NOT NULL,
  `password` varchar(40) DEFAULT NULL,
  active tinyint(1) NOT NULL,
  group_id int(11) NOT NULL DEFAULT '1',
  confirmed tinyint(1) NOT NULL DEFAULT '0',
  is_guest tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

INSERT INTO customers (id, firstname, lastname, email, email_subscribe, phone, company, password, active, group_id, confirmed, is_guest) VALUES
(1, '', '', '', 1, '', '', '', 1, 1, 0, 1);

DROP TABLE IF EXISTS customer_groups;
CREATE TABLE IF NOT EXISTS customer_groups (
id int(9) unsigned NOT NULL,
  `name` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS group_access;
CREATE TABLE IF NOT EXISTS group_access (
  group_id int(11) NOT NULL,
  module_id int(11) NOT NULL,
  r int(1) NOT NULL DEFAULT '0',
  w int(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO group_access (group_id, module_id, r, w) VALUES
(2, 7, 1, 1),
(1, 13, 1, 1),
(5, 7, 1, 0),
(1, 12, 1, 1),
(1, 11, 1, 1),
(1, 10, 1, 1),
(1, 7, 1, 1),
(1, 6, 1, 1),
(6, 11, 1, 0),
(7, 11, 1, 1),
(8, 12, 1, 0),
(9, 12, 1, 1),
(10, 13, 1, 0),
(11, 13, 1, 1),
(13, 10, 1, 1);

DROP TABLE IF EXISTS hrdsurats;
CREATE TABLE IF NOT EXISTS hrdsurats (
id int(11) NOT NULL,
  `no` varchar(255) NOT NULL,
  no_surat varchar(255) NOT NULL,
  tanggal date NOT NULL,
  kap varchar(255) NOT NULL,
  nama varchar(255) NOT NULL,
  alamat varchar(255) NOT NULL,
  keterangan varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  tujuan varchar(255) DEFAULT NULL,
  pembuat varchar(255) NOT NULL,
  is_trash tinyint(4) NOT NULL DEFAULT '0',
  createdby varchar(100) DEFAULT NULL,
  createdat datetime DEFAULT NULL,
  updatedby varchar(100) DEFAULT NULL,
  updatedat datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS mst_module;
CREATE TABLE IF NOT EXISTS mst_module (
module_id int(3) NOT NULL,
  module_name varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  module_path varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  module_desc varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

INSERT INTO mst_module (module_id, module_name, module_path, module_desc) VALUES
(6, 'system', 'system', 'Configure system behaviour, user'),
(7, 'hhes', 'hhes', 'Modul Surat HHES'),
(10, 'restore', 'restore', 'Restore deleted surats'),
(11, 'hrd', 'hrd', 'Modul Surat HRD'),
(12, 'quotation', 'quotation', 'Modul Surat Quotation'),
(13, 'opini', 'opini', 'Modul Surat Opinion');

DROP TABLE IF EXISTS opinions;
CREATE TABLE IF NOT EXISTS opinions (
id int(11) NOT NULL,
  no_op varchar(244) NOT NULL,
  `no` varchar(4) NOT NULL,
  `type` varchar(10) DEFAULT NULL,
  tujuan varchar(255) DEFAULT NULL,
  kap varchar(30) NOT NULL,
  tanggal date NOT NULL,
  penandatanganan varchar(5) NOT NULL,
  init_klien varchar(255) DEFAULT NULL,
  nama_klien int(11) NOT NULL,
  alamat varchar(255) DEFAULT NULL,
  pembuat varchar(255) NOT NULL,
  keterangan text NOT NULL,
  is_trash tinyint(1) NOT NULL DEFAULT '0',
  partner varchar(20) NOT NULL,
  manager varchar(100) NOT NULL,
  nama_peminta_no varchar(150) NOT NULL,
  proses_flag varchar(2) NOT NULL,
  tanggal_laporan date NOT NULL,
  change_no tinyint(4) NOT NULL,
  createdby varchar(100) DEFAULT NULL,
  createdat datetime DEFAULT NULL,
  updatedby varchar(100) DEFAULT NULL,
  updatedat datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS pages;
CREATE TABLE IF NOT EXISTS pages (
id int(9) NOT NULL,
  parent_id int(9) NOT NULL,
  title varchar(128) NOT NULL,
  menu_title varchar(128) NOT NULL,
  slug varchar(255) NOT NULL,
  content longtext NOT NULL,
  sequence int(11) NOT NULL DEFAULT '0',
  seo_title text NOT NULL,
  meta text NOT NULL,
  url varchar(255) NOT NULL,
  new_window tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS quotations;
CREATE TABLE IF NOT EXISTS quotations (
id int(11) NOT NULL,
  no_quo varchar(244) NOT NULL,
  `no` varchar(4) NOT NULL,
  `type` varchar(200) NOT NULL,
  tujuan varchar(244) NOT NULL,
  kap varchar(200) NOT NULL,
  tanggal date NOT NULL,
  nama_klien varchar(255) NOT NULL,
  alamat varchar(255) NOT NULL,
  pembuat varchar(255) NOT NULL,
  keterangan text NOT NULL,
  is_trash tinyint(1) NOT NULL DEFAULT '0',
  change_no tinyint(4) NOT NULL,
  createdby varchar(100) DEFAULT NULL,
  createdat datetime DEFAULT NULL,
  updatedby varchar(100) DEFAULT NULL,
  updatedat datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS rev_opinions;
CREATE TABLE IF NOT EXISTS rev_opinions (
id int(11) NOT NULL,
  no_re varchar(244) NOT NULL,
  `no` varchar(4) NOT NULL,
  tujuan varchar(255) NOT NULL,
  kap varchar(150) NOT NULL,
  tanggal date NOT NULL,
  init_klien varchar(255) NOT NULL,
  nama_klien varchar(255) NOT NULL,
  alamat varchar(255) NOT NULL,
  pembuat varchar(255) NOT NULL,
  keterangan text NOT NULL,
  is_trash tinyint(1) NOT NULL DEFAULT '0',
  change_no tinyint(4) NOT NULL,
  createdby varchar(100) DEFAULT NULL,
  createdat date DEFAULT NULL,
  updatedby varchar(100) DEFAULT NULL,
  updatedat date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS search;
CREATE TABLE IF NOT EXISTS search (
  `code` varchar(40) NOT NULL,
  term varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS settings;
CREATE TABLE IF NOT EXISTS settings (
id int(9) unsigned NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  setting_key varchar(255) DEFAULT NULL,
  setting longtext
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;

INSERT INTO settings (id, code, setting_key, setting) VALUES
(1, 'cms', 'theme', 'default'),
(2, 'cms', 'locale', 'id_ID'),
(3, 'cms', 'currency_iso', 'IDR'),
(4, 'cms', 'new_customer_status', NULL),
(7, 'cms', 'default_meta_description', 'Kreston Indonesia'),
(8, 'cms', 'default_meta_keywords', 'Kreston Indonesia'),
(9, 'cms', 'timezone', 'Asia/Jakarta'),
(10, 'cms', 'company_name', 'Kreston Indonesia'),
(11, 'cms', 'homepage', NULL),
(12, 'cms', 'email_to', 'no-reply@kreston-indonesia.co.id'),
(13, 'cms', 'email_from', 'no-reply@kreston-indonesia.co.id'),
(14, 'cms', 'email_method', 'mail'),
(15, 'cms', 'smtp_server', ''),
(16, 'cms', 'smtp_port', '25'),
(17, 'cms', 'smtp_username', ''),
(18, 'cms', 'smtp_password', ''),
(21, 'cms', 'address1', 'Jakarta'),
(22, 'cms', 'address2', ''),
(23, 'cms', 'city', 'Jakarta'),
(25, 'cms', 'zip', ''),
(26, 'cms', 'stage_username', ''),
(27, 'cms', 'stage_password', ''),
(31, 'cms', 'ssl_support', '0'),
(32, 'cms', 'require_login', '0'),
(36, 'cms', 'enabled', '0'),
(40, 'cms', 'school_code', '');

DROP TABLE IF EXISTS surats;
CREATE TABLE IF NOT EXISTS surats (
id int(11) NOT NULL,
  no_surat varchar(244) NOT NULL,
  `no` varchar(4) NOT NULL,
  `type` varchar(10) NOT NULL,
  tujuan varchar(10) NOT NULL,
  kap varchar(255) NOT NULL,
  tanggal date NOT NULL,
  nama_klien varchar(255) NOT NULL,
  alamat varchar(255) NOT NULL,
  pembuat varchar(255) NOT NULL,
  keterangan text NOT NULL,
  is_trash tinyint(1) NOT NULL DEFAULT '0',
  createdby varchar(100) DEFAULT NULL,
  createdat datetime DEFAULT NULL,
  updatedby varchar(100) DEFAULT NULL,
  updatedat datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS user;
CREATE TABLE IF NOT EXISTS `user` (
id int(11) NOT NULL,
  username varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  realname varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  email varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  user_type smallint(2) DEFAULT NULL,
  user_image varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  social_media text COLLATE utf8_unicode_ci,
  nip varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  last_login datetime DEFAULT NULL,
  last_login_ip char(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  groups varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  input_date date DEFAULT '0000-00-00',
  last_update date DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO user (id, username, realname, password, email, user_type, user_image, social_media, nip, last_login, last_login_ip, groups, input_date, last_update) VALUES
(1, 'admin', 'Super User', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'webmaster@gmail.com', NULL, 'login.png', NULL, '197034', '2017-10-06 10:12:01', '127.0.0.1', 'a:1:{i:0;s:1:"1";}', '2015-11-11', '2017-06-02'),
(2, 'user01', 'User Full Access', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'user01@kreston.id', 1, 'login1.png', NULL, '12960', '2017-06-02 09:51:58', '182.253.5.10', 'a:1:{i:0;s:1:"2";}', '2017-04-09', '2017-04-12'),
(3, 'user02', 'User Reader Only', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'user02@kreston.id', 1, 'login2.png', NULL, '12345', '2017-04-12 05:48:07', '127.0.0.1', 'a:1:{i:0;s:1:"5";}', '2017-04-12', NULL),
(6, 'kikit.noviasari', 'Kikit Novia Sari', '5c8cf79017484d5091e0d9f127c6a19031d7dc25', 'kikit.noviasari@kreston.co.id', 1, 'Kikit.jpg', NULL, '50183', '2017-08-24 10:23:10', '182.253.5.10', 'a:2:{i:0;s:1:"2";i:1;s:2:"11";}', '2017-08-21', NULL),
(5, 'faidz.rizqi', 'M. Faidz Rizqi', '5c8cf79017484d5091e0d9f127c6a19031d7dc25', 'faidz.rizqi@kreston.co.id', 1, 'Faiz1.jpg', NULL, '50246', '2017-08-08 11:53:55', '182.253.5.10', 'a:1:{i:0;s:2:"11";}', '2017-07-06', '2017-08-21'),
(7, 'putri.elsa', 'Putri Elsa', '5c8cf79017484d5091e0d9f127c6a19031d7dc25', 'putri.elsa@kreston.co.id', 1, 'elsa.jpg', NULL, '50100', '2017-08-24 10:22:13', '182.253.5.10', 'a:1:{i:0;s:1:"2";}', '2017-08-21', NULL),
(8, 'greyvi.imelda', 'Greyvi Imelda', '5c8cf79017484d5091e0d9f127c6a19031d7dc25', 'greyvi.imelda@kreston.co.id', 1, NULL, NULL, '50021', NULL, NULL, 'literal{NULL}', '2017-08-21', NULL),
(9, 'nuria.sekarayu', 'Nuria Sekar Ayu', '5c8cf79017484d5091e0d9f127c6a19031d7dc25', 'nuria.sekarayu@kreston.co.id', 1, 'Nuria.jpg', NULL, '50015', NULL, NULL, 'a:4:{i:0;s:1:"2";i:1;s:1:"7";i:2;s:1:"9";i:3;s:2:"11";}', '2017-08-21', NULL),
(10, 'ardy.andrean', 'Ardy Andrean', '5c8cf79017484d5091e0d9f127c6a19031d7dc25', 'ardy.andrean@kreston.co.id', 1, NULL, NULL, '50202', '2017-09-05 11:02:46', '202.158.37.130', 'a:1:{i:0;s:2:"11";}', '2017-08-21', NULL);

DROP TABLE IF EXISTS user_group;
CREATE TABLE IF NOT EXISTS user_group (
group_id smallint(6) NOT NULL,
  group_name varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  input_date date DEFAULT NULL,
  last_update date DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO user_group (group_id, group_name, input_date, last_update) VALUES
(1, 'DEV', '2015-11-11', '2017-05-13'),
(2, 'HHES FULL', '2015-11-18', '2017-04-26'),
(5, 'HHES READ', '2017-04-09', '2017-04-26'),
(6, 'HRD READ', '2017-04-26', NULL),
(7, 'HRD FULL', '2017-04-26', NULL),
(8, 'QUOTATION READ', '2017-04-26', NULL),
(9, 'QUOTATION FULL', '2017-04-26', NULL),
(10, 'OPINI READ', '2017-04-26', NULL),
(11, 'OPINI FULL', '2017-04-26', NULL),
(13, 'RESTORE', '2017-05-13', NULL);

DROP TABLE IF EXISTS variable;
CREATE TABLE IF NOT EXISTS variable (
ID int(11) NOT NULL,
  VARIABLE varchar(30) NOT NULL,
  `VALUE` varchar(100) NOT NULL DEFAULT '',
  DESCRIPTION varchar(50) DEFAULT NULL,
  CREATEDBY varchar(12) DEFAULT NULL,
  CREATEDAT datetime DEFAULT NULL,
  CHANGEDBY varchar(12) DEFAULT NULL,
  CHANGEDAT datetime DEFAULT NULL,
  ORDERBY tinyint(4) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=123 DEFAULT CHARSET=utf8;

INSERT INTO variable (ID, VARIABLE, VALUE, DESCRIPTION, CREATEDBY, CREATEDAT, CHANGEDBY, CHANGEDAT, ORDERBY) VALUES
(1, 'TYPE_HHES', 'CL', 'Confirmation Letter', NULL, NULL, NULL, NULL, 0),
(2, 'TYPE_HHES', 'GN', 'General Letter', NULL, NULL, NULL, NULL, 0),
(3, 'TYPE_HHES', 'SK', 'Surat Keputusan', NULL, NULL, NULL, NULL, 0),
(4, 'TYPE_HHES', 'ML', 'Management Letter', NULL, NULL, NULL, NULL, 0),
(5, 'TUJUAN_HHES', 'AHW', 'AHW', NULL, NULL, NULL, NULL, 0),
(6, 'TUJUAN_HHES', 'EAW', 'EAW', NULL, NULL, NULL, NULL, 0),
(7, 'TUJUAN_HHES', 'LW', 'LW', NULL, NULL, NULL, NULL, 0),
(8, 'TUJUAN_HHES', 'RM', 'RM', NULL, NULL, NULL, NULL, 0),
(9, 'TUJUAN_HHES', 'ISS', 'ISS', NULL, NULL, NULL, NULL, 0),
(10, 'TUJUAN_OPINI', 'TS', 'TS', NULL, NULL, NULL, NULL, 0),
(11, 'TUJUAN_OPINI', 'AW', 'AW', NULL, NULL, NULL, NULL, 0),
(12, 'KAP', '03', 'Medan', NULL, NULL, NULL, NULL, 0),
(13, 'KAP', '02', 'Surabaya', NULL, NULL, NULL, NULL, 0),
(14, 'KAP', '01', 'Jakarta', NULL, NULL, NULL, NULL, 0),
(15, 'TYPE_OPINIONS', 'GN', 'General Letter', '', NULL, NULL, NULL, 0),
(16, 'TYPE_OPINIONS', 'SK', 'Surat Keputusan', NULL, NULL, NULL, NULL, 0),
(17, 'TYPE_OPINIONS', 'QUO', 'Surat Quotations', NULL, NULL, NULL, NULL, 0),
(18, 'TYPE_OPINIONS', 'ML', 'Management Letter', NULL, NULL, NULL, NULL, 0),
(19, 'TUJUAN_OPINI', 'ED', 'ED', NULL, NULL, NULL, NULL, 0),
(20, 'TUJUAN_OPINI', 'EP', 'EP', NULL, NULL, NULL, NULL, 0),
(21, 'TUJUAN_OPINI', 'SW', 'SW', NULL, NULL, NULL, NULL, 0),
(22, 'TUJUAN_OPINI', 'MPS', 'MPS', NULL, NULL, NULL, NULL, 0),
(23, 'TUJUAN_OPINI', 'AHW', 'AHW', NULL, NULL, NULL, NULL, 0),
(24, 'TUJUAN_OPINI', 'EAW', 'EAW', NULL, NULL, NULL, NULL, 0),
(25, 'TUJUAN_OPINI', 'RM', 'RM', NULL, NULL, NULL, NULL, 0),
(26, 'TUJUAN_OPINI', 'RP', 'RP', NULL, NULL, NULL, NULL, 0),
(27, 'TUJUAN_OPINI', 'RS', 'RS', NULL, NULL, NULL, NULL, 0),
(28, 'TUJUAN_OPINI', 'SP', 'SP', NULL, NULL, NULL, NULL, 0),
(29, 'TUJUAN_OPINI', 'UGS', 'UGS', NULL, NULL, NULL, NULL, 0),
(30, 'TUJUAN_OPINI', 'AH', 'AH', NULL, NULL, NULL, NULL, 0),
(31, 'TUJUAN_OPINI', 'LNS', 'LNS', NULL, NULL, NULL, NULL, 0),
(32, 'TUJUAN_OPINI', 'ISS', 'ISS', NULL, NULL, NULL, NULL, 0),
(33, 'TUJUAN_OPINI', 'LW', 'LW', NULL, NULL, NULL, NULL, 0),
(34, 'TUJUAN_OPINI', 'DPL', 'DPL', NULL, NULL, NULL, NULL, 0),
(35, 'TUJUAN_OPINI', 'FS', 'FS', NULL, NULL, NULL, NULL, 0),
(36, 'TUJUAN_OPINI', 'IS', 'IS', NULL, NULL, NULL, NULL, 0),
(37, 'TUJUAN_OPINI', 'SD', 'SD', NULL, NULL, NULL, NULL, 0),
(38, 'TUJUAN_OPINI', 'DH', 'DH', NULL, NULL, NULL, NULL, 0),
(39, 'TUJUAN_OPINI', 'FD', 'FD', NULL, NULL, NULL, NULL, 0),
(40, 'TUJUAN_OPINI', 'WEL', 'WEL', NULL, NULL, NULL, NULL, 0),
(41, 'TUJUAN_OPINI', 'ARY', 'ARY', NULL, NULL, NULL, NULL, 0),
(42, 'TUJUAN_OPINI', 'JT', 'JT', NULL, NULL, NULL, NULL, 0),
(43, 'TUJUAN_OPINI', 'RSL', 'RSL', NULL, NULL, NULL, NULL, 0),
(44, 'TUJUAN_OPINI', 'RW', 'RW', NULL, NULL, NULL, NULL, 0),
(45, 'TUJUAN_OPINI', 'LS', 'LS', NULL, NULL, NULL, NULL, 0),
(46, 'TUJUAN_OPINI', 'SL', 'SL', NULL, NULL, NULL, NULL, 0),
(47, 'TUJUAN_OPINI', 'NM', 'NM', NULL, NULL, NULL, NULL, 0),
(48, 'TUJUAN_OPINI', 'BDC', 'BDC', NULL, NULL, NULL, NULL, 0),
(49, 'TUJUAN_OPINI', 'FA', 'FA', NULL, NULL, NULL, NULL, 0),
(50, 'TUJUAN_OPINI', 'ISD', 'ISD', NULL, NULL, NULL, NULL, 0),
(51, 'TUJUAN', 'AW', 'AW', NULL, NULL, NULL, NULL, 0),
(52, 'TUJUAN', 'TS', 'TS', NULL, NULL, NULL, NULL, 0),
(53, 'TUJUAN', 'ED', 'ED', NULL, NULL, NULL, NULL, 0),
(54, 'TUJUAN', 'EP', 'EP', NULL, NULL, NULL, NULL, 0),
(55, 'TUJUAN', 'SW', 'SW', NULL, NULL, NULL, NULL, 0),
(56, 'TUJUAN', 'MPS', 'MPS', NULL, NULL, NULL, NULL, 0),
(57, 'TUJUAN', 'AHW', 'AHW', NULL, NULL, NULL, NULL, 0),
(58, 'TUJUAN', 'EAW', 'EAW', NULL, NULL, NULL, NULL, 0),
(59, 'TUJUAN', 'RM', 'RM', NULL, NULL, NULL, NULL, 0),
(60, 'TUJUAN', 'RP', 'RP', NULL, NULL, NULL, NULL, 0),
(61, 'TUJUAN', 'RS', 'RS', NULL, NULL, NULL, NULL, 0),
(62, 'TUJUAN', 'SP', 'SP', NULL, NULL, NULL, NULL, 0),
(63, 'TUJUAN', 'UGS', 'UGS', NULL, NULL, NULL, NULL, 0),
(64, 'TUJUAN', 'AH', 'AH', NULL, NULL, NULL, NULL, 0),
(65, 'TUJUAN', 'LNS', 'LNS', NULL, NULL, NULL, NULL, 0),
(66, 'TUJUAN', 'ISS', 'ISS', NULL, NULL, NULL, NULL, 0),
(67, 'TUJUAN', 'LW', 'LW', NULL, NULL, NULL, NULL, 0),
(68, 'TUJUAN', 'DPL', 'DPL', NULL, NULL, NULL, NULL, 0),
(69, 'TUJUAN', 'FS', 'FS', NULL, NULL, NULL, NULL, 0),
(70, 'TUJUAN', 'IS', 'IS', NULL, NULL, NULL, NULL, 0),
(71, 'TUJUAN', 'SD', 'SD', NULL, NULL, NULL, NULL, 0),
(72, 'TUJUAN', 'DH', 'DH', NULL, NULL, NULL, NULL, 0),
(73, 'TUJUAN', 'FD', 'FD', NULL, NULL, NULL, NULL, 0),
(74, 'TUJUAN', 'WEL', 'WEL', NULL, NULL, NULL, NULL, 0),
(75, 'TUJUAN', 'ARY', 'ARY', NULL, NULL, NULL, NULL, 0),
(76, 'TUJUAN', 'JT', 'JT', NULL, NULL, NULL, NULL, 0),
(77, 'TUJUAN', 'RSL', 'RSL', NULL, NULL, NULL, NULL, 0),
(78, 'TUJUAN', 'RW', 'RW', NULL, NULL, NULL, NULL, 0),
(79, 'TUJUAN', 'LS', 'LS', NULL, NULL, NULL, NULL, 0),
(80, 'TUJUAN', 'SL', 'SL', NULL, NULL, NULL, NULL, 0),
(81, 'TUJUAN', 'NM', 'NM', NULL, NULL, NULL, NULL, 0),
(82, 'TUJUAN', 'BDC', 'BDC', NULL, NULL, NULL, NULL, 0),
(83, 'TUJUAN', 'FA', 'FA', NULL, NULL, NULL, NULL, 0),
(84, 'TUJUAN', 'ISD', 'ISD', NULL, NULL, NULL, NULL, 0),
(85, 'TUJUAN_HRD', 'DAN', 'Dina Anggraini', NULL, NULL, NULL, NULL, 0),
(86, 'TUJUAN_HRD', 'NSP', 'Nuria Sekar Ayu', NULL, NULL, NULL, NULL, 0),
(87, 'TUJUAN_HRD', 'JUN', 'Juni', NULL, NULL, NULL, NULL, 0),
(88, 'TYPE_HRD', 'SK', 'Surat Keterangan', NULL, NULL, NULL, NULL, 0),
(89, 'TYPE_HRD', 'SKK', 'Surat Kontrak Kerja', NULL, NULL, NULL, NULL, 0),
(90, 'TYPE_HRD', 'SPK', 'Surat Pengalaman Kerja', NULL, NULL, NULL, NULL, 0),
(91, 'TYPE_HRD', 'SNK', 'Surat Naik Jabatan', NULL, NULL, NULL, NULL, 0),
(92, 'TYPE_HRD', 'SPKT', 'Surat Pengangkatan Karyawan', NULL, NULL, NULL, NULL, 0),
(93, 'TYPE_HRD', 'SPKJ', 'Surat Perjanjian Kerja', NULL, NULL, NULL, NULL, 0),
(94, 'TYPE_HRD', 'SP', 'Surat Peringatan', NULL, NULL, NULL, NULL, 0),
(95, 'TYPE_HRD', 'SIC', 'Surat Izin Cuti', NULL, NULL, NULL, NULL, 0),
(96, 'TYPE_HRD', 'SPD', 'Surat Pengunduran Diri', NULL, NULL, NULL, NULL, 0),
(97, 'TYPE_HRD', 'SPPHK', 'Surat PHK', NULL, NULL, NULL, NULL, 0),
(98, 'TYPE_HRD', 'SKM', 'Surat Keterangan Magang', NULL, NULL, NULL, NULL, 0),
(99, 'TYPE_HRD', 'SPT', 'Surat Perjanjian Training', NULL, NULL, NULL, NULL, 0),
(100, 'TYPE_HRD', 'SLL', 'Surat Lain-Lain', NULL, NULL, NULL, NULL, 0),
(101, 'TUJUAN_QUO', 'WEL', 'WEL', NULL, NULL, NULL, NULL, 0),
(102, 'PROSES_FLAG', 'P', 'PROSES', 'SQL', '2017-05-13 00:00:00', NULL, NULL, 0),
(103, 'PROSES_FLAG', 'F', 'FINAL', 'SQL', '2017-05-13 00:00:00', NULL, NULL, 0),
(104, 'TUJUAN_HHES', 'WEL', 'WEL', NULL, NULL, NULL, NULL, 0),
(105, 'TUJUAN_HHES', 'AH', 'AH', NULL, NULL, NULL, NULL, 0),
(106, 'TUJUAN_QUO', 'AHW', 'AHW', NULL, NULL, NULL, NULL, 0),
(107, 'TUJUAN_QUO', 'EAW', 'EAW', NULL, NULL, NULL, NULL, 0),
(108, 'TUJUAN_QUO', 'LW', 'LW', NULL, NULL, NULL, NULL, 0),
(109, 'TUJUAN_QUO', 'RM', 'RM', NULL, NULL, NULL, NULL, 0),
(110, 'TUJUAN_QUO', 'ISS', 'ISS', NULL, NULL, NULL, NULL, 0),
(111, 'TUJUAN_QUO', 'AH', 'AH', NULL, NULL, NULL, NULL, 0),
(112, 'TUJUAN_QUO', 'FAS', 'FAS', NULL, NULL, NULL, NULL, 0),
(113, 'TUJUAN_QUO', 'RAS', 'RAS', NULL, NULL, NULL, NULL, 0),
(117, 'PARTNER_OPINI', 'EAW', 'ERWIN WINATA', 'admin', '2017-10-04 03:59:52', NULL, NULL, 2),
(115, 'PARTNER_OPINI', 'AH', 'AMNY HUTAGAOL', NULL, NULL, NULL, NULL, 8),
(116, 'PARTNER_OPINI', 'AHW', 'HENDRA WINATA', 'admin', '2017-10-04 03:57:32', NULL, NULL, 1),
(118, 'PARTNER_OPINI', 'LW', 'LIANTY WIDJAJA', 'admin', '2017-10-04 04:00:37', NULL, NULL, 3),
(119, 'PARTNER_OPINI', 'RM', 'RAZMAL MUIN', 'admin', '2017-10-04 04:01:05', NULL, NULL, 4),
(120, 'PARTNER_OPINI', 'RS', 'ROBBY SUMARGO', 'admin', '2017-10-04 04:01:45', NULL, NULL, 5),
(121, 'PARTNER_OPINI', 'ISS', 'ISKARIMAN SUPARDJO', 'admin', '2017-10-04 04:02:22', NULL, NULL, 6),
(122, 'PARTNER_OPINI', 'WEL', 'WELLY ADRIANTO', 'admin', '2017-10-04 04:02:55', NULL, NULL, 7);


ALTER TABLE canned_messages
 ADD PRIMARY KEY (id);

ALTER TABLE company
 ADD PRIMARY KEY (id), ADD KEY idx_company_01 (is_trash);

ALTER TABLE customers
 ADD PRIMARY KEY (id);

ALTER TABLE customer_groups
 ADD PRIMARY KEY (id);

ALTER TABLE group_access
 ADD PRIMARY KEY (group_id,module_id);

ALTER TABLE hrdsurats
 ADD PRIMARY KEY (id);

ALTER TABLE mst_module
 ADD PRIMARY KEY (module_id), ADD UNIQUE KEY module_name (module_name,module_path);

ALTER TABLE opinions
 ADD PRIMARY KEY (id);

ALTER TABLE pages
 ADD PRIMARY KEY (id);

ALTER TABLE quotations
 ADD PRIMARY KEY (id);

ALTER TABLE rev_opinions
 ADD PRIMARY KEY (id);

ALTER TABLE search
 ADD PRIMARY KEY (`code`);

ALTER TABLE settings
 ADD PRIMARY KEY (id);

ALTER TABLE surats
 ADD PRIMARY KEY (id);

ALTER TABLE user
 ADD PRIMARY KEY (id), ADD UNIQUE KEY username (username), ADD KEY realname (realname);

ALTER TABLE user_group
 ADD PRIMARY KEY (group_id), ADD UNIQUE KEY group_name (group_name);

ALTER TABLE variable
 ADD PRIMARY KEY (ID);


ALTER TABLE canned_messages
MODIFY id int(9) unsigned NOT NULL AUTO_INCREMENT;
ALTER TABLE company
MODIFY id int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE customers
MODIFY id int(9) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
ALTER TABLE customer_groups
MODIFY id int(9) unsigned NOT NULL AUTO_INCREMENT;
ALTER TABLE hrdsurats
MODIFY id int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE mst_module
MODIFY module_id int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
ALTER TABLE opinions
MODIFY id int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE pages
MODIFY id int(9) NOT NULL AUTO_INCREMENT;
ALTER TABLE quotations
MODIFY id int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE rev_opinions
MODIFY id int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE settings
MODIFY id int(9) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=41;
ALTER TABLE surats
MODIFY id int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE user
MODIFY id int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
ALTER TABLE user_group
MODIFY group_id smallint(6) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
ALTER TABLE variable
MODIFY ID int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=123;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
