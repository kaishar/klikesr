﻿function getGeneralService(URLService, parameters ) {
    jQuery.support.cors = true;
    var rps = $.ajax({
        //crossDomain: true,
        type: "POST",
        contenttype: "application/json; charset=utf-8",
        data: parameters,
        url: URLService,
        dataType: "json",
        //spinner: 'someSpinner',
        //beforeSend: function() { $.mobile.loadingMessage = false; }, //{ $.mobile.utils.showWaitBox("a", "Please wait..."); }, //Show spinner
        //complete: function() { $.mobile.loadingMessage = false; }, //Hide spinner 
        success: function(response) {
            result = response;
        },
        error: function(message) {
        	alert(message.responseText);  
        	//console.log(message);
        },
        failure: function(response) {
            FailedMessage(response);
        }
    });
    return rps;
}

function FailedMessage(result) {
    alert(result.status + ' ' + result.statusText);
}

function showNotification(strMessage) {
    $("<div class='ui-loader ui-overlay-shadow ui-body-e ui-corner-all'>" + strMessage + "</div>").css({ "display": "block", "opacity": 0.96})
                  .appendTo($.mobile.pageContainer)
                  .delay(3000)
                  .fadeOut(400, function() {
                      $(this).remove();
                  });
}


/*Start Formater*/
function ChangeDateFormatFromJSON(jsondate) { // formatting in normally acakadut date format from JSON data
    jsondate = jsondate.replace("/Date(", "").replace(")/", "");
    if (jsondate.indexOf("+") > 0) {
        jsondate = jsondate.substring(0, jsondate.indexOf("+"));
    }
    else if (jsondate.indexOf("-") > 0) {
        jsondate = jsondate.substring(0, jsondate.indexOf("-"));
    }

    var date = new Date(parseInt(jsondate, 10));
    var month = date.getMonth() + 1 < 10 ?
		"0" + (date.getMonth() + 1) : date.getMonth() + 1;
    var currentDate = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
    //return date.getFullYear() + "-" + month + "-" + currentDate;
    return currentDate +"/" + month + "/" + date.getFullYear();
}

function getParameterByName(name) { // get query string(URL parameter) from format: ?loginid=adsa&password=sdfs#MainPage 
    var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
}

function GetParameterValues(param) { // get query string(URL parameter) from format: ../entah.html#MainPage?loginid=adsa&password=sdfs
    var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < url.length; i++) {
        var urlparam = url[i].split('=');
        if (urlparam[0] == param) {
            return urlparam[1];
        }
    }
}

function parseJSONErrorMsg(JSONError) {
    var str = JSONError;
    var delimiter = ':';
    var start = 1;
    var tokens = str.split(delimiter).slice(start);
    var resultmsg = tokens.join(delimiter);
    return resultmsg;
}
/*End Formater*/

/*Start Data*/
function saveSessionData(DataKey,DataValue) {    
    sessionStorage.setItem(DataKey, DataValue);
}

function getSessionData(DataKey) {    
    return sessionStorage.getItem(DataKey);
}

function Logout() {
    $.removeCookie("NIK");
    sessionStorage.clear();
    window.location.href = "../../Default.aspx";
}

function deleteAllCookies() {
    var cookies = document.cookie.split(";");

    for (var i = 0; i < cookies.length; i++) {
        var cookie = cookies[i];
        var eqPos = cookie.indexOf("=");
        var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
        document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
    }
}

function getUrlParameterData(ParamName) {
    var ParamValue = null;
    if (getParameterByName(ParamName) != null) {
        ParamValue = getParameterByName(ParamName);
    }
    else {      
        ParamValue = GetParameterValues(ParamName);
    }
    return ParamValue;
}

function AlphaNumCheck(e) {
    var charCode = (e.which) ? e.which : e.keyCode;
    if (charCode == 8) return true;

    var keynum;
    var keychar;
    var charcheck = /[a-zA-Z0-9]/;
    if (window.event) // IE
    {
        keynum = e.keyCode;
    }
    else {
        if (e.which) // Netscape/Firefox/Opera
        {
            keynum = e.which;
        }
        else return true;
    }

    keychar = String.fromCharCode(keynum);
    return charcheck.test(keychar);
}

function ValidateNumInput(TextFieldID) {
    var inputyped = TextFieldID.value;
    var limitField = inputyped.length;
    TextFieldID.value = TextFieldID.value.replace(/[^0-9_\-\.]/g, '');
}

function DateDiff(date1, date2) {
    var datediff = date1.getTime() - date2.getTime(); //store the getTime diff - or +
    return (datediff / (24 * 60 * 60 * 1000)); //Convert values to -/+ days and return value
}

function FormatDate(InputDate) {
    var ddInputDate = '';
    var mmInputDate = '';
    var yyyyInputDate = '';
    var FinalInputDate = '';
    if (InputDate.getDate() < 10) {
        ddInputDate = '0' + InputDate.getDate();
    } else {
        ddInputDate = InputDate.getDate();
    }
    if (InputDate.getMonth() + 1 < 10) {
        mmInputDate = '0' + (InputDate.getMonth() + 1);
    } else {
        mmInputDate = (InputDate.getMonth() + 1);
    }
    yyyyInputDate = InputDate.getFullYear();
    if (yyyyInputDate == 1)
        return '';
    else
        return yyyyInputDate + '-' + mmInputDate + '-' + ddInputDate;
}


function addThousandsSeparator(x) {
    //remove commas
    retVal = x ? parseFloat(x.replace(/,/g, '')) : 0;

    //apply formatting
    return retVal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function trimString(str) {
    return str.replace(/^\s+|\s+$/g, "");
}
/*End Data*/