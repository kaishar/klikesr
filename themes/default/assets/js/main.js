jQuery(function($) {'use strict';

	//Initiat WOW JS
	new WOW().init();

	$('.timer').each(count);
	function count(options) {
		var $this = $(this);
		options = $.extend({}, options || {}, $this.data('countToOptions') || {});
		$this.countTo(options);
	}
	//copyright
	$('img').bind('contextmenu', function(e){
	return false;
	});
	
	$('img').bind('mouseover', function () {
		var img = $(this); // cache query
			if (img.title) {
			  return;
			}
		img.attr('title', img.attr('alt'));
	});
	//Scrollspy
	$('body').scrollspy({ target: '#navalpha' })
	//main
	var $toTop = $('#toTop');
	$('#toTop').hide();
	$(window).scroll(function () 
	{
		if ($(this).scrollTop() > 100)
		{
			$toTop.fadeIn('slow');
		} else if ($toTop.is(':visible')) 
		{
			$toTop.fadeOut('slow');
		}
	});

	$('#toTop').click(function()
	{
		$('html,body').animate({scrollTop:0},1200);
		return false;
	});

	$('.gowes').click(function(){
		$('html, body').animate({
			scrollTop: $( $(this).attr('href') ).offset().top
		}, 600);
		return false;
	});
	
	$('#header').affix({
	  offset: {
		top: 70
	  }
	});
});

//search
$('.searchclk').click(function () {
	$('.searchhere').toggleClass('expanded');
});
	
//logo member slider
$(document).ready(function(){
	$('.koleksi').slick({
		dots: false,
		infinite: false,
		autoplay: true,
		speed: 300,
		slidesToShow: 4,
		slidesToScroll: 4,
		responsive: [
			{
				breakpoint: 1024,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3,
					infinite: true,
					dots: false
				}
			},
			{
				breakpoint: 600,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2,
					dots: false
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					dots: false
				}
			},
			// You can unslick at a given breakpoint now by adding:
			// settings: "unslick"
			// instead of a settings object
			{
				breakpoint: 300,
				settings: "unslick" // destroys slick
			}
		]
	});
});

$(document).ready(function(){
	// hide .navbar first
	$(".navbar").hide();

	// fade in .navbar
	$(function () {
		$(window).scroll(function () {
			// set distance user needs to scroll before we fadeIn navbar
			if ($(this).scrollTop() > 370) {
				$('.navbar').fadeIn();
			} else {
				$('.navbar').fadeOut();
			}
		});
	});
});
