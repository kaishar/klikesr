<!-- footer -->
    <footer id="footer" class="bg-digital framesection">
        <div class="container contentfull">
            <div class="row">
                <div class="col-sm-12">
                    <div class="copyright-text text-center news-list small">
                        <p>&copy; <strong>KRESTON - INDONESIA</strong> 2017. All Rights Reserved.</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- end footer -->
	
	<!-- Backtotop -->
	<div class="body-back-to-top align-right" id="toTop" style="bottom: 40px; opacity: 1;">
		<i class="fa fa-chevron-up"></i>
	</div>
	<!-- End backtotop -->
				
    <script type="text/javascript" src="<?php echo theme_js('jquery.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo theme_js('bootstrap.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo theme_js('wow.min.js'); ?>"></script>	
	<script type="text/javascript" src="<?php echo theme_js('slick.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo theme_js('main.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo theme_js('general.js')?>"></script>
	
</body>
</html>