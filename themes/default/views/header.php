<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="description" content="koleksi digital, perpustakaan sekolah staf dan komando tentara nasional indonesia">
    <meta name="author" content="Jalopy Inc.">
    <meta name="copyright" content="Sesko - TNI">
    <meta name="keywords" content="Sesko - TNI">

    <title>PERPUSTAKAAN SESKO - TNI</title>
	<link href="<?php echo theme_css('font-awesome.min.css')?>" rel="stylesheet">
    <link href="<?php echo theme_css('animate.min.css')?>" rel="stylesheet"> 
	<link href="<?php echo theme_css('slick.css')?>" rel="stylesheet"> 
	<link href="<?php echo theme_css('slick-theme.css')?>" rel="stylesheet"> 
	<link href="<?php echo theme_css('styles.css')?>" rel="stylesheet">
	<link href="<?=theme_css('ionicons.min.css')?>" rel="stylesheet" >
    <!--[if lt IE 9]>
	    <script src="<?php echo theme_js('html5shiv.js')?>"></script>
	    <script src="<?php echo theme_js('respond.min.js')?>"></script>
    <![endif]-->
    <link rel="shortcut icon" href="<?php echo theme_img('favicon.ico')?>">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo theme_img('logo/apple-touch-icon-144-precomposed.png')?>">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo theme_img('logo/apple-touch-icon-114-precomposed.png')?>">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo theme_img('logo/apple-touch-icon-72-precomposed.png')?>">
    <link rel="apple-touch-icon-precomposed" href="<?php echo theme_img('logo/apple-touch-icon-57-precomposed.png')?>">
</head><!--/head-->

<body data-spy="scroll" data-target="#navalphaoo">
	