	<!--   Header   -->
	<header id="header">
        <div class="container">
        	<div class="navbar navbar-inverse" role="banner">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand logoalpha" href="<?php echo base_url(); ?>"></a>					
                </div>
                
                <div class="collapse navbar-collapse" id="navalphaoo">              	
					<form class="form-inline" method="post" action="<?php echo site_url('result'); ?>">	
						<div class="col-md-8 col-sm-8 pull-right top-search">							
							<div class="col-md-12 col-sm-12 input-group">
								<span class="input-group-btn">
									<button type="button" class="col-md-12 col-sm-12 btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Filter <span class="caret"></span></button>
									<ul class="dropdown-menu">
										<li value="title">Judul</li>
										<li value="author">Pengarang</li>																			
									</ul>
								</span><!-- /btn-group -->								
								<input type="text" class="form-control" name="term" placeholder="Kata kunci pencarian...">								
								<span class="input-group-btn">
									<button class="btn btn-primary" type="submit"><span class="fa fa-search"></span></button>
								</span>
							</div><!-- /input-group -->
							<input type='hidden' name='filtername'>
						</div>
					</form>                      			
                </div>
            </div>
        </div>
    </header>
    <!--   End Header   -->
	
	<section class="sectionalpha bg-login">
    	<div class="container">
    		<div class="bgtop">
    			<div class="row">
	    			<div class="col-sm-1">
	    				<img src="<?php echo theme_img('logo/sesko-tni.png')?>">
	    			</div>
	    			<div class="col-sm-11">
	    				<div class="bgtop-title wow fadeInDown" data-wow-duration="500ms" data-wow-delay="300ms">
	    					<h3 class="text-uppercase txt-black3">
	    						Koleksi Digital Perpustakaan Sekolah Staf dan Komando<br>
	    						Tentara Nasional Indonesia
	    					</h3>
	    				</div>
	    			</div>
	    		</div>
    		</div>
        	<div class="row stepbystepalpha">
            	<div class="col-sm-8 col-xs-12 pull-right wow fadeInRight" data-wow-duration="500ms" data-wow-delay="300ms">
                    <form class="form-signin formbottom" action="<?php echo site_url('result'); ?>" method="post" role="form">                        
                        <h2 class="text-uppercase txt-white"><img src="<?php echo theme_img('logo/logo_small.png')?>">&nbsp;&nbsp;Koleksi Digital</h2>
                        <div class="input-group">
							<div class="input-group-btn">				
								<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Filter <span class="caret"></span></button>
								<ul class="dropdown-menu">
									<li value="title">Judul</li>
									<li value="author">Pengarang</li>																											
								</ul>
							</div><!-- /btn-group -->
							<input type="text" class="form-control" name="term" placeholder="Kata kunci pencarian...">	
							<span class="input-group-btn">
								<button class="btn btn-primary" type="submit"><span class="fa fa-search"></span></button>
							</span>
						</div><!-- /input-group -->	
						<!-- Create a hidden input -->
						<input type='hidden' name='filtername'>					
                    </form>
                </div>
            </div>
        </div>
    </section>
	
	<!-- Koleksi Digital -->
	<section class="sectionalpha bg-grey2 framesection">
    	<div class="container">
			<div class="openingpage">
                <h1 class="text-uppercase txt-blue3 list-item box wow fadeInDown" data-wow-duration="500ms" data-wow-delay="300ms">Koleksi EBooks (PDF)</h1>
                <span class="bordertext5 list-item box wow fadeInUp" data-wow-duration="500ms" data-wow-delay="300ms"><span></span></span>
            </div>
			<div class="row">
				<div class="col-sm-12">
					<div class="koleksi">
						<?php 
							foreach($koleksi_digital as $rows):
						?>
							<div>
								<a href="<?php echo site_url('detail/'.$rows->controlnum); ?>"><img src="<?php echo theme_upload('cover/'.$rows->cover_picture)?>"></a>
							</div>
						<?php 
							endforeach;
						?>                                 
					</div>
				</div>
			</div>
		</div>	
    </section>
	<!-- End Koleksi Digital -->
	
	<section class="sectionalpha framesection">
        <div class="container">
			<div class="openingpage">
                <h1 class="text-uppercase txt-blue3 list-item box wow fadeInDown" data-wow-duration="500ms" data-wow-delay="300ms">Daftar Koleksi</h1>
                <span class="bordertext5 list-item box wow fadeInUp" data-wow-duration="500ms" data-wow-delay="300ms"><span></span></span>
            </div>
            
            <div class="row">
        		<div class="col-sm-12 text-center">
        			<ul class="pagination">
					  	<li><button id="btnFirstPageKatalog" class="btn btn-primary">First</button></li>
					  	<li><button id="btnPrevPageKatalog" class="btn btn-primary">Prev</button></li>
					  	<li><button id="btnPositionPageKatalog" class="btn btn-primary"></button></li>
					  	<li><button id="btnNextPageKatalog" class="btn btn-primary">Next</button></li>
					  	<li><button id="btnLastPageKatalog" class="btn btn-primary">Last</button></li>
					</ul>		               					               
        		</div>
        	</div>
            <div class="row">               	
                <div id="listkoleksi-ajax">
					No Data
				</div>				               
        	</div>
        </div>
    </section>
    <!--/#recent-projects-->